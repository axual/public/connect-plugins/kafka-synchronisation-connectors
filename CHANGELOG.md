# Changelog
All notable changes to this project will be documented in this file.

## [master] - TBR
* update build/release pipelines and add renovate support
* added validation for key/value/header converters, these are mandatory and should be `ByteArrayConverter`
* Update versions of dependencies

## [1.0.7] - 2023-07-17
* Retain source record timestamp in the producer record

## [1.0.6] - 2023-02-27
* Added exception handling logic

## [1.0.5] - 2023-02-02
* Issue [#4](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/4) - Sink connector accepts a config which can never work

## [1.0.4] - 2022-12-09
* Issue [#12](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/12) - Kafka Source Connector fails due to non assigned partitions being resumed

## [1.0.3] - 2022-10-21
* Added HeaderFrom SMT to allow copying keys and values to a header
* Issue [#11](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/11) - Fix concurrency issue in KafkaSourceTask

## [1.0.2] - 2022-07-27 
* Issue [#10](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/10) - Fix issue for Context Logging inside Kafka Producer started from Connect Sink and improve error message when Task Stops

## [1.0.1] - 2022-07-12
* Issue [#2](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/2) - Make `source.topics` configuration of Source connector required
* Issue [#6](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/6) - Update TopicSelector config definition
* Issue [#3](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/3) - Configure key and value serializers internally
* Issue [#8](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/issues/8) - [Sink Connector] Offsets commit because of wrong source topicPartition
* [Sink Connector] SinkTask allow access when extending the Task class
* Null TopicSelector target topic throws IllegalStateException
* Added trace logs
* Issue [#9](https://gitlab.com/axual-public/connect-plugins/kafka-synchronisation-connectors/-/issues/9) - [Sink Connector] Fixed the issue of the tight loop which is causing 100% CPU

## [1.0.0] - 2022-02-18
* Release of the Kafka Synchronisation Connectors
  * Kafka Source connector to read from remote Kafka clusters
  * Kafka Sink connector to write to remote Kafka clusters

[master]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.7...master
[1.0.7]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.6...1.0.7
[1.0.6]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.5...1.0.6
[1.0.5]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.4...1.0.5
[1.0.4]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.3...1.0.4
[1.0.3]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/-/tree/1.0.0
