# Kafka Synchronisation Connectors
[![Pipeline Status](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/badges/master/pipeline.svg)](https://gitlab.com/axual/public/connect-plugins/kafka-synchronisation-connectors/commits/master)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=axual-public-connect-plugins-kafka-synchronisation-connectors&metric=coverage&token=5aed982f59a97ceb3a779a73ca5e742160e2a203)](https://sonarcloud.io/dashboard?id=axual/public-connect-plugins-kafka-synchronisation-connectors)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=axual-public-connect-plugins-kafka-synchronisation-connectors&metric=sqale_rating&token=5aed982f59a97ceb3a779a73ca5e742160e2a203)](https://sonarcloud.io/dashboard?id=axual/public-connect-plugins-kafka-synchronisation-connectors)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=axual-public-connect-plugins-kafka-synchronisation-connectors&metric=alert_status&token=5aed982f59a97ceb3a779a73ca5e742160e2a203)](https://sonarcloud.io/dashboard?id=axual/public-connect-plugins-kafka-synchronisation-connectors)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Contains Kafka Connect connector plugins to produce data to, and consume data from remote Kafka clusters.

[[_TOC_]]

## How to install
The library required to run the connector can be found in the [Maven Central Repository](https://search.maven.org/)
1. Search for the artifact on [Maven](https://search.maven.org/search?q=g:io.axual.connect.plugins.kafka%20AND%20a:kafka-synchronisation-connectors)
2. Download the type `jar-with-dependencies.jar`. This jar contains all runtime dependencies to use the connectors
3. Locate the `plugins` directory for every member of your Kafka Connect cluster.
4. Create a subdirectory in the `plugins` directory and copy the downloaded jar file into this directory on each member.
5. Restart the Kafka Connect members to load the new plugin.
6. Check if the plugin shows using each of your members REST API. `http://<host>:<connectport>/connector-plugins`

When all members of the cluster are loaded you can configure connectors using the Kafka Synchronisation Connectors

## Kafka Sink Connector
The Kafka Sink Connector is used to read records produced on the Kafka cluster where Kafka Connect is paired with, and produce them to a topic on a remote Kafka cluster. Or to a system which accepts the Kafka protocol to produce records.

The Kafka Sink Connector supports:

- Remote producer configuration 

  The producer used to connect to the remote system supports all Kafka producer configuration options, except the serializer settings.

- Static header selection 
  
  Define your own headers to be statically added to each produced record.
  
  This can be used to identify records produced by the connector
    
- Metadata forwarding

  Forward record metadata as a header with each record. Supported fields are:

  - Topic
    
    Send the topic name as a UTF-8 String. Default header name = `X-Kafka-Original-Topic`

  - Partition
    
    Send the partition as a 32 bit integer. Default header name = `X-Kafka-Original-Partition`

  - Offset
    
    Send the offset as a 64 bit integer (Type long). Default header name = `X-Kafka-Original-Offset`

  - Timestamp
    
    Send the record timestamp as a 64 bit integer (Type long). Default header name = `X-Kafka-Original-Timestamp`
    
- Target topic selection logic

  The target topic name can be controlled in different ways, and can be extended in the future.

  The current implementations are:

  - source
    
    Use the topic name used for the original record, this is the default setting

  - prefix
    
    Add a prefix to the topic name used for the original record

  - fixed
    
    Use a fixed topic name

  - mapping

    Use a configurable mapping from source to target topic.

- Target partition selection logic

  The target partition number can be determined in different ways, and can be extended in the future.

  The current implementations are:

    - source

      Use the partition number used for the original record, this is the default setting

    - partitioner

      Use the partitioner defined in the remote producer settings


### How to configure the KafkaSinkConnector
#### Class
The KafkaSinkConnector class is `io.axual.connect.plugins.kafka.sink.KafkaSinkConnector`

#### Configuration options

##### Worker and remote producer settings
The worker settings control the internal connector functionality, like queue sizes and wait times for handling Connect internal requests.<br>
The remote settings are used to set producer connecting to the remote cluster.

| Key | Type | Default  | Description  |
|:---|:----|:--------:|:-------------|
| `remote.` | prefix | _not applicable_ | This is the prefix for the default producer settings. <br> For example, the producer configuration `bootstrap.servers` is provided as `remote.bootstrap.servers` |
| `queue.size` | Integer | `10` | Connect provides the connector with a list of records to send which are put on a queue.<br>The sender is an asynchronous process retrieving these lists from a queue.<br> This setting determines the size of the queue |
| `queue.put.wait.ms` | Long | `500` | This setting determines how long the connector waits for the queue to accepts the list with records |

##### Static header selection
These settings are used to provide each record with a static header.<br>
The name and value will be set as String, and the value will be sent as a UTF-8 encoded byte array. 

| Key | Type | Default  | Description  |
|:---|:----|:--------:|:-------------|
| `header.static.aliases` |  List  | _empty list_ | The aliases used to determine the headers used for name and value definition |
| `header.static.<alias>.name` | String | _null_ | The name of the header for the `alias`|
| `header.static.<alias>.value` | String | _null_ | The value of header for the `alias`. This string value will be sent as an UTF-8 String |

##### Metadata forwarding
These settings make it possible to forward certain local record metadata fields as headers of the remote record metadata.
This makes it possible to identify the original source of a record that was produced by this connector.

| Key | Type | Default  | Description  |
|:---|:----|:--------:|:-------------|
| `header.remote.prefix` | String | `""` | Prefix to add to header names of forwarded remote headers. |
| `forward.metadata.topic.name` | String  | `X-Kafka-Original-Topic` | The name to use for the header containing the record topic name |
| `forward.metadata.partition.name` | String  | `X-Kafka-Original-Partition` | The name to use for the header containing the record partition |
| `forward.metadata.offset.name` | String  | `X-Kafka-Original-Offset` | The name to use for the header containing the record offset |
| `forward.metadata.timestamp.name` | String  | `X-Kafka-Original-Timestamp` | The name to use for the header containing the record timestamp |

##### Topic Selector settings
The topic selector determines to which topic a record will be produced.

The current implementations are:

- source

  Use the topic name used for the original record, this is the default setting.<br>
  This selector requires no selector configuration.

  | Key                    | Type   | Example                                                        | Description                                                                                                 |
  |------------------------|:---------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------|:-------------|
  | `topic.selector`       | String | `source`                                                       | **Deprecated** Determines which topic selector to use, the default selector needs no further configuration. |
  | `topic.selector.class` | String | `io.axual.connect.plugins.kafka.selectors.SourceTopicSelector` | Determines which topic selector to use, the default selector needs no further configuration.                |

- prefix

  Add a prefix to the topic name used for the original record.<br>
  This selector uses the selector configuration as the prefix for the remote topic name. 

  | Key                     | Type   | Example                                                        |                                  Description                                  |
  |-------------------------|:---------------------------------------------------------------|:-----------------------------------------------------------------------------:|:-------------|
  | `topic.selector`       | String | `prefix`                                                       |     **Deprecated** Determines which topic selector to use, here `prefix`      |
  | `topic.selector.class` | String | `io.axual.connect.plugins.kafka.selectors.PrefixTopicSelector` | Determines which topic selector to use, here `PrefixTopicSelector`.                 |
  | `topic.selector.prefix` | String | _none_                                                         | The prefix to prepend to forwarded topics. This is a mandatory configuration. |

- fixed

  Use a fixed topic name and ignores the original topic name.<br>
  This selector uses the selector configuration as the remote topic name.

  | Key                     | Type | Example  | Description  |
  |------------------------|---------------------------|:--------:|:-------------|
  | `topic.selector`        |  String | `fixed` | Determines which topic selector to use; here `fixed`. |
  | `topic.selector.class`  |  String | `io.axual.connect.plugins.kafka.selectors.FixedTopicSelector` | Determines which topic selector to use; here `FixedTopicSelector`. |
  | `topic.selector.target` | String | _none_ | The fixed target topic to use. This is a mandatory configuration. |

- mapping
  
  Use a mapping from input topic name to output. Multiple mappings can be configured, as follows:

  | Key | Type | Example                                                         | Description |
  |:----|:----------------------------------------------------------------|:--------|:------------|
  | `topic.selector` |  String | `mapping`                                                       | **Deprecated** Determines which topic selector to use, here `mapping`. |
  | `topic.selector.class` | String | `io.axual.connect.plugins.kafka.selectors.MappingTopicSelector` | Determines which topic selector to use, here `MappingTopicSelector`.                 |
  | `topic.selector.mapping.<topic-in>` | String | _n/a_                                                           | map messages coming from `topic-in` to the configured topic. This configuration can be repeated multiple times for multiple input topics; if no mappings are provided, the selector will log an error and exit. | 


##### Partition selector settings

The partition selector determines to which topic a record will be produced.

The current implementations are:

  - source

    Use the partition number used for the original record, this is the default setting.<br>
    If the remote topic doesn't have this partition the produce call and connector task will fail.

  - partitioner

    Use the partitioner defined by the remote producer settings.

| Key | Type | Default  | Description  |
|:---|:----|:--------:|:-------------|
| `partition.selector` |  String | `source` | Determines which partition selector to use, accepts `source`, `partitioner` |

### Example configurations

#### Minimal setup 
No headers are added to the produced record.<br>
The default topic and partition selectors are used; for Sink Connector, the default topic selector is `source` which needs no further configuration.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-minimal",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093"
  }
}
```
</details>

#### Using static headers
Three static headers are added to each produced record.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-static-header",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "header.static.aliases" : "why,now",
    "header.static.why.name": "Example-Why",
    "header.static.why.value": "We needed an example",
    "header.static.how.name": "Example-How",
    "header.static.how.value": "Using the Kafka Sink Connector"
  }
}
```
</details>

#### Using metadata forwarding
Each produced record contains the metadata from the original record using custom header names.
<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-metadata-forward",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "forward.metadata.topic.name": "Example-Topic",
    "forward.metadata.partition.name": "Example-Partition",
    "forward.metadata.offset.name": "Example-Offset",
    "forward.metadata.timestamp.name": "Example-Timestamp"
  }
}
```

</details>

#### Using _source_ topic selector
Each record is written to the same topic as the original record.<br>
This example is functionally identical to the Minimal setup example

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-topic-source",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "topic.selector.class": "io.axual.connect.plugins.kafka.selectors.SourceTopicSelector"
  }
}
```

</details>

#### Using _fixed_ topic selector
Each record is written to the remote topic `forwarded-to-us`.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-topic-fixed",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "topic.selector.class": "io.axual.connect.plugins.kafka.selectors.FixedTopicSelector",
    "topic.selector.target": "forwarded-to-us"
  }
}
```

</details>


#### Using _prefix_ topic selector
This example produces records to the same topic on the remote cluster.<br>
The selector configuration sets the prefix to `forwarded-`.<br>
In this case the connector only reads from topic `test-topic`, resulting in the target topic name `forwarded-test-topic`.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-topic-prefix",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "topic.selector.class": "io.axual.connect.plugins.kafka.selectors.PrefixTopicSelector",
    "topic.selector.prefix": "forwarded-"
  }
}
```
</details>

#### Using _mapping_ topic selector
This example produces records to the same topic on the remote cluster.<br>
In this case the connector only reads from topic `test-topic`, resulting in the target topic name `forwarded-from-test-topic`. The other mapping is for local topic `topic2` to target topic `another-out`.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-topic-prefix",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "topic.selector.class": "io.axual.connect.plugins.kafka.selectors.MappingTopicSelector",
    "topic.selector.mapping.test-topic": "forwarded-from-test-topic",
    "topic.selector.mapping.topic2": "another-out"
  }
}
```
</details>

#### Using _source_ partition selector
Each record is written to the same partition as the original record.<br>
This example is functionally identical to the Minimal setup example

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-partition-source",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "partition.selector": "source"
  }
}
```

</details>

#### Using _partitioner_ partition selector
The partition of each record produced to the remote cluster is determined by the **_Partitioner_** class used by the producer.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-partition-partitioner",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "remote.partitioner.class": "org.apache.kafka.clients.producer.internals.DefaultPartitioner",
    "partition.selector": "partitioner"
  }
}
```

The following configuration is functionally identical to the one above because the default value of `remote.partitioner.class` is `org.apache.kafka.clients.producer.internals.DefaultPartitioner`.

```json
{
  "name": "kafka-sink-partition-partitioner",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "partition.selector": "partitioner"
  }
}
```

</details>

#### Prefixing headers
Kafka headers will be forwarded, with their names prefixed with `X-Original-` in the produced SinkRecord.
<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-metadata-forward",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "header.remote.prefix": "X-Original-"
  }
}
```

</details>

## Kafka Source Connector
The Kafka Source Connector is used to read records produced on a remote Kafka cluster (or a system on which records 
can be consumed using the Kafka protocol), and produce them on the cluster Kafka Connect is paired with.
<br>
The Kafka Source Connector supports:
- Remote consumer configuration

  The consumer used to connect to the remote system supports all Kafka consumer configuration options, except the 
  serializer settings.

- Static header defintion
  
  Define your own headers to be statically added to each consumed record.

- Metadata forwarding

  Remote metadata such as source topic, partition, offset and timestamp can optionally be forwarded as Kafka headers.
  The names of these headers are configurable; if no header name is configured for a field, that information is not sent.

- Local topic selector logic
 
  The local topic where Connect will produce to can be controlled in different ways. Current implementations are:
  - single

    All records are produced on a single, configurable output topic.
  - prefix

    Records are produced on the source topic, with the name prefixed by the configured value.
  - source
    
    All records are produced on the same topic as where they came from
  - map

    A mapping can be configured from input topic to topic to produce on.<br>
    _Note:_ in case of pattern subscriptions on the remote system, if a message is received on a topic for which
    no mapping is configured, this is flagged as a runtime exception.

### How to configure the Kafka Source Connector
#### Class
The Kafka Source Connector class is `io.axual.connect.plugins.kafka.sink.KafkaSourceConnector`

#### Configuration options
##### Worker and remote consumer settings
The remote settings are used to set up the consumer connecting to the remote cluster. All consumer properties can
be set with the exception of the key and value deserializers. Note that the remote user group is _not_ optional for this connector.

| Key | Type | Default | Description |
|:----|:-----|:--------|:------------|
|`remote.`|prefix|_n/a_|The prefix for remote consumer settings. For example, the configuration `bootstrap.servers` is provided as `remote.bootstrap.servers`. |

##### Static header configuration
These settings are used to provide each record with one or more static headers. The name and value are set as Strings,
and the value will be sent as a UTF-8 encoded byte array.

| Key | Type | Default | Description |
|:----|:-----|:--------|:------------|
| `header.static.aliases` | List | _empty list_ | The aliases used to determine the static header names and values |
| `header.static.<alias>.name` | String | _null_ | The name of the header for this `alias` |
| `header.static.<alias>.value` | String | _null_ | The value of the header for this `alias`. This value will be sent as an UTF-8 encoded byte array. |

##### Metadata forwarding
These settings make it possible to forward remote metadata to the local system as Kafka headers.This makes it possible
to identify the original source of a record produced on the local system.<br>
Remote headers are always forwarded as-is; header names can optionally be prefixed with a configurable string.

_Note:_ Kafka messages can have different types of timestamps. If timestamps are to be forwarded (`forward.metadata.timestamp.name` is set) an extra header is sent with the configured name, suffixed by `-Type`. This will indicate the type of the timestamp as specified in the originating record; the value is one of `NO_TIMESTAMP_TYPE`, `CREATE_TIME` or `LOG_APPEND_TIME`.

| Key | Type | Default | Description |
|:----|:-----|:--------|:------------|
| `header.remote.prefix` | String | `""` | Prefix to add to header names of forwarded remote headers. |
| `forward.metadata.topic.name` | String | _null_ | If configured, name of the Kafka header in which the originating topic is sent. The value is sent as an UTF-8 encoded byte array. |
| `forward.metadata.partition.name` | String | _null_ | If configured, name of the Kafka header in which the originating partition is sent. The value is forwarded as a 32-bit integer. |
| `forward.metadata.offset.name` | String | _null_ | If configured, name of the Kafka header in which the offset of the originating message is sent. The value is sent as a 64-bit Long. |
| `forward.metadata.timestamp.name` | String | _null_ | If configured, name of the Kafka header in which the timestamp of the  originating message is sent.  |

##### Topic mapping settings
The topic mapping determines on which local topic remote messages will be produced.<br>
Currently there are the following implementations:
- fixed

  All messages are produced to a single topic.
  
  | Key | Type | Example                                                       | Description                                      |
  |:----|:--------------------------------------------------------------|:-------------------------------------------------|:------------|
  | `topic.selector` | String | `fixed`                                                       | **Deprecated** Select fixed topic selection type |
  | `topic.selector` | String | `io.axual.connect.plugins.kafka.selectors.FixedTopicSelector` | Select fixed topic selection type               |
  | `topic.selector.target` | String | `topic-out`                                                   | The single output topic to select                |

- prefix

  All messages are produced on the same topic as they came from, with the name prefixed by a configurable prefix.

  | Key                     | Type | Example | Description |
-------------------------|:----|:-----|:--------|:------------|
  | `topic.selector`        | String | `prefix` | **Deprecated** Select prefixed topic selection type |
  | `topic.selector.class`       | String | `io.axual.connect.plugins.kafka.selectors.PrefixTopicSelector` | Select prefixed topic selection type |
  | `topic.selector.prefix` | String | `from-` | The prefix to prepend to topic names |

- source

  All messages are produced on the same topic as they came from. This selector has no further configuration.

  | Key               | Type | Example | Description                                                                                  |
  |-------------------|:-----|:---------------------------------------------------------------------------------------------|:------------|
  | `topic.selector`  | String | `source` | **Deprecated** Select source topic selection type                                            |
  | `topic.selector.class` | String | `io.axual.connect.plugins.kafka.selectors.SourceTopicSelector` | Select source topic selection type. |

- mapping

  Incoming messages are mapped by a configurable mapping.

  | Key | Type | Example                                                         | Description                                                                                                  |
  |:----|:----------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------|:------------|
  | `topic.selector` | String | `mapping`                                                       | **Deprecated** Select prefixed topic selection type                                                          |
  | `topic.selector.class` | String | `io.axual.connect.plugins.kafka.selectors.MappingTopicSelector` | Select prefixed topic selection type.                                                                        |
  | `topic.selector.mapping.in-1` | String | `out-1`                                                         | Maps records from `in-1` to the specified output `out-1`. This setting is repeated for each desired mapping. |


### Example configurations
#### Minimal setup 
No headers are added to the produced record.<br> 
_*Note:*_ by default topic selector `single` is configured, so the topic mapping target needs to be specified.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-source-minimal",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.source.KafkaSourceConnector",
    "tasks.max": "1",
    "source.topics": "input-topic-remote",
    "topic.selector.target":"output-topic-local",
    "remote.bootstrap.servers": "remote-broker:19093",
    "remote.sasl.mechanism": "PLAIN",
    "remote.group.id": "connect_group",
    "remote.enable.auto.commit": "true",
    "remote.auto.offset.reset": "earliest",
    "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "header.converter": "org.apache.kafka.connect.converters.ByteArrayConverter"
  }
}
```
</details>

#### Using `mapping` topic selector
Maps incoming records from two subscribed topics to two different output topics.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-source-connector",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.source.KafkaSourceConnector",
    "tasks.max": "1",
    "source.topics": "input-topic-1,input-topic-2",
    "topic.selector.class": "io.axual.connect.plugins.kafka.selectors.MappingTopicSelector",
    "topic.selector.mapping.input-topic-1": "local-topic-one",
    "topic.selector.mapping.input-topic-2": "local-topic-two",
    "remote.bootstrap.servers": "remote-broker:19093",
    "remote.sasl.mechanism": "PLAIN",
    "remote.group.id": "connect_group",
    "remote.enable.auto.commit": "true",
    "remote.auto.offset.reset": "earliest",
    "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "header.converter": "org.apache.kafka.connect.converters.ByteArrayConverter"
  }
}
```
</details>

#### Using `source` topic selector
Maps incoming records to the topic they came from.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-source-connector",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.source.KafkaSourceConnector",
    "tasks.max": "1",
    "source.topics": "input-topic-1,input-topic-2",
    "topic.selector.class": "io.axual.connect.plugins.kafka.selectors.SourceTopicSelector",
    "remote.bootstrap.servers": "remote-broker:19093",
    "remote.sasl.mechanism": "PLAIN",
    "remote.group.id": "connect_group",
    "remote.enable.auto.commit": "true",
    "remote.auto.offset.reset": "earliest",
    "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "header.converter": "org.apache.kafka.connect.converters.ByteArrayConverter"
  }
}
```
</details>

#### Using `prefix` topic selector
Maps incoming records to the topic they came from, with a prefix prepended to the name.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-source-connector",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.source.KafkaSourceConnector",
    "tasks.max": "1",
    "source.topics": "input-topic-1,input-topic-2",
    "topic.selector.class": "io.axual.connect.plugins.kafka.selectors.PrefixTopicSelector",
    "topic.selector.prefix": "incoming-",
    "remote.bootstrap.servers": "remote-broker:19093",
    "remote.sasl.mechanism": "PLAIN",
    "remote.group.id": "connect_group",
    "remote.enable.auto.commit": "true",
    "remote.auto.offset.reset": "earliest",
    "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "header.converter": "org.apache.kafka.connect.converters.ByteArrayConverter"
  }
}
```
</details>

#### Forwarding remote metadata
Adds headers to each produced record containing the originating topic, partition, offset and timestamp.<br>
The timestamp type will be appended as a header with `-Type` appended to the specified timestamp header name. <br>
Using the timestamp header `X-Kafka-Original-Timestamp` will also create the header `X-Kafka-Original-Timestamp-Type`.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-source-connector",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.source.KafkaSourceConnector",
    "tasks.max": "1",
    "source.topics": "input-topic-remote",
    "topic.selector.target":"output-topic-local",
    "forward.metadata.topic.name": "X-Kafka-Original-Topic",
    "forward.metadata.partition.name": "X-Kafka-Original-Partition",
    "forward.metadata.offset.name": "X-Kafka-Original-Offset",
    "forward.metadata.timestamp.name": "X-Kafka-Original-Timestamp",
    "remote.bootstrap.servers": "remote-broker:19093",
    "remote.sasl.mechanism": "PLAIN",
    "remote.group.id": "connect_group",
    "remote.enable.auto.commit": "true",
    "remote.auto.offset.reset": "earliest",
    "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "header.converter": "org.apache.kafka.connect.converters.ByteArrayConverter"
  }
}
```
</details>

#### Using static headers
Two static headers are added to each produced record.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-source-connector",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.source.KafkaSourceConnector",
    "tasks.max": "1",
    "source.topics": "input-topic-remote",
    "topic.selector.target":"output-topic-local",
    "header.static.aliases": "hdr1,hdr2",
    "header.static.hdr1.name": "X-Header-1",
    "header.static.hdr1.value": "Value for header 1",
    "header.static.hdr2.name": "X-Header-Another",
    "header.static.hdr2.value": "Header added by source connector",
    "remote.bootstrap.servers": "remote-broker:19093",
    "remote.sasl.mechanism": "PLAIN",
    "remote.group.id": "connect_group",
    "remote.enable.auto.commit": "true",
    "remote.auto.offset.reset": "earliest",
    "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "header.converter": "org.apache.kafka.connect.converters.ByteArrayConverter"
  }
}
```
</details>

#### Prefixing remote headers
Header names of the record from the remote system will be forwarded, with the name prefixed
with `Remote-` on the record produced to the local system.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-source-connector",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.source.KafkaSourceConnector",
    "tasks.max": "1",
    "source.topics": "input-topic-remote",
    "topic.selector.target":"output-topic-local",
    "header.remote.prefix": "Remote-",
    "remote.bootstrap.servers": "remote-broker:19093",
    "remote.sasl.mechanism": "PLAIN",
    "remote.group.id": "connect_group",
    "remote.enable.auto.commit": "true",
    "remote.auto.offset.reset": "earliest",
    "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
    "header.converter": "org.apache.kafka.connect.converters.ByteArrayConverter"
  }
}
```
</details>

## Single Message Transformation
### HeaderTo
Next to existing message transformations, this connector package provides an SMT which makes it possible to copy the schema and value 
of a Connect header to either the key of the value of the result message. Optionally, the copied header can be removed. <br>
Use the classes `io.axual.connect.plugins.kafka.transforms.HeaderTo$Key` or `io.axual.connect.plugins.kafka.transforms.HeaderTo$Value` to copy a header to the record key or value field, overwriting any existing key or value data.<br>
The following configuration properties exist:

| Name | Type | Description |
|:-----|:-----|:------------|
| `name` | String | Name of the header to copy. |
| `remove` | Boolean | Indicate if the copied header needs to be removed. |

The following example sets up the Sink connector with a transform which will copy the data
from header `Original-Header` to the key of the resulting message and remove the header.

<details>
<summary>Open example configuration</summary>

```json
{
  "name": "kafka-sink-with-transform",
  "config": {
    "connector.class": "io.axual.connect.plugins.kafka.sink.KafkaSinkConnector",
    "tasks.max": "10",
    "topics": "test-topic",
    "remote.bootstrap.servers": "remotekafka-1:9093,remotekafka-2:9093",
    "transforms": "headerto",
    "transforms.headerto.type": "io.axual.connect.plugins.kafka.transforms.HeaderTo$Key",
    "transforms.headerto.name": "Original-Header",
    "transforms.headerto.remove": "true"
  }
}
```
</details>

## Contributing
Axual is interested in building the community; we would welcome any thoughts or
[patches](https://gitlab.com/axual/public//connect-plugins/kafka-synchronisation-connectors/-/issues).
You can reach us [here](https://axual.com/contact/).

See [contributing](https://gitlab.com/axual/public//connect-plugins/kafka-synchronisation-connectors/blob/master/CONTRIBUTING.md).

## License
Kafka Synchronisation Connectors is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
