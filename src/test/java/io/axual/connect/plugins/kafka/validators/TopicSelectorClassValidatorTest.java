package io.axual.connect.plugins.kafka.validators;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import io.axual.connect.plugins.kafka.selectors.FixedTopicSelector;
import io.axual.connect.plugins.kafka.selectors.MappingTopicSelector;
import io.axual.connect.plugins.kafka.selectors.PartitionSelector;
import io.axual.connect.plugins.kafka.selectors.PrefixTopicSelector;
import io.axual.connect.plugins.kafka.selectors.SourceTopicSelector;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TopicSelectorClassValidatorTest {

    TopicSelectorClassValidator validator = new TopicSelectorClassValidator();

    @ParameterizedTest
    @ValueSource(classes = {
            FixedTopicSelector.class,
            MappingTopicSelector.class,
            PrefixTopicSelector.class,
            SourceTopicSelector.class,
    })
    void ensureValid(Class<?> clazz) {
        assertDoesNotThrow(
                () -> validator.ensureValid("validator", clazz)
        );
    }

    @ParameterizedTest
    @ValueSource(classes = {
            String.class,
            Object.class,
            PartitionSelector.class,
    })
    void ensureValidIsInvalid(Class<?> clazz) {
        assertThrows(
                ConfigException.class, () -> validator.ensureValid("validator", clazz)
        );
    }

}