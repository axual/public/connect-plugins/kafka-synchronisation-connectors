package io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.integrationtests.tools.providers.StartableKafkaCluster;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.testcontainers.containers.output.Slf4jLogConsumer;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implementation of {@link StartableKafkaCluster} based on testcontainers.
 */
@Slf4j
public class TestcontainerKafkaCluster implements StartableKafkaCluster {
    private static final AtomicInteger CLUSTER_COUNTER = new AtomicInteger(1);
    public static final String DEFAULT_KAFKA_LISTENER_NAME = "default";

    private final KafkaContainer kafkaContainer;

    private final KafkaOperations kafkaOperations;

    public TestcontainerKafkaCluster(String logPrefix) {
        this(null, logPrefix);
    }

    public TestcontainerKafkaCluster(KafkaContainer kafkaContainer, String logPrefix) {
        final var prefix = logPrefix + "- Kafka-" + CLUSTER_COUNTER.getAndIncrement() + " - ";
        if (kafkaContainer == null) {
            this.kafkaContainer = new KafkaContainer()
                    .withExposedListener(DEFAULT_KAFKA_LISTENER_NAME)
            ;
        } else {
            this.kafkaContainer = kafkaContainer;
        }

        if (this.kafkaContainer.getLogConsumers().isEmpty()) {
            this.kafkaContainer.setLogConsumers(
                    List.of(
                            new Slf4jLogConsumer(log, true)
                                    .withPrefix(prefix)
                    ));
        }

        this.kafkaOperations = new KafkaOperations(this.kafkaContainer, prefix);
    }


    @Override
    public void start() {
        this.kafkaContainer.start();
    }

    @Override
    public void stop() {
        this.kafkaContainer.stop();

    }

    @Override
    public void createTopic(final String name, final int partitions, final boolean compacted) {
        kafkaOperations.createTopic(name, partitions, compacted);
    }

    @Override
    public void deleteTopic(final String name) {
        kafkaOperations.deleteTopic(name);
    }

    @Override
    public ConsumerRecords<byte[], byte[]> consume(final String listenerName, final int expectedRecordCount, final long maxDuration, final String groupId, final Collection<String> topics) {
        return kafkaOperations.consume(listenerName, expectedRecordCount, maxDuration, groupId, topics);
    }

    @Override
    public long produce(final String listenerName, final ProducerRecord<byte[], byte[]> producerRecord) {
        return kafkaOperations.produce(listenerName, producerRecord);
    }

    @Override
    public String bootstrapServers() {
        return kafkaOperations.bootstrapServers();
    }

    @Override
    public String bootstrapServers(final String listenerName) {
        return kafkaOperations.bootstrapServers(listenerName);
    }

    @Override
    public Map<String, Object> clientConnectionProperties() {
        return kafkaOperations.clientConnectionProperties();
    }

    @Override
    public Map<String, Object> clientConnectionProperties(final String listenerName) {
        return kafkaOperations.clientConnectionProperties(listenerName);
    }

    @Override
    public Map<String, Object> producerProperties(final String listenerName, final String transactionalId) {
        return kafkaOperations.producerProperties(listenerName, transactionalId);
    }

    @Override
    public Map<String, Object> consumerProperties(final String listenerName, final String groupId) {
        return kafkaOperations.consumerProperties(listenerName, groupId);
    }
}
