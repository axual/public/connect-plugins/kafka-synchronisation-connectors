package io.axual.connect.plugins.kafka.transforms;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaAndValue;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.header.Headers;
import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import static io.axual.connect.plugins.kafka.transforms.HeaderFrom.HEADER_APPEND_CONFIG;
import static io.axual.connect.plugins.kafka.transforms.HeaderFrom.HEADER_NAME_CONFIG;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class HeaderFromTest {

    private static final String TARGET_HEADER_NAME = "copiedFrom";

    @Test
    void config_TestDefinitions() {
        HeaderFrom<SinkRecord> transformer = new HeaderFrom.Key<>();
        ConfigDef configDef = assertDoesNotThrow(transformer::config);
        assertNotNull(configDef);
        assertTrue(configDef.configKeys().containsKey(HEADER_NAME_CONFIG));
        assertTrue(configDef.configKeys().containsKey(HEADER_APPEND_CONFIG));
    }

    @Test
    void apply_WithoutConfiguring() {
        HeaderFrom<SinkRecord> transformer = new HeaderFrom.Key<>();
        SinkRecord testRecord = new SinkRecord("topic", 1, Schema.INT32_SCHEMA, 2, Schema.INT64_SCHEMA, 3L, 4L);
        assertThrows(IllegalStateException.class, () -> transformer.apply(testRecord));
    }

    @Test
    void configure_Fail_EmptyMap() {
        HeaderFrom<SinkRecord> transformer = new HeaderFrom.Key<>();
        Map<String, String> config = new HashMap<>();
        assertThrows(ConfigException.class, () -> transformer.configure(config));
    }

    @Test
    void configure_Fail_EmptyHeaderName() {
        HeaderFrom<SinkRecord> transformer = new HeaderFrom.Key<>();
        Map<String, String> config = new HashMap<>();
        config.put(HEADER_NAME_CONFIG, "");
        assertThrows(ConfigException.class, () -> transformer.configure(config));
    }

    @Test
    void configure_Fail_InvalidBoolean() {
        HeaderFrom<SinkRecord> transformer = new HeaderFrom.Key<>();
        Map<String, String> config = new HashMap<>();
        config.put(HEADER_NAME_CONFIG, "notEmpty");
        config.put(HEADER_APPEND_CONFIG, "invalidBoolean");
        assertThrows(ConfigException.class, () -> transformer.configure(config));
    }

    @ParameterizedTest(name = "{0}")
    @CsvSource(value = {
            "Key transform   - with header    - append header, true, true, true",
            "Key transform   - with header    - overwrite header, true, true, false",
            "Key transform   - without header - append header, true, false, true",
            "Key transform   - without header - overwrite header, true, false, false",
            "Value transform - with header    - append header, false, true, true",
            "Value transform - with header    - overwrite header, false, true, false",
            "Value transform - without header - append header, false, false, true",
            "Value transform - without header - overwrite header, false, false, false",
    })
    void apply_Transformation_ValidData(String testName, boolean forKey, boolean alreadyHasTargetHeader, boolean appendHeader) {
        log.info("Running transformation test : {}", testName);

        // Create the test data
        final Headers inputHeaders = createInputHeaders(alreadyHasTargetHeader);
        final SinkRecord inputRecord = new SinkRecord("TestTopic", 2, Schema.OPTIONAL_STRING_SCHEMA, "expected Key", Schema.STRING_SCHEMA, "Expected Value", 5L, 6L, TimestampType.CREATE_TIME, inputHeaders);

        // Create the expected result
        final SinkRecord expectedRecord = createExpectedOutput(inputRecord, forKey, appendHeader);

        // Create header configuration
        final Map<String, String> transformationConfig = new HashMap<>();
        transformationConfig.put(HEADER_NAME_CONFIG, TARGET_HEADER_NAME);
        transformationConfig.put(HEADER_APPEND_CONFIG, Boolean.toString(appendHeader));

        // create HeaderFrom Transform and configure
        final HeaderFrom<SinkRecord> transform = forKey ? new HeaderFrom.Key<>() : new HeaderFrom.Value<>();
        transform.configure(transformationConfig);

        // apply transformation
        final SinkRecord transformedRecord = assertDoesNotThrow(() -> transform.apply(inputRecord));

        // Verify contents, input record
        assertEquals(expectedRecord, transformedRecord);
    }

    // Create a collection of headers to use in the test
    Headers createInputHeaders(boolean alreadyHasTargetHeader) {
        final Headers headerCollection = new ConnectHeaders()
                .add("Header 1", "First header, first time", Schema.STRING_SCHEMA)
                .add("Header 1", "First header, second time", Schema.STRING_SCHEMA)
                .add("Header 1", "First header, third time", Schema.STRING_SCHEMA)
                .add("Header 2", "Second header", Schema.STRING_SCHEMA)
                .add("Header 3", "Third header", Schema.STRING_SCHEMA)
                .add("Header 4", "Fourth header", Schema.STRING_SCHEMA);

        if (alreadyHasTargetHeader) {
            headerCollection
                    .add(TARGET_HEADER_NAME, "FirstValue", Schema.STRING_SCHEMA)
                    .add(TARGET_HEADER_NAME, "SecondValue", Schema.STRING_SCHEMA);

        }
        return headerCollection;
    }

    // Construct the expected transformed record based on the input record and configuration
    SinkRecord createExpectedOutput(SinkRecord inputRecord, boolean useKey, boolean appendHeader) {
        final SchemaAndValue headerData;
        if (useKey) {
            headerData = new SchemaAndValue(inputRecord.keySchema(), inputRecord.key());
        } else {
            headerData = new SchemaAndValue(inputRecord.valueSchema(), inputRecord.value());
        }

        final ConnectHeaders expectedHeaders = new ConnectHeaders();
        inputRecord.headers().forEach(header -> expectedHeaders.add(header.key(), header.value(), header.schema()));

        if (!appendHeader) {
            expectedHeaders.remove(TARGET_HEADER_NAME);
        }
        expectedHeaders.add(TARGET_HEADER_NAME, headerData);

        return inputRecord.newRecord(inputRecord.topic(), inputRecord.kafkaPartition(), inputRecord.keySchema(), inputRecord.key(), inputRecord.valueSchema(), inputRecord.value(), inputRecord.timestamp(), expectedHeaders);
    }

    @ParameterizedTest(name = "{0}")
    @CsvSource(value = {
            "Key invalid data, true",
            "Value invalid data, false",
    })
    void apply_Transformation_InvalidData(String testName, boolean forKey) {
        log.info("Running transformation test : {}", testName);

        // Create the test data
        final Headers inputHeaders = createInputHeaders(false);
        final SinkRecord inputRecord = new SinkRecord("TestTopic", 2, null, "expected Key", null, "Expected Value", 5L, 6L, TimestampType.CREATE_TIME, inputHeaders);

        // Create the expected result which should be the same
        final SinkRecord expectedRecord = new SinkRecord("TestTopic", 2, null, "expected Key", null, "Expected Value", 5L, 6L, TimestampType.CREATE_TIME, inputHeaders);

        // Create header configuration
        final Map<String, String> transformationConfig = new HashMap<>();
        transformationConfig.put(HEADER_NAME_CONFIG, TARGET_HEADER_NAME);
        transformationConfig.put(HEADER_APPEND_CONFIG, Boolean.toString(true));

        // create HeaderFrom Transform and configure
        final HeaderFrom<SinkRecord> transform = forKey ? new HeaderFrom.Key<>() : new HeaderFrom.Value<>();
        transform.configure(transformationConfig);

        // apply transformation
        final SinkRecord transformedRecord = assertDoesNotThrow(() -> transform.apply(inputRecord));

        // Verify contents, input record
        assertEquals(expectedRecord, transformedRecord);
    }
}
