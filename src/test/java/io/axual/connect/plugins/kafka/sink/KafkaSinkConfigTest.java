package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.axual.connect.plugins.kafka.selectors.FixedTopicSelector;
import io.axual.connect.plugins.kafka.selectors.PrefixTopicSelector;
import io.axual.connect.plugins.kafka.selectors.SourceTopicSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;

import static io.axual.connect.plugins.kafka.sink.KafkaSinkConfig.*;
import static org.junit.jupiter.api.Assertions.*;

class KafkaSinkConfigTest {
    public static final String STATIC_HEADER_ALIAS_1 = "static_header_alias1";
    public static final String STATIC_HEADER_ALIAS_2 = "static_header_alias2";
    public static final String STATIC_HEADER_ALIAS_3 = "static_header_alias3";
    public static final String STATIC_HEADER_KEY_1 = "staticHeaderKey1";
    public static final String STATIC_HEADER_KEY_2 = "staticHeaderKey2";
    public static final String STATIC_HEADER_KEY_3 = "staticHeaderKey3";
    public static final String STATIC_HEADER_VALUE_1 = "staticHeaderValue1";
    public static final String STATIC_HEADER_VALUE_2 = "staticHeaderValue2";
    public static final String STATIC_HEADER_VALUE_3 = "staticHeaderValue3";
    private static final String REMOTE_PREFIX = "remote";
    private static final String CONFIG_REMOTE_SEPARATOR = ".";
    private static final String GROUP_REMOTE_SEPARATOR = ": ";
    private static final String EXPECTED_BOOTSTRAP_SERVER = "someserver:1234";
    private static final String EXPECTED_KEY_SERIALIZER = ByteArraySerializer.class.getName();
    private static final String EXPECTED_VALUE_SERIALIZER = ByteArraySerializer.class.getName();
    private static final String EXTRA_REMOTE_CONFIG = "example.extra.config";
    private static final String EXTRA_REMOTE_DATA = "Testing extra data";

    static Stream<Arguments> partitionSelector() {
        return Stream.of(
                Arguments.of("Source", Collections.singletonMap(PARTITION_SELECTOR_CONFIG, PARTITION_SELECTOR_SOURCE), PARTITION_SELECTOR_SOURCE),
                Arguments.of("Partitioner", Collections.singletonMap(PARTITION_SELECTOR_CONFIG, PARTITION_SELECTOR_PARTITIONER), PARTITION_SELECTOR_PARTITIONER),
                Arguments.of("Default", Collections.emptyMap(), PARTITION_SELECTOR_SOURCE)
        );
    }

    static Stream<Arguments> topicSelector() {
        return Stream.of(
                Arguments.of("Source", Collections.singletonMap(TOPIC_SELECTOR_CONFIG, TopicSelector.Type.source.name()), TopicSelector.Type.source.name()),
                Arguments.of("Prefix", Collections.singletonMap(TOPIC_SELECTOR_CONFIG, TopicSelector.Type.prefix.name()), TopicSelector.Type.prefix.name()),
                Arguments.of("Fixed", Collections.singletonMap(TOPIC_SELECTOR_CONFIG, TopicSelector.Type.fixed.name()), TopicSelector.Type.fixed.name()),
                Arguments.of("Default", Collections.emptyMap(), TopicSelector.Type.source.name())
        );
    }

    static Stream<Arguments> topicSelectorClass() {
        return Stream.of(
                Arguments.of("Source", Collections.singletonMap(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG, SourceTopicSelector.class.getName()), SourceTopicSelector.class),
                Arguments.of("Prefix", Collections.singletonMap(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG, PrefixTopicSelector.class.getName()), PrefixTopicSelector.class),
                Arguments.of("Fixed", Collections.singletonMap(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG, FixedTopicSelector.class.getName()), FixedTopicSelector.class),
                Arguments.of("Default", Collections.emptyMap(), KafkaSinkConfig.TOPIC_SELECTOR_CLASS_DEFAULT)
        );
    }

    private Map<String, String> configs;

    @BeforeEach
    void setupMinimalConfig() {
        // set up minimal configs for remote producer
        configs = new HashMap<>();
        configs.put(addConfigPrefix(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG), EXPECTED_BOOTSTRAP_SERVER);
    }

    @Test
    void verify_configDef_Remote() {
        final ConfigDef producerConfigDef = ProducerConfig.configDef();
        final ConfigDef sinkConfig = KafkaSinkConfig.configDef();

        for (final String key : producerConfigDef.names()) {
            // key and value serializers should be removed from sink configdef, so skip the check of these
            if (ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG.equals(key) || ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG.equals(key)) {
                continue;
            }
            if (ProducerConfig.BOOTSTRAP_SERVERS_CONFIG.equals(key) ) {
                continue;
            }

            ConfigDef.ConfigKey producerConfigKey = producerConfigDef.configKeys().get(key);

            final String sinkKey = addConfigPrefix(key);
            assertTrue(sinkConfig.configKeys().containsKey(sinkKey), String.format("Key %s should be present", sinkKey));
            ConfigDef.ConfigKey sinkConfigKey = sinkConfig.configKeys().get(sinkKey);

            final ConfigDef.Type expectedType = producerConfigKey.type == ConfigDef.Type.CLASS ? ConfigDef.Type.STRING : producerConfigKey.type;
            final Object expectedDefault;
            if (producerConfigKey.defaultValue instanceof Collection<?>) {
                // loop
                expectedDefault = ((Collection<?>) producerConfigKey.defaultValue).stream()
                        .map(value -> value instanceof Class<?> ? ((Class) value).getName() : value)
                        .toList();
            } else if (producerConfigKey.defaultValue instanceof Class<?>) {
                expectedDefault = ((Class<?>) producerConfigKey.defaultValue).getName();
            } else {
                expectedDefault = producerConfigKey.defaultValue;
            }

            assertEquals(addConfigPrefix(producerConfigKey.name), sinkConfigKey.name, sinkKey + ".name not the same");
            assertEquals(expectedType, sinkConfigKey.type, sinkKey + ".type not the same");
            assertEquals(producerConfigKey.documentation, sinkConfigKey.documentation, sinkKey + ".documentation not the same");
            assertEquals(producerConfigKey.importance, sinkConfigKey.importance, sinkKey + ".importance not the same");
            assertEquals(expectedDefault, sinkConfigKey.defaultValue, sinkKey + ".defaultValue not the same");
            assertEquals(producerConfigKey.displayName, sinkConfigKey.displayName, sinkKey + ".displayName not the same");
            assertEquals(addGroupPrefix(producerConfigKey.group), sinkConfigKey.group, sinkKey + ".group not the same");
        }
    }

    private String addConfigPrefix(String field) {
        if (field == null) {
            return REMOTE_PREFIX + CONFIG_REMOTE_SEPARATOR;
        }
        return REMOTE_PREFIX + CONFIG_REMOTE_SEPARATOR + field;
    }

    private String addGroupPrefix(String field) {
        if (field == null) {
            return REMOTE_PREFIX;
        }
        return REMOTE_PREFIX + GROUP_REMOTE_SEPARATOR + field;
    }

    @Test
    void getRemoteConfig() {
        configs.put(addConfigPrefix(EXTRA_REMOTE_CONFIG), EXTRA_REMOTE_DATA);

        KafkaSinkConfig sinkConfig = new KafkaSinkConfig(configs);
        assertNotNull(sinkConfig);

        Map<String, Object> remoteConfigs = sinkConfig.getRemoteConfig();
        assertEquals(EXPECTED_BOOTSTRAP_SERVER, remoteConfigs.get(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG));
        assertEquals(EXPECTED_KEY_SERIALIZER, remoteConfigs.get(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG));
        assertEquals(EXPECTED_VALUE_SERIALIZER, remoteConfigs.get(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG));
        assertEquals(EXTRA_REMOTE_DATA, remoteConfigs.get(EXTRA_REMOTE_CONFIG));
    }

    @Test
    void getStaticHeaders_NoneSet() {
        KafkaSinkConfig connectorConfig = new KafkaSinkConfig(configs);
        Map<String, String> staticHeaders = connectorConfig.getStaticHeaders();
        assertNotNull(staticHeaders);
        assertTrue(staticHeaders.isEmpty());
    }

    @Test
    void getStaticHeaders_Set() {
        configs.put(STATIC_HEADER_ALIAS_CONFIG, String.join(",", STATIC_HEADER_ALIAS_1, STATIC_HEADER_ALIAS_2, STATIC_HEADER_ALIAS_3));

        configs.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_KEY_1);
        configs.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_VALUE_1);

        configs.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_KEY_2);
        configs.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_VALUE_2);

        configs.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_3), STATIC_HEADER_KEY_3);
        configs.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_3), STATIC_HEADER_VALUE_3);

        KafkaSinkConfig connectorConfig = new KafkaSinkConfig(configs);
        Map<String, String> staticHeaders = connectorConfig.getStaticHeaders();
        assertNotNull(staticHeaders);
        assertFalse(staticHeaders.isEmpty());

        assertTrue(staticHeaders.containsKey(STATIC_HEADER_KEY_1));
        assertTrue(staticHeaders.containsKey(STATIC_HEADER_KEY_2));
        assertTrue(staticHeaders.containsKey(STATIC_HEADER_KEY_3));

        assertEquals(STATIC_HEADER_VALUE_1, staticHeaders.get(STATIC_HEADER_KEY_1));
        assertEquals(STATIC_HEADER_VALUE_2, staticHeaders.get(STATIC_HEADER_KEY_2));
        assertEquals(STATIC_HEADER_VALUE_3, staticHeaders.get(STATIC_HEADER_KEY_3));
    }

    @Test
    void getStaticHeaders_FieldNotSet() {
        configs.put(STATIC_HEADER_ALIAS_CONFIG, String.join(",", STATIC_HEADER_ALIAS_1, STATIC_HEADER_ALIAS_2, STATIC_HEADER_ALIAS_3));

        configs.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_KEY_1);
        configs.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_VALUE_1);

        configs.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_KEY_2);
        configs.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_VALUE_2);

        assertThrows(ConfigException.class, () -> new KafkaSinkConfig(configs));
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("partitionSelector")
    void partitionSelector_valid(String name, Map<String, String> extraConfigs, String expectedSelector) {
        configs.putAll(extraConfigs);

        KafkaSinkConfig kafkaSinkConfig = new KafkaSinkConfig(configs);
        assertEquals(expectedSelector, kafkaSinkConfig.getPartitionSelector());
    }

    @Test
    void partitionSelector_Unknown() {
        configs.put(PARTITION_SELECTOR_CONFIG, "Unknown");

        assertThrows(ConfigException.class, () -> new KafkaSinkConfig(configs));
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("topicSelector")
    void topicSelector_valid(String name, Map<String, String> extraConfigs, String expectedSelector) {
        configs.putAll(extraConfigs);

        KafkaSinkConfig kafkaSinkConfig = new KafkaSinkConfig(configs);
        assertEquals(expectedSelector, kafkaSinkConfig.getTopicSelector());
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("topicSelectorClass")
    void topicSelectorClass_valid(String name, Map<String, String> extraConfigs, Class<?> clazz) {
        configs.putAll(extraConfigs);

        KafkaSinkConfig kafkaSinkConfig = new KafkaSinkConfig(configs);
        assertEquals(clazz, kafkaSinkConfig.getTopicSelectorClass());
    }

    @Test
    void topicSelector_Unknown() {
        configs.put(TOPIC_SELECTOR_CONFIG, "Unknown");

        assertThrows(ConfigException.class, () -> new KafkaSinkConfig(configs));
    }

    @Test
    void topicSelectorClass_Unknown() {
        configs.put(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG, "Unknown");

        assertThrows(ConfigException.class, () -> new KafkaSinkConfig(configs));
    }

    @Test
    void metadataHeadersNotSpecified_returnDefaults() {
        // given a config with no forwarding header names set
        KafkaSinkConfig kafkaSinkConfig = new KafkaSinkConfig(configs);

        // it will return the default forwarding header names
        assertEquals(Optional.of(FORWARD_TOPIC_HEADER_NAME_DEFAULT), kafkaSinkConfig.getTopicHeader());
        assertEquals(Optional.of(FORWARD_PARTITION_HEADER_NAME_DEFAULT), kafkaSinkConfig.getPartitionHeader());
        assertEquals(Optional.of(FORWARD_OFFSET_HEADER_NAME_DEFAULT), kafkaSinkConfig.getOffsetHeader());
        assertEquals(Optional.of(FORWARD_TIMESTAMP_HEADER_NAME_DEFAULT), kafkaSinkConfig.getTimestampHeader());
    }

    @Test
    void metadataHeadersSet_returnConfiguredValues() {
        // given a config with no forwarding header names set
        configs.put(FORWARD_TOPIC_HEADER_NAME_CONFIG, "X-Topic");
        configs.put(FORWARD_PARTITION_HEADER_NAME_CONFIG, "X-Partition");
        configs.put(FORWARD_OFFSET_HEADER_NAME_CONFIG, "X-Offset");
        configs.put(FORWARD_TIMESTAMP_HEADER_NAME_CONFIG, "X-Timestamp");
        KafkaSinkConfig kafkaSinkConfig = new KafkaSinkConfig(configs);

        // it will return the default forwarding header names
        assertEquals(Optional.of("X-Topic"), kafkaSinkConfig.getTopicHeader());
        assertEquals(Optional.of("X-Partition"), kafkaSinkConfig.getPartitionHeader());
        assertEquals(Optional.of("X-Offset"), kafkaSinkConfig.getOffsetHeader());
        assertEquals(Optional.of("X-Timestamp"), kafkaSinkConfig.getTimestampHeader());
    }

    @Test
    void metadataHeadersSetToNull_returnEmptyOptional() {
        // given a config with no forwarding header names set
        configs.put(FORWARD_TOPIC_HEADER_NAME_CONFIG, null);
        configs.put(FORWARD_PARTITION_HEADER_NAME_CONFIG, null);
        configs.put(FORWARD_OFFSET_HEADER_NAME_CONFIG, null);
        configs.put(FORWARD_TIMESTAMP_HEADER_NAME_CONFIG, null);
        KafkaSinkConfig kafkaSinkConfig = new KafkaSinkConfig(configs);

        // it will return empty optionals
        assertEquals(Optional.empty(), kafkaSinkConfig.getTopicHeader());
        assertEquals(Optional.empty(), kafkaSinkConfig.getPartitionHeader());
        assertEquals(Optional.empty(), kafkaSinkConfig.getOffsetHeader());
        assertEquals(Optional.empty(), kafkaSinkConfig.getTimestampHeader());
    }

    @Test
    @DisplayName("Producer key and value serializers are removed from configdef")
    void keyAndValueSerializerSettingsAreRemoved() {
        final ConfigDef sinkConfigDef = KafkaSinkConfig.configDef();
        final Set<String> remoteProducerConfigs = sinkConfigDef.configKeys().keySet().stream()
                .filter(key -> key.startsWith("remote."))
                .collect(Collectors.toSet());
        assertFalse(remoteProducerConfigs.contains("remote.key.serializer"), "key serializer config is still present");
        assertFalse(remoteProducerConfigs.contains("remote.value.serializer"), "value serializer config is still present");
    }

    @Test
    void testGetRemoteConfig_should_stripRemotePrefixFromConfigs() {
        KafkaSinkConfig kafkaSinkConfig = new KafkaSinkConfig(this.configs);

        Map<String, Object> remoteConfig = kafkaSinkConfig.getRemoteConfig();

        assertEquals(EXPECTED_BOOTSTRAP_SERVER, remoteConfig.get(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG));
        assertEquals(EXPECTED_KEY_SERIALIZER, remoteConfig.get(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG));
        assertEquals(EXPECTED_VALUE_SERIALIZER, remoteConfig.get(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG));
    }
}
