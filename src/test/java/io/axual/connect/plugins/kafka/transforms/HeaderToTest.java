package io.axual.connect.plugins.kafka.transforms;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.header.Header;
import org.apache.kafka.connect.header.Headers;
import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import static io.axual.connect.plugins.kafka.transforms.HeaderTo.HEADER_NAME_CONFIG;
import static io.axual.connect.plugins.kafka.transforms.HeaderTo.HEADER_REMOVE_CONFIG;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class HeaderToTest {

    private static final String TARGET_HEADER_NAME = "toCopy";

    @Test
    void config_TestDefinitions() {
        HeaderTo<SinkRecord> transformer = new HeaderTo.Key<>();
        ConfigDef configDef = assertDoesNotThrow(transformer::config);
        assertNotNull(configDef);
        assertTrue(configDef.configKeys().containsKey(HEADER_NAME_CONFIG));
        assertTrue(configDef.configKeys().containsKey(HEADER_REMOVE_CONFIG));
    }

    @Test
    void apply_WithoutConfiguring() {
        HeaderTo<SinkRecord> transformer = new HeaderTo.Key<>();
        SinkRecord testRecord = new SinkRecord("topic", 1, Schema.INT32_SCHEMA, 2, Schema.INT64_SCHEMA, 3L, 4L);
        assertThrows(IllegalStateException.class, () -> transformer.apply(testRecord));
    }

    @Test
    void configure_Fail_EmptyMap() {
        HeaderTo<SinkRecord> transformer = new HeaderTo.Key<>();
        Map<String, String> config = new HashMap<>();
        assertThrows(ConfigException.class, () -> transformer.configure(config));
    }

    @Test
    void configure_Fail_EmptyHeaderName() {
        HeaderTo<SinkRecord> transformer = new HeaderTo.Key<>();
        Map<String, String> config = new HashMap<>();
        config.put(HEADER_NAME_CONFIG, "");
        assertThrows(ConfigException.class, () -> transformer.configure(config));
    }

    @Test
    void configure_Fail_InvalidBoolean() {
        HeaderTo<SinkRecord> transformer = new HeaderTo.Key<>();
        Map<String, String> config = new HashMap<>();
        config.put(HEADER_NAME_CONFIG, "notEmpty");
        config.put(HEADER_REMOVE_CONFIG, "invalidBoolean");
        assertThrows(ConfigException.class, () -> transformer.configure(config));
    }

    @ParameterizedTest(name = "{0}")
    @CsvSource(value = {
            "Key transform   - with header    - remove header, true, true, true",
            "Key transform   - with header    - keep header, true, true, false",
            "Key transform   - without header - remove header, true, false, true",
            "Key transform   - without header - keep header, true, false, false",
            "Value transform - with header    - remove header, false, true, true",
            "Value transform - with header    - keep header, false, true, false",
            "Value transform - without header - remove header, false, false, true",
            "Value transform - without header - keep header, false, false, false",
    })
    void apply_TransformationTest(String testName, boolean forKey, boolean withHeader, boolean removeHeader) {
        log.info("Running transformation test : {}", testName);

        // Define schema type and content for the target header
        final Schema targetHeaderSchema = Schema.STRING_SCHEMA;
        final String targetHeaderValue = "expectedContent";

        // Create the test data
        final Headers inputHeaders = createInputHeaders(withHeader, targetHeaderSchema, targetHeaderValue);
        final SinkRecord inputRecord = new SinkRecord("TestTopic", 2, Schema.INT32_SCHEMA, 3, Schema.INT64_SCHEMA, 4L, 5L, 6L, TimestampType.CREATE_TIME, inputHeaders);

        // Create the expected result
        final SinkRecord expectedRecord = createExpectedOutput(inputRecord, forKey, removeHeader);

        // Create header configuration
        final Map<String, String> transformationConfig = new HashMap<>();
        transformationConfig.put(HEADER_NAME_CONFIG, TARGET_HEADER_NAME);
        transformationConfig.put(HEADER_REMOVE_CONFIG, Boolean.toString(removeHeader));

        // create HeaderTo Transform and configure
        final HeaderTo<SinkRecord> transform = forKey ? new HeaderTo.Key<>() : new HeaderTo.Value<>();
        transform.configure(transformationConfig);

        // apply transformation
        final SinkRecord transformedRecord = assertDoesNotThrow(() -> transform.apply(inputRecord));

        // Verify contents
        assertEquals(expectedRecord, transformedRecord);
    }

    // Create a collection of headers to use in the test, The expected target header can be added optionally
    Headers createInputHeaders(boolean withTargetHeader, Schema schema, String value) {
        final Headers headerCollection = new ConnectHeaders()
                .add("Header 1", "First header, first time", Schema.STRING_SCHEMA)
                .add("Header 1", "First header, second time", Schema.STRING_SCHEMA)
                .add("Header 1", "First header, third time", Schema.STRING_SCHEMA)
                .add("Header 2", "Second header", Schema.STRING_SCHEMA)
                .add("Header 3", "Third header", Schema.STRING_SCHEMA)
                .add("Header 4", "Fourth header", Schema.STRING_SCHEMA);

        if (withTargetHeader) {
            headerCollection
                    .add(TARGET_HEADER_NAME, "NotExpectedContent", schema)
                    .add(TARGET_HEADER_NAME, "AlsoNotExpectedContent", schema)
                    .add(TARGET_HEADER_NAME, value, schema);

        }
        return headerCollection;
    }

    // Construct the expected transformed record based on the input record and configuration
    SinkRecord createExpectedOutput(SinkRecord inputRecord, boolean forKey, boolean removeHeader) {
        final Header targetHeader = inputRecord.headers().lastWithName(TARGET_HEADER_NAME);
        if (targetHeader == null) {
            // Return the input as the header is not found.
            return inputRecord;
        }

        final Headers expectedHeaders = inputRecord.headers().duplicate();
        if (removeHeader) {
            // Remove the header from the expected header list
            expectedHeaders.remove(TARGET_HEADER_NAME);
        }
        if (forKey) {
            // Create a copy of the inputRecord, but with the key arguments replaced with the values from the target header
            return inputRecord.newRecord(inputRecord.topic(), inputRecord.kafkaPartition(), targetHeader.schema(), targetHeader.value(), inputRecord.valueSchema(), inputRecord.value(), inputRecord.timestamp(), expectedHeaders);
        } else {
            // Create a copy of the inputRecord, but with the value arguments replaced with the values from the target header
            return inputRecord.newRecord(inputRecord.topic(), inputRecord.kafkaPartition(), inputRecord.keySchema(), inputRecord.key(), targetHeader.schema(), targetHeader.value(), inputRecord.timestamp(), expectedHeaders);
        }
    }
}