package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.Config;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.axual.connect.plugins.kafka.selectors.FixedTopicSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import io.axual.connect.plugins.kafka.sink.KafkaSinkConfig;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static io.axual.connect.plugins.kafka.util.ConverterVerifierTest.getConverterConfigs;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class KafkaSourceConnectorTest {

    private KafkaSourceConnector sourceConnector;

    @BeforeEach
    void createFixtures() {
        sourceConnector = new KafkaSourceConnector();
    }

    @Test
    void start_shouldNotThrowException() {
        assertDoesNotThrow(() -> sourceConnector.start(new HashMap<>()), "connector should start without throwing exceptions");
    }

    @Test
    void testCorrectTaskClass() {
        assertEquals(KafkaSourceTask.class, sourceConnector.taskClass());
    }

    @Test
    void versionIsReadFromConfigfile() {
        assertEquals("testing", sourceConnector.version());
    }

    @Test
    void provideCorrectConfigs() {
        final ConfigDef config = sourceConnector.config();
        assertNotNull(config);
    }

    @Test
    void taskConfigsReturnsCorrectNumberOfConfigs() {
        final List<Map<String, String>> configs = sourceConnector.taskConfigs(5);
        assertEquals(5, configs.size(), "Should return one config per task");
        configs.forEach(
                config -> assertTrue(config.containsKey(KafkaSourceTask.TASK_ID_CONFIG), "task.id should be configured"));
    }

    @Test
    void connectorNameIsSetInClientId() {
        // given a config with a connector name and default empty remote client id
        Map<String, String> connectorConfig = new HashMap<>();
        connectorConfig.put("name", "unit-test");
        connectorConfig.put("remote.client.id", "");

        // when the connector is started and we ask for the task configs
        sourceConnector.start(connectorConfig);
        final List<Map<String, String>> configs = sourceConnector.taskConfigs(1);

        // the remote client id in the config is set to connector name plus task index
        assertEquals("unit-test-0", configs.get(0).get("remote.client.id"));
    }

    @Test
    void validate_embedsTopicSelectorConfigDef() {
        // given a configuration with a topicSelector configured, but no config for that selector
        Map<String, String> configs = new HashMap<>();
        configs.put(KafkaSinkConfig.TOPIC_SELECTOR_CONFIG, TopicSelector.Type.fixed.name());

        // when the connector validates the config
        final Config validateResult = sourceConnector.validate(configs);

        // the result contains an error, originating from the configDef from the configured topicselector
        final ConfigValue configValue = validateResult.configValues().stream()
                .filter(cv -> cv.name().startsWith("topic.selector.target"))
                .findFirst().get();
        assertEquals(1, configValue.errorMessages().size());
        assertEquals("Missing required configuration \"topic.selector.target\" which has no default value.", configValue.errorMessages().get(0));
    }

    @Test
    void validate_embedsTopicSelectorClassConfigDef() {
        // given a configuration with a topicSelector configured, but no config for that selector
        Map<String, String> configs = new HashMap<>();
        configs.put(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG, FixedTopicSelector.class.getName());

        // when the connector validates the config
        final Config validateResult = sourceConnector.validate(configs);

        // the result contains an error, originating from the configDef from the configured topicselector
        final ConfigValue configValue = validateResult.configValues().stream()
                .filter(cv -> cv.name().startsWith("topic.selector.target"))
                .findFirst().get();
        assertEquals(1, configValue.errorMessages().size());
        assertEquals("Missing required configuration \"topic.selector.target\" which has no default value.", configValue.errorMessages().get(0));
    }

    @ParameterizedTest
    @ValueSource(strings = {"key.converter", "value.converter", "header.converter"})
    @DisplayName("a missing converter is reported")
    void missingConverterIsReported(String missingConverter) {
        // given a config where a converter is not specified
        Map<String, String> configs = getConverterConfigs();
        configs.remove(missingConverter);

        // when the config is validated
        final Config validated =  sourceConnector.validate(configs);

        // an error is reported for the missing key
        final Optional<ConfigValue> configValue = validated.configValues().stream()
                .filter(cv -> cv.name().startsWith(missingConverter))
                .findFirst();
        assertTrue(configValue.isPresent(), "an error is reported  for the missing converter " + missingConverter);

    }

}
