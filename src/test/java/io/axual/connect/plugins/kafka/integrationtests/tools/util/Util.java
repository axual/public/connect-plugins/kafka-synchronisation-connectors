package io.axual.connect.plugins.kafka.integrationtests.tools.util;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.UUID;

@Slf4j
public class Util {

    public static ProducerRecord<byte[], byte[]> generateRandomProducerRecord(String resolvedTopicName, Integer partition) {
        final String K_FORMAT = "Key topic %s %s";
        final String V_FORMAT = "Value topic %s %s";
        return new ProducerRecord<>(
                        resolvedTopicName,
                        partition,
                        String.format(K_FORMAT, resolvedTopicName, UUID.randomUUID()).getBytes(),
                        String.format(V_FORMAT, resolvedTopicName, UUID.randomUUID()).getBytes()
        );
    }
}
