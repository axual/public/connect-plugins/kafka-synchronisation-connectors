package io.axual.connect.plugins.kafka.integrationtests;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.integrationtests.tools.providers.StartableKafkaCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.StartableKafkaConnectCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers.TestcontainerKafkaCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers.TestcontainerKafkaConnectCluster;
import io.axual.connect.plugins.kafka.selectors.FixedTopicSelector;
import io.axual.connect.plugins.kafka.selectors.MappingTopicSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import io.axual.connect.plugins.kafka.source.KafkaSourceConfigDefinitions;
import io.axual.connect.plugins.kafka.source.KafkaSourceConnector;
import io.axual.connect.plugins.kafka.util.ConverterVerifierTest;
import lombok.val;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.connect.runtime.ConnectorConfig;
import org.apache.kafka.connect.runtime.SourceConnectorConfig;
import org.apache.kafka.connect.runtime.distributed.DistributedConfig;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
class KafkaSourceIT {

    private static final Logger log = LoggerFactory.getLogger(KafkaSourceIT.class);
    private static final String CONNECTOR_NAME = "passthrough-test";

    @Container
    StartableKafkaConnectCluster local = new TestcontainerKafkaConnectCluster("local");

    @Container
    StartableKafkaCluster remote = new TestcontainerKafkaCluster("remote");

    @Test
    void remoteClusterIsCopiedToLocal() {
        // with two topics on remote and one topic on the local
        remote.createTopic("topic1");
        remote.createTopic("topic2");
        local.createTopic("topic-out");

        // and a connector that listens on both topics and produces on local
        setupConnector("topic1,topic2", "topic-out");

        // given that we successfully produce on topic1 and topic2 on the remote
        assertTrue(produceRecords(5, "topic1"), "Producing test messages on topic1 failed!");
        assertTrue(produceRecords(5, "topic2"), "Producing test messages on topic2 failed!");

        // whe should be able to consume all messages
        final ConsumerRecords<byte[], byte[]> consumed = local.consume(10, SECONDS.toMillis(300), "topic-out");
        assertEquals(10, consumed.count(), "Should be able to read 10 records");

        // quick and dirty: visual inspection
        consumed.forEach(consumerRecord -> System.out.printf("key: %s, value: %s%n", new String(consumerRecord.key()), new String(consumerRecord.value())));
    }

    @Test
    void remoteClusterIsCopiedToLocalWithMapping() {
        // with two topics on remote and one topic on the local
        remote.createTopic("topic1");
        remote.createTopic("topic2");
        local.createTopic("topic-out");

        // and a connector that listens on both topics and produces on local, using a mapping from source to target topic
        Map<String, String> topicMapping = new HashMap<>();
        topicMapping.put("topic1", "topic-out");
        topicMapping.put("topic2", "topic-out");
        setupConnector("topic1,topic2", topicMapping);

        // given that we successfully produce on topic1 and topic2 on the remote
        assertTrue(produceRecords(5, "topic1"), "Producing test messages on topic1 failed!");
        assertTrue(produceRecords(5, "topic2"), "Producing test messages on topic2 failed!");

        // whe should be able to consume all messages
        final ConsumerRecords<byte[], byte[]> consumed = local.consume(10, SECONDS.toMillis(300), "topic-out");
        assertEquals(10, consumed.count(), "Should be able to read 10 records");

        // quick and dirty: visual inspection
        consumed.forEach(consumerRecord -> System.out.printf("key: %s, value: %s%n", new String(consumerRecord.key()), new String(consumerRecord.value())));
    }

    @Test
    void remoteClusterIsCopiedToLocalWithMappingWhileGroupRebalancing() throws InterruptedException {
        // with two topics on remote and one topic on the local
        remote.createTopic("topic1", 3);
        remote.createTopic("topic2", 3);
        local.createTopic("topic-out", 3);

        // and a connector that listens on both topics and produces on local, using a mapping from source to target topic
        Map<String, String> topicMapping = new HashMap<>();
        topicMapping.put("topic1", "topic-out");
        topicMapping.put("topic2", "topic-out");

        final String commonGroupId = getClass().getSimpleName() + "-" + UUID.randomUUID();
        setupConnector("topic1,topic2", topicMapping, commonGroupId);
        // given that we successfully produce on topic1 and topic2 on the remote
        assertTrue(produceRecords(1000, "topic1"), "Producing test messages on topic1 failed!");
        assertTrue(produceRecords(1000, "topic2"), "Producing test messages on topic1 failed!");
        Thread.sleep(5000);

        //intruding consumer to the same consumer group of connector's remote
        Thread extConsumerThread = new Thread(() -> remote.consume(500, SECONDS.toMillis(10), commonGroupId, "topic1"));
        extConsumerThread.start();

        assertTrue(produceRecords(1000, "topic1"), "Producing test messages on topic1 failed!");
        assertTrue(produceRecords(1000, "topic2"), "Producing test messages on topic2 failed!");

        // whe should be able to consume all messages
        final ConsumerRecords<byte[], byte[]> consumed = local.consume(5000, SECONDS.toMillis(10), "topic-out");
        assertTrue(4000 <= consumed.count(), "Should at least be 4000 records");

        // quick and dirty: visual inspection
        consumed.forEach(consumerRecord -> System.out.printf("key: %s, value: %s%n", new String(consumerRecord.key()), new String(consumerRecord.value())));
    }

    private boolean produceRecords(int count, String topicName) {
        log.info("produceRecords({},{})", count, topicName);
        val records = createProducerRecords(count, topicName);
        return produceMessagesOnRemote(records);
    }

    /**
     * Produce the given list of ProducerRecords on the remote.
     *
     * @return true is the produce was successful.
     */
    private boolean produceMessagesOnRemote(List<ProducerRecord<byte[], byte[]>> records) {
        log.info("produceMessagesOnRemote({} records)", records.size());
        final Map<String, Object> props = remote.clientConnectionProperties();
        final String uuid = UUID.randomUUID().toString();
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, getClass().getSimpleName() + "-" + uuid);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, 1);

        try (KafkaProducer<byte[], byte[]> producer = new KafkaProducer<>(props)) {
            for (ProducerRecord<byte[], byte[]> producerRecord : records) {
                producer.send(producerRecord).get();
            }
            log.info("{} messages produced successfully", records.size());
            return true;
        } catch (InterruptedException | ExecutionException e) {
            log.info("message produce failed", e);
            return false;
        }
    }

    /**
     * Create producer records for the given topic.
     *
     * @param count     number of records to create.
     * @param topicName topic where the records will be produced.
     * @return the producer records.
     */
    private List<ProducerRecord<byte[], byte[]>> createProducerRecords(int count, String topicName) {
        return IntStream.range(0, count)
                .mapToObj(nr -> new ProducerRecord<>(
                        topicName, (topicName + "-key" + nr).getBytes(UTF_8), (topicName + "-value" + nr).getBytes(UTF_8)
                ))
                .toList();
    }

    /**
     * Set up the test Connector.
     *
     * @param sourceTopics comma separated topics to listen on.
     * @param targetTopic  topic where messages are sent to.
     */
    private void setupConnector(String sourceTopics, String targetTopic) {
        Map<String, String> config = ConverterVerifierTest.getConverterConfigs();
        config.put(SourceConnectorConfig.NAME_CONFIG, CONNECTOR_NAME);
        config.put(SourceConnectorConfig.CONNECTOR_CLASS_CONFIG, KafkaSourceConnector.class.getName());
        remote.consumerProperties().forEach((key, value) -> {
            if (value != null && !key.endsWith(".deserializer")) {
                // do not configure deserializers, as these are hardcoded in KafkaSourceConfig.
                config.put(KafkaSourceConfigDefinitions.REMOTE_CONFIG_PREFIX + key, value.toString());
            }
        });
        config.put("remote.group.id", getClass().getSimpleName() + "-" + UUID.randomUUID());

        config.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, sourceTopics);
        config.put(KafkaSourceConfigDefinitions.TOPIC_SELECTOR_CLASS_CONFIG, FixedTopicSelector.class.getName());
        config.put(TopicSelector.TOPIC_SELECTOR_CONFIG + ".target", targetTopic);
        config.put(KafkaSourceConfigDefinitions.OFFSET_HEADER_NAME_CONFIG, "X-orig-offset");
        config.put(KafkaSourceConfigDefinitions.PARTITION_HEADER_NAME_CONFIG, "X-orig-part");
        config.put(KafkaSourceConfigDefinitions.TIMESTAMP_HEADER_NAME_CONFIG, "X-orig-time");

        local.connectClient().createConnector(CONNECTOR_NAME, config);
    }

    /**
     * Set up the test Connector.
     *
     * @param sourceTopics  comma separated topics to listen on.
     * @param topicMappings topic mappings from source to target topic.
     */
    private void setupConnector(String sourceTopics, Map<String, String> topicMappings) {
        Map<String, String> config = ConverterVerifierTest.getConverterConfigs();
        config.put(SourceConnectorConfig.NAME_CONFIG, CONNECTOR_NAME);
        config.put(SourceConnectorConfig.CONNECTOR_CLASS_CONFIG, KafkaSourceConnector.class.getName());
        remote.consumerProperties().forEach((key, value) -> {
            if (value != null && !key.endsWith(".deserializer")) {
                // do not configure deserializers, as these are hardcoded in KafkaSourceConfig.
                config.put(KafkaSourceConfigDefinitions.REMOTE_CONFIG_PREFIX + key, value.toString());
            }
        });
        config.put("remote.group.id", getClass().getSimpleName() + "-" + "xaz1");

        config.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, sourceTopics);
        config.put(KafkaSourceConfigDefinitions.TOPIC_SELECTOR_CLASS_CONFIG, MappingTopicSelector.class.getName());
        topicMappings.forEach((source, target) -> config.put("topic.selector.mapping." + source, target));

        config.put(KafkaSourceConfigDefinitions.OFFSET_HEADER_NAME_CONFIG, "X-orig-offset");
        config.put(KafkaSourceConfigDefinitions.PARTITION_HEADER_NAME_CONFIG, "X-orig-part");
        config.put(KafkaSourceConfigDefinitions.TIMESTAMP_HEADER_NAME_CONFIG, "X-orig-time");

        local.connectClient().createConnector(CONNECTOR_NAME, config);
    }

    /**
     * Set up the test Connector.
     *
     * @param sourceTopics  comma separated topics to listen on.
     * @param topicMappings topic mappings from source to target topic.
     * @param groupId       The group ID of the consumers
     */
    private void setupConnector(String sourceTopics, Map<String, String> topicMappings, String groupId) {
        Map<String, String> config = ConverterVerifierTest.getConverterConfigs();
        config.put(SourceConnectorConfig.NAME_CONFIG, CONNECTOR_NAME);
        config.put(SourceConnectorConfig.CONNECTOR_CLASS_CONFIG, KafkaSourceConnector.class.getName());
        remote.consumerProperties().forEach((key, value) -> {
            if (value != null && !key.endsWith(".deserializer")) {
                // do not configure deserializers, as these are hardcoded in KafkaSourceConfig.
                config.put(KafkaSourceConfigDefinitions.REMOTE_CONFIG_PREFIX + key, value.toString());
            }
        });
        config.put("remote.group.id", groupId);

        config.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, sourceTopics);
        config.put(KafkaSourceConfigDefinitions.TOPIC_SELECTOR_CLASS_CONFIG, MappingTopicSelector.class.getName());
        topicMappings.forEach((source, target) -> config.put("topic.selector.mapping." + source, target));

        config.put(KafkaSourceConfigDefinitions.OFFSET_HEADER_NAME_CONFIG, "X-orig-offset");
        config.put(KafkaSourceConfigDefinitions.PARTITION_HEADER_NAME_CONFIG, "X-orig-part");
        config.put(KafkaSourceConfigDefinitions.TIMESTAMP_HEADER_NAME_CONFIG, "X-orig-time");

        //Using only two tasks with 3 partitions (in the test) so that, one task can let go of some partitions to
        //a new consumer in the group
        config.put(ConnectorConfig.TASKS_MAX_CONFIG, "2");

        //In case if the consumers are late to join back, we wait for 30 seconds instead of 1 minute
        //because it is better to fail fast that wait long
        config.put(DistributedConfig.REBALANCE_TIMEOUT_MS_CONFIG, "30000");
        local.connectClient().createConnector(CONNECTOR_NAME, config);
    }
}
