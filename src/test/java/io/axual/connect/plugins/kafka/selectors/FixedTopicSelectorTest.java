package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigValue;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.source.SourceRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FixedTopicSelectorTest {
    private static final String NEW_TARGET = "new-target";
    private static final String SOURCE_TOPIC = "test-topic-1";
    private static final String EXPECTED_TOPIC = NEW_TARGET;
    private static final SinkRecord TEST_SINK_RECORD_VALID = new SinkRecord(SOURCE_TOPIC, 0, null, null, null, null, 0L);
    private static final SinkRecord TEST_SINK_RECORD_NULL = null;
    private static final SinkRecord TEST_SINK_RECORD_TOPIC_NULL = new SinkRecord(null, 0, null, null, null, null, 0L);
    private static final SourceRecord TEST_SOURCE_RECORD_VALID = new SourceRecord(Collections.emptyMap(), Collections.emptyMap(), SOURCE_TOPIC, 0, null, null, null, null);
    private static final SourceRecord TEST_SOURCE_RECORD_NULL = null;
    private static final SourceRecord TEST_SOURCE_RECORD_TOPIC_NULL = new SourceRecord(Collections.emptyMap(), Collections.emptyMap(), null, 0, null, null, null, null);

    private TopicSelector topicSelector;

    private final Map<String, Object> validConfig = new HashMap<>();

    @BeforeEach
    void createFixture() {
        topicSelector = new FixedTopicSelector();
        validConfig.put(FixedTopicSelector.TOPIC_SELECTOR_TARGET, NEW_TARGET);
    }

    @Test
    void select_ValidSink() {
        topicSelector.configure(validConfig);
        assertEquals(EXPECTED_TOPIC, topicSelector.select(TEST_SINK_RECORD_VALID));
    }

    @Test
    void select_NotConfigured() {
        assertThrows(IllegalStateException.class, () -> topicSelector.select(TEST_SINK_RECORD_VALID));
    }

    @Test
    void select_NullSink() {
        topicSelector.configure(validConfig);
        assertEquals(EXPECTED_TOPIC, topicSelector.select(TEST_SINK_RECORD_VALID));
    }

    @Test
    void select_NullTopicSink() {
        topicSelector.configure(validConfig);
        assertEquals(EXPECTED_TOPIC, topicSelector.select(TEST_SINK_RECORD_TOPIC_NULL));
    }

    @Test
    void select_ValidSource() {
        topicSelector.configure(validConfig);
        assertEquals(EXPECTED_TOPIC, topicSelector.select(TEST_SOURCE_RECORD_VALID));
    }

    @Test
    void select_NullSource() {
        topicSelector.configure(validConfig);
        assertThrows(IllegalArgumentException.class, () -> topicSelector.select(TEST_SOURCE_RECORD_NULL));
    }

    @Test
    void select_NullTopicSource() {
        topicSelector.configure(validConfig);
        assertEquals(EXPECTED_TOPIC, topicSelector.select(TEST_SOURCE_RECORD_TOPIC_NULL));
    }

    @Test
    void testConfigValidationValid() {
        Map<String, String> config = new HashMap<>();
        config.put(FixedTopicSelector.TOPIC_SELECTOR_TARGET, "topic-out");
        final List<ConfigValue> configValues = topicSelector.configDef().validate(config);
        assertEquals(1, configValues.size());
        assertEquals("topic-out", configValues.get(0).value());
        assertEquals(0, configValues.get(0).errorMessages().size());
    }

    @Test
    void testConfigValidationInvalid() {
        Map<String, String> config = new HashMap<>();
        final List<ConfigValue> configValues = topicSelector.configDef().validate(config);
        assertEquals(1, configValues.size());
        assertEquals(1, configValues.get(0).errorMessages().size());
        assertEquals("Missing required configuration \"target\" which has no default value.", configValues.get(0).errorMessages().get(0));
    }
}
