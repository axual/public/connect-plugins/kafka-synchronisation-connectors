package io.axual.connect.plugins.kafka.integrationtests.tools;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

@Slf4j
public class PortAllocator {
    private static final int AUTO_ALLOCATE = 0;

    private static final Set<Integer> allocatedPorts = new HashSet<>();

    /**
     * Find a not yet allocated port and return it.
     * @return the port number.
     */
    public static Integer allocateSinglePort() {
        Integer result = null;
        while (result == null) {
            try (ServerSocket socket = new ServerSocket(AUTO_ALLOCATE)) {
                if (!allocatedPorts.contains(socket.getLocalPort())) {
                    result = socket.getLocalPort();
                    allocatedPorts.add(result);
                }
            } catch (IOException e) {
                throw new RuntimeException("Could not allocate free port", e);
            }
        }
        return result;
    }
}
