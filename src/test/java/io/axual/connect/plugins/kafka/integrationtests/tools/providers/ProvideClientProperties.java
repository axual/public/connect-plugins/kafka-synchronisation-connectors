package io.axual.connect.plugins.kafka.integrationtests.tools.providers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Maps;

import java.util.Map;

public interface ProvideClientProperties {
    String bootstrapServers();

    String bootstrapServers(String listenerName);

    Map<String, Object> clientConnectionProperties();

    Map<String, Object> clientConnectionProperties(String listenerName);

    default Map<String, String> clientConnectionStringProperties() {
        return propertiesToStringValue(clientConnectionProperties());
    }

    default Map<String, String> clientConnectionStringProperties(String listenerName) {
        return propertiesToStringValue(clientConnectionProperties(listenerName));
    }

    default Map<String, String> propertiesToStringValue(Map<String, Object> map) {
        return Maps.transformValues(map, obj -> obj == null ? null : obj.toString());
    }
}
