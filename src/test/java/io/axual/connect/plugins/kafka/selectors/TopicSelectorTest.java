package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TopicSelectorTest {

    @Test
    void createSelector_Source() {
        TopicSelector selector = TopicSelector.Type.valueOf("source").createInstance();
        assertEquals(SourceTopicSelector.class, selector.getClass());
    }

    @Test
    void createSelector_Prefix() {
        TopicSelector selector = TopicSelector.Type.valueOf("prefix").createInstance();
        assertEquals(PrefixTopicSelector.class, selector.getClass());
    }

    @Test
    void createSelector_Fixed() {
        TopicSelector selector = TopicSelector.Type.valueOf("fixed").createInstance();
        assertEquals(FixedTopicSelector.class, selector.getClass());
    }

    @Test
    void createSelector_Null() {
        assertThrows(NullPointerException.class, () -> TopicSelector.Type.valueOf(null).createInstance());
    }

    @Test
    void createSelector_Unknown() {
        assertThrows(IllegalArgumentException.class, () -> TopicSelector.Type.valueOf("unknown").createInstance());
    }

    @Test
    void select_EmptyTarget() {
        TopicSelector selector = new SourceTopicSelector();
        SinkRecord sinkRecord = new SinkRecord(" ", 0, null, null, null, null, 0L);

        assertThrows(IllegalStateException.class, () -> selector.select(sinkRecord));
    }

}
