package io.axual.connect.plugins.kafka.util;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2025 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigValue;
import org.apache.kafka.connect.converters.ByteArrayConverter;
import org.apache.kafka.connect.json.JsonConverter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConverterVerifierTest {

    @ParameterizedTest
    @ValueSource(strings = {"key.converter", "value.converter", "header.converter"})
    @DisplayName("a missing converter is reported")
    void missingConverterIsReported(String missingConverter) {
        // given a config where a converter is not specified
        Map<String, String> configs = getConverterConfigs();
        configs.remove(missingConverter);

        // when the config is validated
        List<ConfigValue> configValues = new ArrayList<>();
        ConverterVerifier.verifyConverters(configs, configValues);

        // an error is reported for the missing key
        assertEquals(1,  configValues.size(), "An error shuld be reported");
        ConfigValue configValue = configValues.get(0);
        assertEquals(missingConverter, configValue.name(), "The error is reported for the missing converter");
    }

    @ParameterizedTest
    @ValueSource(strings = {"key.converter", "value.converter", "header.converter"})
    @DisplayName("an incorrect converter is reported")
    void incorrectConverterIsReported(String missingConverter) {
        // given a config where a converter is not specified
        Map<String, String> configs = getConverterConfigs();
        configs.put(missingConverter, JsonConverter.class.getName());

        // when the config is validated
        List<ConfigValue> configValues = new ArrayList<>();
        ConverterVerifier.verifyConverters(configs, configValues);

        // an error is reported for the missing key
        assertEquals(1,  configValues.size(), "An error shuld be reported");
        ConfigValue configValue = configValues.get(0);
        assertEquals(missingConverter, configValue.name(), "The error is reported for the incorrect converter");
    }

    /**
     * Create a minimal config with only the key, value and header converters.
     * @return the map with values.
     */
    public static Map<String, String> getConverterConfigs() {
        final Map<String, String> configs = new HashMap<>();
        List.of("key.converter", "value.converter", "header.converter")
                .forEach(key -> configs.put(key, ByteArrayConverter.class.getName()));
        return configs;
    }
}
