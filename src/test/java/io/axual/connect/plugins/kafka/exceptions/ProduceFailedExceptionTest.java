package io.axual.connect.plugins.kafka.exceptions;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProduceFailedExceptionTest {
    private static final Throwable EXPECTED_CAUSE = new RuntimeException(ProduceFailedExceptionTest.class.getName() + "_1");
    private static final String EXPECTED_MESSAGE = "Testing the exception";

    @Test
    void getInputs() {
        ProduceFailedException toTest = new ProduceFailedException(EXPECTED_MESSAGE, EXPECTED_CAUSE);

        assertEquals(EXPECTED_MESSAGE, toTest.getMessage());
        assertEquals(EXPECTED_CAUSE, toTest.getCause());
    }
}