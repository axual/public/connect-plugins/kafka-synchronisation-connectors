package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.selectors.FixedTopicSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import org.apache.kafka.common.config.Config;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigValue;
import org.apache.kafka.connect.connector.Task;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;
import java.util.stream.IntStream;

import static io.axual.connect.plugins.kafka.util.ConverterVerifierTest.getConverterConfigs;
import static org.junit.jupiter.api.Assertions.*;

class KafkaSinkConnectorTest {
    private static final String TASK_ID_CONFIG = KafkaSinkTask.TASK_ID_CONFIG;
    private static final String EXPECTED_VERSION = "testing";
    private static final ConfigDef EXPECTED_CONFIG_DEF = KafkaSinkConfig.configDef();
    private static final Class<? extends Task> EXPECTED_TASK = KafkaSinkTask.class;

    private KafkaSinkConnector kafkaSinkConnector;


    Map<String, String> createBasicProperties() {
        return new HashMap<>();
    }

    @BeforeEach
    void createFixture() {
        kafkaSinkConnector = new KafkaSinkConnector();
    }

    @Test
    void start_with_taskConfigs() {
        final Map<String, String> configMap = Collections.unmodifiableMap(createBasicProperties());

        assertDoesNotThrow(() -> kafkaSinkConnector.start(configMap));

        final int maxNrOfTasks = 2;
        List<Map<String, String>> taskConfigs = kafkaSinkConnector.taskConfigs(maxNrOfTasks);

        List<String> expectedTaskIds = IntStream.range(0, maxNrOfTasks).mapToObj(Integer::toString).toList();
        List<String> foundTaskIds = new ArrayList<>();

        assertEquals(maxNrOfTasks, taskConfigs.size());
        for (Map<String, String> taskConfig : taskConfigs) {
            String taskId = taskConfig.get(TASK_ID_CONFIG);
            assertNotNull(taskId);
            foundTaskIds.add(taskId);

            for (String configKey : configMap.keySet()) {
                assertTrue(taskConfig.containsKey(configKey), "Key '" + configKey + "' not found");
                assertEquals(configMap.get(configKey), taskConfig.get(configKey), "Values for key '" + configKey + "' are not equal");
            }
        }

        assertIterableEquals(expectedTaskIds, foundTaskIds);
    }

    @Test
    void stop() {
        assertDoesNotThrow(() -> kafkaSinkConnector.stop());
    }

    @Test
    void taskClass() {
        assertEquals(EXPECTED_TASK, kafkaSinkConnector.taskClass());
    }

    @Test
    void config() {
        assertEquals(EXPECTED_CONFIG_DEF.configKeys(), kafkaSinkConnector.config().configKeys());
    }

    @Test
    void version() {
        assertEquals(EXPECTED_VERSION, kafkaSinkConnector.version());
    }

    @Test
    void validate_embedsTopicSelectorConfigDef() {
        // given a configuration with a topicSelector configured, but no config for that selector
        Map<String, String> configs = new HashMap<>();
        configs.put(KafkaSinkConfig.TOPIC_SELECTOR_CONFIG, TopicSelector.Type.fixed.name());

        // when the connector validates the config
        final Config validateResult = kafkaSinkConnector.validate(configs);

        // the result contains an error, originating from the configDef from the configured topicselector
        final ConfigValue configValue = validateResult.configValues().stream()
                .filter(cv -> cv.name().startsWith("topic.selector.target"))
                .findFirst().get();
        assertEquals(1, configValue.errorMessages().size());
        assertEquals("Missing required configuration \"topic.selector.target\" which has no default value.", configValue.errorMessages().get(0));
    }

    @Test
    void validate_embedsTopicSelectorClassConfigDef() {
        // given a configuration with a topicSelector configured, but no config for that selector
        Map<String, String> configs = new HashMap<>();
        configs.put(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG, FixedTopicSelector.class.getName());

        // when the connector validates the config
        final Config validateResult = kafkaSinkConnector.validate(configs);

        // the result contains an error, originating from the configDef from the configured topicselector
        final ConfigValue configValue = validateResult.configValues().stream()
                .filter(cv -> cv.name().startsWith("topic.selector.target"))
                .findFirst().get();
        assertEquals(1, configValue.errorMessages().size());
        assertEquals("Missing required configuration \"topic.selector.target\" which has no default value.", configValue.errorMessages().get(0));
    }

    @ParameterizedTest
    @ValueSource(strings = {"key.converter", "value.converter", "header.converter"})
    @DisplayName("a missing converter is reported")
    void missingConverterIsReported(String missingConverter) {
        // given a config where a converter is not specified
        Map<String, String> configs = getConverterConfigs();
        configs.remove(missingConverter);

        // when the config is validated
        final Config validated =  kafkaSinkConnector.validate(configs);

        // an error is reported for the missing key
        final Optional<ConfigValue> configValue = validated.configValues().stream()
                .filter(cv -> cv.name().startsWith(missingConverter))
                .findFirst();
        assertTrue(configValue.isPresent(), "an error is reported  for the missing converter " + missingConverter);

    }

}