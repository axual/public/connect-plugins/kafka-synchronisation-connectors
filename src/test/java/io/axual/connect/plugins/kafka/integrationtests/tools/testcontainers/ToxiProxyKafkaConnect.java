package io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.integrationtests.tools.PortAllocator;
import io.axual.connect.plugins.kafka.integrationtests.tools.client.ConnectClient;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.KafkaCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.ProvideConnectClient;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.ProvideConnectOperations;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.connect.cli.ConnectDistributed;
import org.apache.kafka.connect.converters.ByteArrayConverter;
import org.apache.kafka.connect.runtime.Connect;
import org.apache.kafka.connect.runtime.rest.entities.ConnectorInfo;
import org.testcontainers.lifecycle.Startable;

import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers.ToxiProxyKafka.DIRECT_LISTENER_NAME;
import static org.apache.kafka.connect.runtime.distributed.DistributedConfig.*;
import static org.apache.kafka.connect.runtime.rest.RestServerConfig.LISTENERS_CONFIG;

@Slf4j
public class ToxiProxyKafkaConnect implements Startable, ProvideConnectOperations, ProvideConnectClient, KafkaCluster {
    private static final AtomicInteger CLUSTER_COUNTER = new AtomicInteger(1);

    private static final String INTERNAL_TOPIC_PREFIX = "_";
    private static final String CONFIG_TOPIC_POSTFIX = "-config";
    private static final String OFFSET_TOPIC_POSTFIX = "-offset";
    private static final String STATUS_TOPIC_POSTFIX = "-status";

    @Getter
    private final ToxiProxyKafka toxiProxyKafka;
    private final String logPrefix;
    private final int connectPort;

    private final String connectGroupId;
    private final String connectConfigTopicName;
    private final String connectOffsetTopicName;
    private final String connectStatusTopicName;

    private Connect connect;

    private final KafkaConnectOperations connectOperations;

    public ToxiProxyKafkaConnect() {
        var clusterId = "ToxiProxyConnect-" + CLUSTER_COUNTER.getAndIncrement();
        this.logPrefix = clusterId + " - ";
        this.connectPort = PortAllocator.allocateSinglePort();
        this.toxiProxyKafka = new ToxiProxyKafka();

        this.connectGroupId = "GRP_" + clusterId;
        this.connectConfigTopicName = INTERNAL_TOPIC_PREFIX + clusterId + CONFIG_TOPIC_POSTFIX;
        this.connectOffsetTopicName = INTERNAL_TOPIC_PREFIX + clusterId + OFFSET_TOPIC_POSTFIX;
        this.connectStatusTopicName = INTERNAL_TOPIC_PREFIX + clusterId + STATUS_TOPIC_POSTFIX;

        this.connectOperations = new KafkaConnectOperations(this, this.toxiProxyKafka, this.logPrefix);
    }

    public String getListener() {
        return "http://localhost:" + connectPort;
    }

    @Override
    public void start() {
        log.info("{} - Starting Kafka Connect Cluster", logPrefix);
        if (!toxiProxyKafka.kafka().isRunning()) {
            log.info("{} - Starting Kafka Cluster", logPrefix);
            toxiProxyKafka.start();
            log.info("{} - Started Kafka Cluster", logPrefix);
        }

        log.info("{} - Creating Connect Topics", logPrefix);
        createTopic(connectConfigTopicName, 1, true);
        createTopic(connectOffsetTopicName, 1, true);
        createTopic(connectStatusTopicName, 1, true);
        log.info("{} - Created Connect Topics", logPrefix);

        connect = new ConnectDistributed().startConnect(workerProperties());

        log.info("{} - Started Kafka Connect Cluster", logPrefix);
    }

    @Override
    public void stop() {
        log.info("{} - Stopping Kafka Connect Cluster", logPrefix);
        connect.stop();
        connect.awaitStop();
        toxiProxyKafka.stop();
        log.info("{} - Stopped Kafka Connect Cluster", logPrefix);
    }

    @Override
    public void close() {
        stop();
        toxiProxyKafka.close();
    }

    @Override
    public ConnectClient connectClient() {
        return ConnectClient.createClient(getListener());
    }

    private Map<String, String> workerProperties() {
        final Map<String, String> workerProperties = new HashMap<>(toxiProxyKafka.clientConnectionStringProperties(DIRECT_LISTENER_NAME));

        workerProperties.put(OFFSET_COMMIT_INTERVAL_MS_CONFIG, "5000");
        workerProperties.put(LISTENERS_CONFIG, getListener());
        workerProperties.put(GROUP_ID_CONFIG, connectGroupId);
        workerProperties.put(CONFIG_TOPIC_CONFIG, connectConfigTopicName);
        workerProperties.put(CONFIG_STORAGE_REPLICATION_FACTOR_CONFIG, "1");
        workerProperties.put(OFFSET_STORAGE_TOPIC_CONFIG, connectOffsetTopicName);
        workerProperties.put(OFFSET_STORAGE_REPLICATION_FACTOR_CONFIG, "1");
        workerProperties.put(STATUS_STORAGE_TOPIC_CONFIG, connectStatusTopicName);
        workerProperties.put(STATUS_STORAGE_REPLICATION_FACTOR_CONFIG, "1");
        workerProperties.put(KEY_CONVERTER_CLASS_CONFIG, ByteArrayConverter.class.getName());
        workerProperties.put(VALUE_CONVERTER_CLASS_CONFIG, ByteArrayConverter.class.getName());
        workerProperties.put(CONNECTOR_CLIENT_POLICY_CLASS_CONFIG, "All");

        return workerProperties;
    }

    @Override
    public ConnectorInfo createConnector(final String connectorName, final Map<String, String> connectorConfig) {
        return connectOperations.createConnector(connectorName, connectorConfig);
    }

    @Override
    public Optional<Boolean> isConnectorRunning(final String connectorName, final int numTasks) {
        return connectOperations.isConnectorRunning(connectorName, numTasks);
    }

    @Override
    public void waitForConnectorAndTasksToStart(final String connectorName, final int numTasks, final Duration timeout) {
        connectOperations.waitForConnectorAndTasksToStart(connectorName, numTasks, timeout);
    }

    @Override
    public void createTopic(final String name, final int partitions, final boolean compacted) {
        connectOperations.createTopic(name, partitions, compacted);
    }

    @Override
    public void deleteTopic(final String name) {
        connectOperations.deleteTopic(name);
    }

    @Override
    public ConsumerRecords<byte[], byte[]> consume(final String listenerName, final int expectedRecordCount, final long maxDuration, final String groupId, final Collection<String> topics) {
        return connectOperations.consume(listenerName, expectedRecordCount, maxDuration, groupId, topics);
    }

    @Override
    public long produce(final String listenerName, final ProducerRecord<byte[], byte[]> producerRecord) {
        return connectOperations.produce(listenerName, producerRecord);
    }

    @Override
    public String bootstrapServers() {
        return connectOperations.bootstrapServers();
    }

    @Override
    public String bootstrapServers(final String listenerName) {
        return connectOperations.bootstrapServers(listenerName);
    }

    @Override
    public Map<String, Object> clientConnectionProperties() {
        return connectOperations.clientConnectionProperties();
    }

    @Override
    public Map<String, Object> clientConnectionProperties(final String listenerName) {
        return connectOperations.clientConnectionProperties(listenerName);
    }

    @Override
    public Map<String, Object> producerProperties(final String listenerName, final String transactionalId) {
        return connectOperations.producerProperties(listenerName, transactionalId);
    }

    @Override
    public Map<String, Object> consumerProperties(final String listenerName, final String groupId) {
        return connectOperations.consumerProperties(listenerName, groupId);
    }
}
