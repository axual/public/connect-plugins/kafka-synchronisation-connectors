package io.axual.connect.plugins.kafka.integrationtests;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.integrationtests.tools.providers.StartableKafkaConnectCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers.TestcontainerKafkaConnectCluster;
import io.axual.connect.plugins.kafka.sink.KafkaSinkConfig;
import io.axual.connect.plugins.kafka.sink.KafkaSinkConnector;
import io.axual.connect.plugins.kafka.util.ConverterVerifierTest;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.connect.runtime.SinkConnectorConfig;
import org.apache.kafka.connect.runtime.rest.entities.ConfigInfo;
import org.apache.kafka.connect.runtime.rest.entities.ConfigInfos;
import org.apache.kafka.connect.runtime.rest.entities.ConfigValueInfo;
import org.apache.kafka.connect.runtime.rest.entities.PluginInfo;
import org.junit.jupiter.api.Test;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
class PluginValidationIT {
    private static final String EXPECTED_CONNECTOR_CLASS = KafkaSinkConnector.class.getName();
    private static final String EXPECTED_CONNECTOR_VERSION = "testing";

    @Container
    StartableKafkaConnectCluster connect = new TestcontainerKafkaConnectCluster("connect");

    static String processConfigErrorInfo(ConfigInfos configInfos) {
        return configInfos.values().stream()
                .map(ConfigInfo::configValue)
                .map(ConfigValueInfo::errors)
                .flatMap(Collection::stream)
                .filter(v -> !v.isEmpty())
                .reduce("", (first, second) -> first + "\n" + second);
    }

    @Test
    void checkPluginExistence() {
        assertTrue(connect.connectClient().getAllConnectorPlugins().stream().anyMatch(this::matchPlugin), () -> String.format("Could not find plugin %s with version %s", EXPECTED_CONNECTOR_CLASS, EXPECTED_CONNECTOR_VERSION));
    }

    private boolean matchPlugin(PluginInfo connectorPluginInfo) {
        return connectorPluginInfo != null
                && EXPECTED_CONNECTOR_CLASS.equals(connectorPluginInfo.className())
                && EXPECTED_CONNECTOR_VERSION.equals(connectorPluginInfo.version());
    }

    @Test
    void validateConfig() {
        final String connectorType = KafkaSinkConnector.class.getSimpleName();

        String errorMessage = processConfigErrorInfo(connect.connectClient().validateConnectorPluginConfig(connectorType, getConfig(true)));
        assertTrue(errorMessage.isEmpty(), errorMessage);

        errorMessage = processConfigErrorInfo(connect.connectClient().validateConnectorPluginConfig(connectorType, getConfig(false)));
        assertFalse(errorMessage.isEmpty(), "Expected error messages");
    }

    Map<String, String> getConfig(boolean validRemote) {
        // make sure converters are configured with correct type
        Map<String, String> config = ConverterVerifierTest.getConverterConfigs();

        // Basic requirement for validation
        config.put(SinkConnectorConfig.CONNECTOR_CLASS_CONFIG, KafkaSinkConnector.class.getName());
        config.put(SinkConnectorConfig.NAME_CONFIG, "TestConnector");
        config.put(SinkConnectorConfig.TOPICS_CONFIG, "TestTopic");



        if (validRemote) {
            config.put(KafkaSinkConfig.REMOTE_CONFIG_PREFIX + ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, connect.bootstrapServers());
        }

        Stream.of("one", "two", "three")
                .peek(v -> {
                    config.put(String.format(KafkaSinkConfig.STATIC_HEADER_NAME_CONFIG_FORMAT, v), "Name " + v);
                    config.put(String.format(KafkaSinkConfig.STATIC_HEADER_VALUE_CONFIG_FORMAT, v), "Value " + v);
                })
                .reduce((one, two) -> one + "," + two).ifPresent(v -> config.put(KafkaSinkConfig.STATIC_HEADER_ALIAS_CONFIG, v));

        
        return config;
    }

}
