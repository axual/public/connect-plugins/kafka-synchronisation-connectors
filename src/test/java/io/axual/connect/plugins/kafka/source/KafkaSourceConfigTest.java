package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import org.apache.kafka.common.config.ConfigException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class KafkaSourceConfigTest {

    // add required configuration values for consumer
    public static final Map<String, String> REQUIRED_CONFIG = new HashMap<>();
    static {
        REQUIRED_CONFIG.put("source.topics", "topic-1,topic-2");
        REQUIRED_CONFIG.put("remote.bootstrap.servers", "localhost:9092");
        REQUIRED_CONFIG.put("remote.group.id", "testing");
    }

    @ParameterizedTest
    @ValueSource(strings = { "source.topics", "remote.group.id"})
    void testMissingRequiredProperties(String missingKey) {
        Map<String, String> incompleteConfig = new HashMap<>(REQUIRED_CONFIG);
        incompleteConfig.remove(missingKey);
        assertThrows(ConfigException.class, () -> new KafkaSourceConfig(incompleteConfig), "failed to get exception for missing required key " + missingKey);
    }

    @Test
    void noStaticHeadersSet_returnsEmptyMap() {
        // given a KafkaSourceConfig from a map that specifies no list of custom header names
        final KafkaSourceConfig kafkaSourceConfig = new KafkaSourceConfig(REQUIRED_CONFIG);

        // when we retrieve the static headers, we get an empty map
        final Map<String, String> staticHeaders = kafkaSourceConfig.getStaticHeaders();
        assertNotNull(staticHeaders, "getStaticHeaders never returns null");
        assertTrue(staticHeaders.isEmpty(), "There should be no static headers");
    }

    @Test
    void staticHeadersSpecified_keyIsMissing_shouldThrowConfigException() {
        // given a config that specifies a custom header, but no name for that header
        Map<String, String> configMap = new HashMap<>(REQUIRED_CONFIG);
        configMap.put(KafkaSourceConfigDefinitions.STATIC_HEADER_NAMES_CONFIG, "myheader");

        // when we create a KafkaSourceConfig from it, we should get an exception
        try {
            new KafkaSourceConfig(configMap);
            fail("Should have gotten exception for missing config key");
        } catch (ConfigException e) {
            // the exception should specify the correctly formatted name
            assertTrue(e.getMessage().contains("No header name defined for header 'myheader'; was expecting config 'header.static.myheader.name'"));
        }
    }

    @Test
    void staticHeadersSpecified_valueIsMissing_shouldThrowConfigException() {
        // given a config that specifies a custom header and the name for that header, but no value for it
        Map<String, String> configMap = new HashMap<>(REQUIRED_CONFIG);
        configMap.put(KafkaSourceConfigDefinitions.STATIC_HEADER_NAMES_CONFIG, "myheader");
        configMap.put("header.static.myheader.name", "X-Static-MyHeader");

        // when we create a KafkaSourceConfig from it, we should get an exception
        try {
            new KafkaSourceConfig(configMap);
            fail("Should have gotten exception for missing config key");
        } catch (ConfigException e) {
            // the exception should specify the correctly formatted name for the value, should not complain about the
            assertFalse(e.getMessage().contains("No header name defined for header 'myheader'; was expecting config 'header.static.myheader.name'"));
            assertTrue(e.getMessage().contains("No header value defined for header 'myheader'; was expecting config 'header.static.myheader.value'"));
        }
    }

    @Test
    void staticHeadersSpecified_areKeptInConfig() {
        // given a config that specifies a custom header and the name and value for that header
        Map<String, String> configMap = new HashMap<>(REQUIRED_CONFIG);
        configMap.put(KafkaSourceConfigDefinitions.STATIC_HEADER_NAMES_CONFIG, "myheader");
        configMap.put("header.static.myheader.name", "X-Static-MyHeader");
        configMap.put("header.static.myheader.value", "my-configured-value");

        // when we create a KafkaSourceConfig from it
        final KafkaSourceConfig kafkaSourceConfig = new KafkaSourceConfig(configMap);

        // the custom header(s) are present in the config with the configured value
        assertTrue(kafkaSourceConfig.getStaticHeaders().containsKey("X-Static-MyHeader"));
        assertEquals("my-configured-value", kafkaSourceConfig.getStaticHeaders().get("X-Static-MyHeader"));
    }

    @Test
    void noSpecialHeadersSpecified_returnDefaults() {
        // given a config that does not specify any special headers
        final HashMap<String, String> configMap = new HashMap<>(REQUIRED_CONFIG);

        // when we create a KafkaSourceConfig from it
        final KafkaSourceConfig kafkaSourceConfig = new KafkaSourceConfig(configMap);

        // the getters for special header names return the default values
        assertTrue(kafkaSourceConfig.getRemoteTopicHeadername().isPresent());
        assertEquals("X-Kafka-Original-Topic", kafkaSourceConfig.getRemoteTopicHeadername().get());
        assertTrue(kafkaSourceConfig.getRemotePartitionHeadername().isPresent());
        assertEquals("X-Kafka-Original-Partition", kafkaSourceConfig.getRemotePartitionHeadername().get());
        assertTrue(kafkaSourceConfig.getRemoteOffsetHeadername().isPresent());
        assertEquals("X-Kafka-Original-Offset", kafkaSourceConfig.getRemoteOffsetHeadername().get());
        assertTrue(kafkaSourceConfig.getRemoteTimestampHeadername().isPresent());
        assertEquals("X-Kafka-Original-Timestamp", kafkaSourceConfig.getRemoteTimestampHeadername().get());
    }

    @Test
    void specialHeadersSpecified_returnNonEmptyOptional() {
        // given a config that specifies all special headers
        final HashMap<String, String> config = new HashMap<>(REQUIRED_CONFIG);
        config.put(KafkaSourceConfigDefinitions.TOPIC_HEADER_NAME_CONFIG, "X-Topic");
        config.put(KafkaSourceConfigDefinitions.PARTITION_HEADER_NAME_CONFIG, "X-Partition");
        config.put(KafkaSourceConfigDefinitions.OFFSET_HEADER_NAME_CONFIG, "X-Offset");
        config.put(KafkaSourceConfigDefinitions.TIMESTAMP_HEADER_NAME_CONFIG, "X-Timestamp");

        // when we create a KafkaSourceConfig from it
        final KafkaSourceConfig kafkaSourceConfig = new KafkaSourceConfig(config);

        // the getters for special header names return a non empty Optional with the configured value
        assertTrue(kafkaSourceConfig.getRemoteTopicHeadername().isPresent());
        assertEquals("X-Topic", kafkaSourceConfig.getRemoteTopicHeadername().get());
        assertTrue(kafkaSourceConfig.getRemotePartitionHeadername().isPresent());
        assertEquals("X-Partition", kafkaSourceConfig.getRemotePartitionHeadername().get());
        assertTrue(kafkaSourceConfig.getRemoteOffsetHeadername().isPresent());
        assertEquals("X-Offset", kafkaSourceConfig.getRemoteOffsetHeadername().get());
        assertTrue(kafkaSourceConfig.getRemoteTimestampHeadername().isPresent());
        assertEquals("X-Timestamp", kafkaSourceConfig.getRemoteTimestampHeadername().get());
    }

    @Test
    void specialHeadersSetToNull_returnEmptyOptional() {
        // given a config that specifies all special headers
        final HashMap<String, String> config = new HashMap<>(REQUIRED_CONFIG);
        config.put(KafkaSourceConfigDefinitions.TOPIC_HEADER_NAME_CONFIG, null);
        config.put(KafkaSourceConfigDefinitions.PARTITION_HEADER_NAME_CONFIG, null);
        config.put(KafkaSourceConfigDefinitions.OFFSET_HEADER_NAME_CONFIG, null);
        config.put(KafkaSourceConfigDefinitions.TIMESTAMP_HEADER_NAME_CONFIG, null);

        // when we create a KafkaSourceConfig from it
        final KafkaSourceConfig kafkaSourceConfig = new KafkaSourceConfig(config);

        // the getters for special header names return an empty Optional
        assertFalse(kafkaSourceConfig.getRemoteTopicHeadername().isPresent());
        assertFalse(kafkaSourceConfig.getRemotePartitionHeadername().isPresent());
        assertFalse(kafkaSourceConfig.getRemoteOffsetHeadername().isPresent());
        assertFalse(kafkaSourceConfig.getRemoteTimestampHeadername().isPresent());
    }

    @Test
    void remoteHeaderPrefixNotSet_returnEmptyString() {
        // given a config that does not specify a remote header prefix
        final HashMap<String, String> config = new HashMap<>(REQUIRED_CONFIG);

        // when we create a config from it
        final KafkaSourceConfig kafkaSourceConfig = new KafkaSourceConfig(config);

        // it returns an empty Optional for the value
        assertTrue(kafkaSourceConfig.getRemoteHeaderPrefix().isPresent());
        kafkaSourceConfig.getRemoteHeaderPrefix().ifPresent(pfx -> assertEquals("", pfx));
    }

    @Test
    void remoteHeaderPrefixSet_returnNonEmptyOptional() {
        // given a config that does not specify a remote header prefix
        final HashMap<String, String> config = new HashMap<>(REQUIRED_CONFIG);
        config.put(KafkaSourceConfigDefinitions.REMOTE_HEADER_PREFIX_CONFIG, "X-Remote-");

        // when we create a config from it
        final KafkaSourceConfig kafkaSourceConfig = new KafkaSourceConfig(config);

        // it returns an empty Optional for the value
        assertTrue(kafkaSourceConfig.getRemoteHeaderPrefix().isPresent());
        kafkaSourceConfig.getRemoteHeaderPrefix().ifPresent(pfx -> assertEquals("X-Remote-", pfx));
    }

    @Test
    void refuseUnknownSelectorType() {
        // given a config that specifies target selection type other than "single" or "map"
        final HashMap<String, String> configMap = new HashMap<>(REQUIRED_CONFIG);
        configMap.put(TopicSelector.TOPIC_SELECTOR_CONFIG, "something_else");

        // when we create a config from it
        try {
            new KafkaSourceConfig(configMap);
            fail("Should refuse config with invalid mapping type");
        } catch (ConfigException e) {
            // this is the expected behavior - message based on Enum values, verify startsWith only
            assertThat(e.getMessage())
                    .startsWith("Invalid value something_else for configuration topic.selector: String must be one of:");
        }
    }

    @Test
    void refuseUnknownSelectorClassType() {
        // given a config that specifies target selection type other than "single" or "map"
        final HashMap<String, String> configMap = new HashMap<>(REQUIRED_CONFIG);
        configMap.put(KafkaSourceConfigDefinitions.TOPIC_SELECTOR_CLASS_CONFIG, "something_else");

        // when we create a config from it
        try {
            new KafkaSourceConfig(configMap);
            fail("Should refuse config with invalid mapping type");
        } catch (ConfigException e) {
            // this is the expected behavior - message based on Enum values, verify startsWith only
            assertThat(e.getMessage())
                    .startsWith("Invalid value something_else for configuration topic.selector.class:");
        }
    }
}
