package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.MockDescribeTopicsResult;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.TopicPartitionInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class KafkaSourceConfigVerifierTest {

    @Mock
    private AdminClient mockAdminClient;

    private Set<Node> checkedNodes;

    private final Map<String, String> configMap = KafkaSourceConfigTest.REQUIRED_CONFIG;

    private KafkaSourceConfigVerifier kafkaSourceConfigVerifier;

    @BeforeEach
    void createFixture() {
        // inject mock AdminClient into verifier under test
        kafkaSourceConfigVerifier = new KafkaSourceConfigVerifier() {
            @Override
            AdminClient createAdminclient(KafkaSourceConfig config) {
                return mockAdminClient;
            }

            // keep track of Nodes this verifier tried to connect to.
            @Override
            Optional<Node> connectToNode(Node node) {
                checkedNodes.add(node);
                if (node instanceof FailingNode) {
                    return Optional.of(node);
                }
                return Optional.empty();
            }
        };

        // reset checked nodes
        checkedNodes = new HashSet<>();
    }

    @Test
    @DisplayName("all configured topics are verified")
    void configuredTopicsAreVerified() {
        // given a config containing some source topics
        configMap.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, "topic1,topic2");
        when(mockAdminClient.describeTopics(anyCollection())).thenReturn(MockDescribeTopicsResult.of(new HashMap<>()));
        final ArgumentCaptor<Collection> verifiedTopics = ArgumentCaptor.forClass(Collection.class);

        // when the config is verified
        kafkaSourceConfigVerifier.verify(new KafkaSourceConfig(configMap));

        // the configured topics are passed to the admin client
        verify(mockAdminClient).describeTopics(verifiedTopics.capture());
        assertTrue(verifiedTopics.getValue().contains("topic1"));
        assertTrue(verifiedTopics.getValue().contains("topic2"));
    }

    @Test
    @DisplayName("verification fails if the returned future contains an exception")
    void failsIfFutureContainsException() {
        // given a config containing some topics
        configMap.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, "topic1,topic2");

        // verification will fail if one the returned futures completed exceptionally
        when(mockAdminClient.describeTopics(anyCollection())).thenReturn(MockDescribeTopicsResult.failingWith(new IOException("test!")));

        // when the config is verified
        try {
            kafkaSourceConfigVerifier.verify(new KafkaSourceConfig(configMap));
            fail("failed to throw exception for failed future");
        } catch (KafkaException ke) {
            // this is the expected behavior.
            assertEquals(IOException.class, ke.getCause().getClass());
            assertEquals("describeTopics failed", ke.getMessage());
        }
    }

    @Test
    @DisplayName("leader nodes for all topic/partitions are checked")
    void nodesForAllTopicsAndPartitionsWereChecked() {
        // given a config containing some topics
        configMap.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, "topic1,topic2");

        // and given two topics with one or two partition, and separate leaders for each partition
        Node node1 = new Node(1, "host1", 9091, "rack1");
        Node node2 = new Node(2, "host2", 9092, "rack2");
        Node node3 = new Node(3, "host3", 9092, "rack2");
        TopicPartitionInfo info1 = new TopicPartitionInfo(1, node1, new ArrayList<>(), new ArrayList<>());
        TopicPartitionInfo info2 = new TopicPartitionInfo(1, node2, new ArrayList<>(), new ArrayList<>());
        TopicPartitionInfo info3 = new TopicPartitionInfo(1, node3, new ArrayList<>(), new ArrayList<>());
        TopicDescription description1 = new TopicDescription("topic1", false, List.of(info1));
        TopicDescription description2 = new TopicDescription("topic2", false, Arrays.asList(info2, info3));
        Map<String, TopicDescription> returnedDescriptions = new HashMap<>();
        returnedDescriptions.put("topic1", description1);
        returnedDescriptions.put("topic2", description2);

        when(mockAdminClient.describeTopics(anyCollection())).thenReturn(MockDescribeTopicsResult.of(returnedDescriptions));

        // when the config is verified, no exception is thrown
        final List<Node> failingNodes = kafkaSourceConfigVerifier.verify(new KafkaSourceConfig(configMap));

        // and all the returned leader nodes were verified
        assertEquals(3, checkedNodes.size());
        assertEquals(0, failingNodes.size());
        assertTrue(checkedNodes.contains(node1));
        assertTrue(checkedNodes.contains(node2));
        assertTrue(checkedNodes.contains(node3));
    }

    @Test
    @DisplayName("verification fails if any node is unreachable")
    void verificationFailsIfConnectionToNodeFails() {
        // given a config containing a topics
        configMap.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, "topic1,topic2");

        // and given a topics with one partition and a leader where connection will fail
        Node node1 = new FailingNode(1, "host1", 9091, "rack1");
        Node node2 = new FailingNode(2, "host1", 9091, "rack1");
        Node node3 = new Node(3, "host1", 9091, "rack1");
        TopicPartitionInfo info1 = new TopicPartitionInfo(1, node1, new ArrayList<>(), new ArrayList<>());
        TopicPartitionInfo info2 = new TopicPartitionInfo(1, node2, new ArrayList<>(), new ArrayList<>());
        TopicPartitionInfo info3 = new TopicPartitionInfo(1, node3, new ArrayList<>(), new ArrayList<>());
        TopicDescription description1 = new TopicDescription("topic1", false, List.of(info1));
        TopicDescription description2 = new TopicDescription("topic2", false, Arrays.asList(info2, info3));
        Map<String, TopicDescription> returnedDescriptions = new HashMap<>();
        returnedDescriptions.put("topic1", description1);
        returnedDescriptions.put("topic2", description2);

        when(mockAdminClient.describeTopics(anyCollection())).thenReturn(MockDescribeTopicsResult.of(returnedDescriptions));

        // when the config is verified, the failing node(s) are returned
        final List<Node> failingNodes = kafkaSourceConfigVerifier.verify(new KafkaSourceConfig(configMap));

        assertEquals(3, checkedNodes.size());
        assertEquals(2, failingNodes.size());
        assertTrue(checkedNodes.contains(node1));
        assertTrue(checkedNodes.contains(node2));
        assertTrue(checkedNodes.contains(node3));
        assertTrue(failingNodes.contains(node1));
        assertTrue(failingNodes.contains(node2));
    }

    /**
     * Subclass of Node for which verification will fail.
     */
    static class FailingNode extends Node {
        public FailingNode(int id, String host, int port, String rack) {
            super(id, host, port, rack);
        }
    }

}
