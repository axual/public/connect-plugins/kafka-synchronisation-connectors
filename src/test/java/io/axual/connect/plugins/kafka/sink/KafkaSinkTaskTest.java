package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.connect.errors.ConnectException;
import org.apache.kafka.connect.errors.RetriableException;
import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.axual.connect.plugins.kafka.selectors.SourceTopicSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;

import static io.axual.connect.plugins.kafka.sink.KafkaSinkConfig.PARTITION_SELECTOR_SOURCE;
import static io.axual.connect.plugins.kafka.sink.KafkaSinkConfig.SINK_QUEUE_SIZE_CONFIG;
import static java.util.Collections.emptyMap;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class KafkaSinkTaskTest {
    private static final String REMOTE_PREFIX = "remote.";
    private static final String TASK_ID_CONFIG = KafkaSinkTask.TASK_ID_CONFIG;
    private static final String EXPECTED_TASK_ID = "9";
    private static final String EXPECTED_VERSION = "testing";

    private static final String EXPECTED_BOOTSTRAP_SERVERS = "some-machine:1234";
    private static final String EXPECTED_KEY_SERIALIZER = ByteArraySerializer.class.getName();
    private static final String EXPECTED_VALUE_SERIALIZER = ByteArraySerializer.class.getName();
    private static final String TOPIC = "sink-test";
    private static final int PARTITION = 1;
    private static final int MAX_QUEUE_SIZE = 1;
    private static final Map<String, String> TASK_PROPERTIES = createTaskProperties();
    private static final long MAX_QUEUE_PUT_WAIT_MS = 1L;

    @Mock
    Producer<byte[], byte[]> producer;
    @Mock(strictness = Mock.Strictness.LENIENT)
    ProducerFactory producerFactory;
    @Mock
    KafkaSinkRunner runner;

    MockedConstruction<ProducerFactory> producerFactoryMockedConstruction;

    private static Map<String, String> createTaskProperties() {
        Map<String, String> props = new HashMap<>();
        props.put(TASK_ID_CONFIG, EXPECTED_TASK_ID);
        props.put(SINK_QUEUE_SIZE_CONFIG, Integer.toString(MAX_QUEUE_SIZE));
        props.put(REMOTE_PREFIX + ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, EXPECTED_BOOTSTRAP_SERVERS);
        props.put(REMOTE_PREFIX + ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, EXPECTED_KEY_SERIALIZER);
        props.put(REMOTE_PREFIX + ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, EXPECTED_VALUE_SERIALIZER);
        return props;
    }

    @BeforeEach
    void setup() {
        producerFactoryMockedConstruction = mockConstruction(ProducerFactory.class,
                (mock, context) -> when(mock.createProducer(any())).thenReturn(producer)
        );
    }

    @AfterEach
    void closeMocks() {
        producerFactoryMockedConstruction.close();
    }

    /**
     * Helper to create a sink record for a test
     *
     * @return a sink record for the expected topic and partition
     */
    static SinkRecord createSinkRecord() {
        return new SinkRecord(TOPIC, PARTITION, null, null, null, null, 0L, null, null, null);
    }

    @Test
    void start_Valid() {
        final KafkaSinkTask task = spy(new KafkaSinkTask());
        doReturn(runner).when(task).createRunner(any());

        task.start(TASK_PROPERTIES);

        verify(task, times(1)).createRunner(any());
    }

    @Test
    @Disabled("The default Kafka validator does not see null as an invalid value")
    void start_Invalid() {
        final KafkaSinkTask task = spy(new KafkaSinkTask());
        final Map<String, String> taskConfig = new HashMap<>();
        taskConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, null);

        assertThrows(ConfigException.class, () -> task.start(taskConfig));

        verify(task, never()).createRunner(any());
    }

    @Test
    void stop() {
        final KafkaSinkTask task = spy(new KafkaSinkTask());
        doReturn(runner).when(task).createRunner(any());

        task.start(TASK_PROPERTIES);
        task.stop();
        verify(runner, times(1)).stop();
    }

    @Test
    void put_Correct() throws InterruptedException {
        final KafkaSinkTask task = spy(new KafkaSinkTask());

//        doReturn(producer).when(task).createProducer(anyMap());
        doReturn(runner).when(task).createRunner(any());
        doReturn(true, true).when(runner).put(anyCollection());
        doReturn(true, true).when(runner).isRunning();

        task.start(TASK_PROPERTIES);

        final SinkRecord record1 = createSinkRecord();
        final SinkRecord record2 = createSinkRecord();
        final SinkRecord record3 = createSinkRecord();

        final List<SinkRecord> sinkRecords = Arrays.asList(record1, record2, record3);

        assertDoesNotThrow(() -> task.put(sinkRecords));
        assertDoesNotThrow(() -> task.put(sinkRecords));

        verify(runner, times(2)).put(sinkRecords);
    }

    @Test
    void put_QueueFull() throws InterruptedException {
        final KafkaSinkTask task = spy(new KafkaSinkTask());

//        doReturn(producer).when(task).createProducer(anyMap());
        doReturn(runner).when(task).createRunner(any());
        doReturn(true, false).when(runner).put(anyCollection());
        doReturn(true, true).when(runner).isRunning();
        task.start(TASK_PROPERTIES);

        final SinkRecord record1 = createSinkRecord();
        final SinkRecord record2 = createSinkRecord();
        final SinkRecord record3 = createSinkRecord();

        final List<SinkRecord> sinkRecords = Arrays.asList(record1, record2, record3);

        assertDoesNotThrow(() -> task.put(sinkRecords));
        assertThrows(RetriableException.class, () -> task.put(sinkRecords));

        verify(runner, times(2)).put(sinkRecords);
    }

    @Test
    void put_StoppedRunning() throws InterruptedException {
        final KafkaSinkTask task = spy(new KafkaSinkTask());

//        doReturn(producer).when(task).createProducer(anyMap());
        doReturn(runner).when(task).createRunner(any());
        doReturn(false).when(runner).isRunning();
        task.start(TASK_PROPERTIES);

        final List<SinkRecord> records = Collections.emptyList();
        assertThrows(ConnectException.class, () -> task.put(records));

        verify(runner, times(0)).put(anyCollection());
    }

    @Test
    void put_Interrupted() throws InterruptedException {
        final KafkaSinkTask task = spy(new KafkaSinkTask());
//        doReturn(producer).when(task).createProducer(anyMap());
        doReturn(runner).when(task).createRunner(any());
        doReturn(true).when(runner).isRunning();
        doThrow(new InterruptedException()).when(runner).put(anyCollection());
        task.start(TASK_PROPERTIES);
        final Collection<SinkRecord> sinkRecords = Collections.emptyList();

        assertThrows(ConnectException.class, () -> task.put(sinkRecords));

        verify(runner, times(1)).put(sinkRecords);
    }

    @Test
    void createRunner() {
        final KafkaSinkTask task = new KafkaSinkTask();
        final KafkaSinkConfig config = mock(KafkaSinkConfig.class);
        doReturn(MAX_QUEUE_SIZE).when(config).getQueueSize();
        doReturn(MAX_QUEUE_PUT_WAIT_MS).when(config).getQueuePutWaitMs();
        doReturn(emptyMap()).when(config).getStaticHeaders();
        doReturn(null).when(config).getTopicHeader();
        doReturn(null).when(config).getPartitionHeader();
        doReturn(null).when(config).getOffsetHeader();
        doReturn(null).when(config).getTimestampHeader();
        doReturn(TopicSelector.Type.source.name()).when(config).getTopicSelector();
        doReturn(PARTITION_SELECTOR_SOURCE).when(config).getPartitionSelector();
        doReturn(producer).when(producerFactory).createProducer(any());

        assertNotNull(task.createRunner(config));
    }

    @Test
    void createRunnerTopicSelectorClass() {
        final KafkaSinkTask task = new KafkaSinkTask();
        final KafkaSinkConfig config = mock(KafkaSinkConfig.class);
        doReturn(MAX_QUEUE_SIZE).when(config).getQueueSize();
        doReturn(MAX_QUEUE_PUT_WAIT_MS).when(config).getQueuePutWaitMs();
        doReturn(emptyMap()).when(config).getStaticHeaders();
        doReturn(null).when(config).getTopicHeader();
        doReturn(null).when(config).getPartitionHeader();
        doReturn(null).when(config).getOffsetHeader();
        doReturn(null).when(config).getTimestampHeader();
        doReturn(SourceTopicSelector.class).when(config).getTopicSelectorClass();
        doReturn(PARTITION_SELECTOR_SOURCE).when(config).getPartitionSelector();
        doReturn(producer).when(producerFactory).createProducer(any());

        assertNotNull(task.createRunner(config));
    }

    @Test
    void version() {
        assertEquals(EXPECTED_VERSION, new KafkaSinkTask().version());
    }

}
