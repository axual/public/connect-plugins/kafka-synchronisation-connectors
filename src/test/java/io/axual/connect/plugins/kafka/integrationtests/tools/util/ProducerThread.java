package io.axual.connect.plugins.kafka.integrationtests.tools.util;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.utils.Utils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class ProducerThread implements Callable<List<ProducerRecord<byte[], byte[]>>> {

    private final String resolvedTopic;
    private final Integer topicPartitions;
    private final Duration timeout;

    private final Map<String, Object> producerProps;

    private final AtomicBoolean stop = new AtomicBoolean(false);
    private final List<ProducerRecord<byte[], byte[]>> producedRecords = new ArrayList<>();

    public ProducerThread(String resolvedTopic, Integer topicPartitions, Duration timeout, Map<String, Object> producerProps) {
        this.resolvedTopic = resolvedTopic;
        this.topicPartitions = topicPartitions;
        this.timeout = timeout;

        this.producerProps = producerProps;
    }

    @Override
    public List<ProducerRecord<byte[], byte[]>> call() throws Exception {
        log.info("Starting ProducerThread");

        try (KafkaProducer<byte[], byte[]> producer = getProducer()) {
            while (!stop.get()) {
                log.info("ProducerThread produces a message...");
                var roundRobinPartition = producedRecords.size() % topicPartitions;
                var randomRecord = Util.generateRandomProducerRecord(resolvedTopic, roundRobinPartition);
                producer.send(randomRecord).get();
                producedRecords.add(randomRecord);
                Utils.sleep(timeout.toMillis());
            }
            log.info("Stopping {}, produced {} records to local cluster topic {}", this.getClass().getSimpleName(), producedRecords.size(), resolvedTopic);
        }
        return producedRecords;
    }

    public void stop() {
        stop.set(true);
    }

    private KafkaProducer<byte[], byte[]> getProducer() {
        final Map<String, Object> props = producerProps;
        final String uuid = UUID.randomUUID().toString();
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, getClass().getSimpleName() + "-" + uuid);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, 1);
        props.put(ProducerConfig.RETRIES_CONFIG, 0);

        return new KafkaProducer<>(props);
    }

}
