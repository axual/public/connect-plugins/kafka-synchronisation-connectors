package io.axual.connect.plugins.kafka.integrationtests.tools.providers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;

import java.util.UUID;

public interface ProvideProducing {

    default long produce(String topic, byte[] key, byte[] value) {
        return produce(null, new ProducerRecord<>(topic, null, key, value, null));
    }

    default long produce(String listenerName,String topic, byte[] key, byte[] value) {
        return produce(listenerName, new ProducerRecord<>(topic, null, key, value, null));
    }

    default long produce(String topic, Integer partition, byte[] key, byte[] value) {
        return produce(null, new ProducerRecord<>(topic, partition, key, value, null));
    }

    default long produce(String listenerName, String topic, Integer partition, byte[] key, byte[] value) {
        return produce(listenerName, new ProducerRecord<>(topic, partition, key, value, null));
    }

    default long produce(String topic, Integer partition, byte[] key, byte[] value, RecordHeaders headers) {
        return produce(null, new ProducerRecord<>(topic, partition, key, value, headers));
    }
    default long produce(String listenerName, String topic, Integer partition, byte[] key, byte[] value, RecordHeaders headers) {
        return produce(listenerName, new ProducerRecord<>(topic, partition, key, value, headers));
    }

    long produce(String listenerName, ProducerRecord<byte[], byte[]> producerRecord);

    /**
     * Generate a unique transactional id to use in producers.
     *
     * @return the new unique transactional id
     */
    default String generateTransactionalId() {
        return UUID.randomUUID().toString();
    }

}