package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.source.SourceRecord;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SourcePartitionSelectorTest {

    private static final Integer PARTITION = 91;
    private static final Integer EXPECTED_PARTITION = PARTITION;
    private static final Integer EXPECTED_NULL_PARTITION = null;

    private static final SinkRecord TEST_SINK_RECORD_VALID = new SinkRecord(null, PARTITION, null, null, null, null, 0L);
    private static final SinkRecord TEST_SINK_RECORD_NULL = null;
    private static final SourceRecord TEST_SOURCE_RECORD_VALID = new SourceRecord(Collections.emptyMap(), Collections.emptyMap(), null, PARTITION, null, null, null, null);
    private static final SourceRecord TEST_SOURCE_RECORD_NULL = null;

    @Test
    void select_ValidSink() {
        PartitionSelector partitionSelector = new SourcePartitionSelector();
        assertEquals(EXPECTED_PARTITION, partitionSelector.select(TEST_SINK_RECORD_VALID));
    }

    @Test
    void select_NullSink() {
        PartitionSelector partitionSelector = new SourcePartitionSelector();
        assertEquals(EXPECTED_NULL_PARTITION, partitionSelector.select(TEST_SINK_RECORD_NULL));
    }

    @Test
    void select_ValidSource() {
        PartitionSelector partitionSelector = new SourcePartitionSelector();
        assertEquals(EXPECTED_PARTITION, partitionSelector.select(TEST_SOURCE_RECORD_VALID));
    }

    @Test
    void select_NullSource() {
        PartitionSelector partitionSelector = new SourcePartitionSelector();
        assertEquals(EXPECTED_NULL_PARTITION, partitionSelector.select(TEST_SOURCE_RECORD_NULL));
    }
}
