package io.axual.connect.plugins.kafka.integrationtests.tools.providers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.connect.runtime.rest.entities.ConnectorInfo;

import java.time.Duration;
import java.util.Map;
import java.util.Optional;

/**
 * A wrapper class to easily implement all Kafka Connect Operations
 */
public interface ProvideConnectOperations {
    /**
     * Create a connector with the provided name and configuration
     *
     * @param connectorName   the name of the connector.
     * @param connectorConfig the configuration for the connector, including the connector class and expected class name
     * @return Information about the created connector
     */
    ConnectorInfo createConnector(String connectorName, Map<String, String> connectorConfig);

    /**
     * Confirm that a connector with an exact number of tasks is running.
     *
     * @param connectorName the connector
     * @return Optional(true) if the connector and tasks are in RUNNING state; Optional(false) otherwise; empty Optional if error.
     */
    Optional<Boolean> isConnectorRunning(final String connectorName, final int numTasks);

    /**
     * Wait up to the provided timeout for the connector with the given connectorName to start the specified number of tasks.
     *
     * @param connectorName the connectorName of the connector
     * @param numTasks      the minimum number of tasks that are expected
     * @param timeout       The amount of time
     */
    void waitForConnectorAndTasksToStart(String connectorName, int numTasks, Duration timeout);
}
