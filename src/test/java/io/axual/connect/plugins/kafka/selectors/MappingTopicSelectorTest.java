package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.source.SourceRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import io.axual.connect.plugins.kafka.source.KafkaSourceTask;
import lombok.Builder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MappingTopicSelectorTest {

    private TopicSelector topicSelector;

    private Map<String, Object> config;

    @BeforeEach
    void createFixture() {
        topicSelector = new MappingTopicSelector();
        config = new HashMap<>();
    }

    @Test
    void emptyConfigThrowsException() {
        assertThrows(ConfigException.class, () -> topicSelector.configure(config));
    }

    @Test
    void mappingFoundReturnsConfigured() {
        // given a mapping for topic input1 to output1
        config.put("mapping.input1", "output1");
        config.put("mapping.input2", "output2");
        topicSelector.configure(config);

        // and a source record coming from input1
        ConnectRecord record = sourceRecord().topic("input1").build();

        // then it will be mapped to output1
        assertEquals("output1", topicSelector.select(record));

        // same for SinkRecord
        record = sinkRecord().topic("input1").build();
        assertEquals("output1", topicSelector.select(record));
    }

    @Test
    void mappingNotFoundReturnsNull() {
        // given a configured MappingTopicSelector
        config.put("mapping.input1", "output1");
        topicSelector.configure(config);

        // and a source record with a topic for which there is no mapping
        final ConnectRecord sourceRecord = sourceRecord().topic("othertopic").build();

        // then the result of the selector throws IllegalStateException
        assertThrows(IllegalStateException.class, () -> topicSelector.select(sourceRecord));

        // same for SinkRecord
        final ConnectRecord sinkRecord = sinkRecord().topic("othertopic").build();
        assertThrows(IllegalStateException.class, () -> topicSelector.select(sinkRecord));
    }

    /**
     * Use lombok to create a fluid method of building Source- and SinkRecords.
     * @return
     */
    @Builder(builderMethodName = "sourceRecord")
    static SourceRecord createSourceRecord(String topic) {
        Map<String, Object> sourcePartition = new HashMap<>();
        sourcePartition.put(KafkaSourceTask.TOPIC, topic);
        sourcePartition.put(KafkaSourceTask.PARTITION, 0);

        Map<String, Object> sourceOffset = new HashMap<>();
        sourceOffset.put(KafkaSourceTask.OFFSET, 0);

        return new SourceRecord(sourcePartition, sourceOffset, topic, null,
                Schema.BYTES_SCHEMA, "",
                Schema.BYTES_SCHEMA, "", null, new ConnectHeaders());
    }

    @Builder(builderMethodName = "sinkRecord")
    static SinkRecord createSinkRecord(String topic) {
        return new SinkRecord(topic, 0, Schema.BYTES_SCHEMA, "", Schema.BYTES_SCHEMA, "", 0L);
    }
}
