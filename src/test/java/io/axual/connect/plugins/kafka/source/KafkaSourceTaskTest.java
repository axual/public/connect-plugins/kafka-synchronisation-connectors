package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.selectors.MappingTopicSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import lombok.Builder;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.header.Header;
import org.apache.kafka.connect.header.Headers;
import org.apache.kafka.connect.source.SourceRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.stream.IntStream;

import static io.axual.connect.plugins.kafka.source.KafkaSourceConfigDefinitions.TOPIC_SELECTOR_CLASS_CONFIG;
import static io.axual.connect.plugins.kafka.selectors.TopicSelector.TOPIC_SELECTOR_CONFIG;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class KafkaSourceTaskTest {

    @Mock
    private KafkaConsumer<byte[], byte[]> mockConsumer;

    @Captor
    private ArgumentCaptor<Map<TopicPartition, OffsetAndMetadata>> commitCaptor;

    @Captor
    private ArgumentCaptor<Collection<TopicPartition>> topicPartitionCaptor;

    private final Map<String, String> taskConfig = KafkaSourceConfigTest.REQUIRED_CONFIG;

    private KafkaSourceTask sourceTask;

    @BeforeEach
    void createFixtures() {
        // create sourceTask with mock KafkaConsumer
        sourceTask = new KafkaSourceTask() {
            @Override
            KafkaConsumer<byte[], byte[]> createConsumer(Map<String, Object> config) {
                return mockConsumer;
            }
        };
        // TopicSelector type "fixed" is default, configure it
        taskConfig.put("topic.selector.target", "fixed-output");
    }

    @Test
    void versionIsReadFromConfigfile() {
        assertEquals("testing", sourceTask.version());
    }

    @Test
    void subscribeToConfiguredTopicsOnStart() {
        // given a config with a list of topics
        taskConfig.put(KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG, "topic1,topic2");

        // when the source task is started with it
        sourceTask.start(taskConfig);

        // it subscribes to all the topics
        verify(mockConsumer).subscribe(eq(Arrays.asList("topic1", "topic2")), any());
    }

    @Test
    void consumerIsClosedOnStop() {
        // given a started source task
        sourceTask.start(taskConfig);

        // when the task is stopped
        sourceTask.stop();

        // it closes the consumer
        verify(mockConsumer).close();
    }

    @Test
    void allConsumedRecordsAreConverted() throws InterruptedException {
        // given a started source task
        sourceTask.start(taskConfig);

        // and the KafkaConsumer returns 5 records
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("test-topic", 1, 5)));

        // when the poll() method is called
        final List<SourceRecord> polled = sourceTask.poll();

        // the output contains 5 source records
        assertEquals(5, polled.size());

        // each source record has the topic, partition and offset it was read from
        polled.forEach(sourceRecord -> {
            final Map<String, ?> sourcePartition = sourceRecord.sourcePartition();
            assertEquals("test-topic", sourcePartition.get("topic"), "the original topic should be kept");
            assertEquals(1, sourcePartition.get("partition"), "the original partition should be kept");
            final Map<String, ?> sourceOffset = sourceRecord.sourceOffset();
            assertTrue(sourceOffset.containsKey("offset"));
        });

    }

    @Test
    void staticHeadersAreSent() throws InterruptedException {
        // given a task started with a config containing static headers
        final HashMap<String, String> staticHeaderConfig = new HashMap<>(taskConfig);
        staticHeaderConfig.put(KafkaSourceConfigDefinitions.STATIC_HEADER_NAMES_CONFIG, "myheader");
        staticHeaderConfig.put("header.static.myheader.name", "X-Static-MyHeader");
        staticHeaderConfig.put("header.static.myheader.value", "my-configured-value");

        sourceTask.start(staticHeaderConfig);

        // when consumerRecords are polled
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("test-topic", 1, 1)));
        final List<SourceRecord> poll = sourceTask.poll();

        // the static header is added to the SourceRecords
        final Headers headers = poll.get(0).headers();
        final Iterator<Header> headerIterator = headers.allWithName("X-Static-MyHeader");
        assertTrue(headerIterator.hasNext(), "the static header should be present in the SourceRecord");

        // and the static header has the configured key and value.
        final Header header = headerIterator.next();
        assertEquals("X-Static-MyHeader", header.key());
        assertArrayEquals("my-configured-value".getBytes(), (byte[]) header.value());
    }

    @Test
    void specialHeadersAreSent() throws InterruptedException {
        // given a task started with a config containing special headers
        final Map<String, String> config = new HashMap<>(taskConfig);
        config.put(KafkaSourceConfigDefinitions.TOPIC_HEADER_NAME_CONFIG, "X-Topic");
        config.put(KafkaSourceConfigDefinitions.PARTITION_HEADER_NAME_CONFIG, "X-Partition");
        config.put(KafkaSourceConfigDefinitions.OFFSET_HEADER_NAME_CONFIG, "X-Offset");
        config.put(KafkaSourceConfigDefinitions.TIMESTAMP_HEADER_NAME_CONFIG, "X-Timestamp");
        sourceTask.start(config);

        // when a consumerRecords is polled
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("mytopic", 1, 1)));
        final List<SourceRecord> poll = sourceTask.poll();

        // the special headers should be present in the produced SourceRecord
        final Headers headers = poll.get(0).headers();

        verifyHeaderPresent("X-Topic", headers).withValue("mytopic");
        verifyHeaderPresent("X-Partition", headers).withValue(1);
        verifyHeaderPresent("X-Offset", headers).withValue(0L);

        // since the mock produce does not set a timestamp, it defaults to -1
        verifyHeaderPresent("X-Timestamp", headers).withValue(-1L);

        // since the mock produce does not set a timestamp, the timestamp type defaults to NO_TIMESTAMP_TYPE
        verifyHeaderPresent("X-Timestamp-Type", headers).withValue(TimestampType.NO_TIMESTAMP_TYPE.name());
    }

    @Test
    void originalHeadersAreSentWithPrefix() throws InterruptedException {
        // given a task started with a config that specifies a remote header prefix
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        config.put(KafkaSourceConfigDefinitions.REMOTE_HEADER_PREFIX_CONFIG, "X-PFX-");
        sourceTask.start(config);

        // and given that, when consumerrecords are polled, a record with some headers are returned
        final Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> records = mockConsumerRecords("sometopic", 1, 1);
        records.get(new TopicPartition("sometopic", 1)).forEach(
                record -> {
                    record.headers().add("header1", "value1".getBytes(UTF_8));
                    record.headers().add("header2", "value2".getBytes(UTF_8));
                    record.headers().add("header3", "value3".getBytes(UTF_8));
                });
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(records));

        // all headers are copied, with the prefix in front, and the correct value
        final List<SourceRecord> sourceRecords = sourceTask.poll();
        final Headers headers = sourceRecords.get(0).headers();
        verifyHeaderPresent("X-PFX-header1", headers).withValue("value1");
        verifyHeaderPresent("X-PFX-header2", headers).withValue("value2");
        verifyHeaderPresent("X-PFX-header3", headers).withValue("value3");
    }

    @Test
    void prefixIsEmptyByDefault() throws InterruptedException {
        // given a task started with a config that does not specify a remote header prefix
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        sourceTask.start(config);

        // and given that, when consumerrecords are polled, a record with some headers are returned
        final Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> records = mockConsumerRecords("sometopic", 1, 1);
        records.get(new TopicPartition("sometopic", 1)).forEach(
                record -> {
                    record.headers().add("header1", "value1".getBytes(UTF_8));
                    record.headers().add("header2", "value2".getBytes(UTF_8));
                    record.headers().add("header3", "value3".getBytes(UTF_8));
                });
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(records));

        // all headers are copied with the names as-is, and the correct value
        final List<SourceRecord> sourceRecords = sourceTask.poll();
        final Headers headers = sourceRecords.get(0).headers();
        verifyHeaderPresent("header1", headers).withValue("value1");
        verifyHeaderPresent("header2", headers).withValue("value2");
        verifyHeaderPresent("header3", headers).withValue("value3");
    }

    @Test
    void inputTopicsCanBeMapped() throws InterruptedException {
        // given a task configured with a topic mapping
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        config.put(TOPIC_SELECTOR_CONFIG, TopicSelector.Type.mapping.name());
        config.put("topic.selector.mapping.topic1", "output1");
        config.put("topic.selector.mapping.topic2", "output2");
        sourceTask.start(config);

        // when a consumerRecord is polled, it is mapped to the configured topic
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic1", 1, 1)));
        List<SourceRecord> sourceRecords = sourceTask.poll();
        sourceRecords.forEach(record -> assertEquals("output1", record.topic()));

        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic2", 1, 1)));
        sourceRecords = sourceTask.poll();
        sourceRecords.forEach(record -> assertEquals("output2", record.topic()));
    }

    @Test
    void inputTopicSelectorClassTopicsCanBeMappedClass() throws InterruptedException {
        // given a task configured with a topic mapping
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        config.put(TOPIC_SELECTOR_CLASS_CONFIG, MappingTopicSelector.class.getName());
        config.put("topic.selector.mapping.topic1", "output1");
        config.put("topic.selector.mapping.topic2", "output2");
        sourceTask.start(config);

        // when a consumerRecord is polled, it is mapped to the configured topic
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic1", 1, 1)));
        List<SourceRecord> sourceRecords = sourceTask.poll();
        sourceRecords.forEach(record -> assertEquals("output1", record.topic()));

        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic2", 1, 1)));
        sourceRecords = sourceTask.poll();
        sourceRecords.forEach(record -> assertEquals("output2", record.topic()));
    }

    @Test
    void unknownTopicResultsInRuntimeException() throws InterruptedException {
        // given a task configured with a topic mapping
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        config.put("topic.selector", TopicSelector.Type.mapping.name());
        config.put("topic.selector.mapping.topic1", "output1");
        config.put("topic.selector.mapping.topic2", "output2");
        sourceTask.start(config);

        // when a consumerRecord is polled from a non configured source topic, we get an exception
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic3", 1, 1)));

        try {
            sourceTask.poll();
        } catch (RuntimeException e) {
            assertEquals("No output topic mapping found for input topic: topic3", e.getMessage());
        }
    }

    @Test
    void committedRecordsAreCommittedOnPoll() throws InterruptedException {
        // given a started task
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        sourceTask.start(config);

        // when SourceRecords are committed on the task for 2 partitions
        SourceRecord record1 = mockSourceRecord().topic("atopic").partition(1).offset(1L).build();
        SourceRecord record2 = mockSourceRecord().topic("atopic").partition(1).offset(2L).build();
        SourceRecord record3 = mockSourceRecord().topic("atopic").partition(2).offset(1L).build();
        sourceTask.commitRecord(record1);
        sourceTask.commitRecord(record2);
        sourceTask.commitRecord(record3);

        // on the next poll()
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic3", 1, 1)));
        sourceTask.poll();

        // the received SourceRecord commits are sent to the consumer
        verify(mockConsumer).commitSync(commitCaptor.capture());

        // we have consumer commits for 2 partitions
        final Map<TopicPartition, OffsetAndMetadata> capturedCommit = commitCaptor.getValue();
        assertEquals(2, capturedCommit.size(), "2 topic/partitions should be committed");

        // and the latest received offset is in the commit for each topic/partition
        final OffsetAndMetadata partition1Meta = capturedCommit.get(new TopicPartition("atopic", 1));
        assertEquals(3L, partition1Meta.offset(), "the latest committed offset, plus one, for partition 1 should be in the commit");
        final OffsetAndMetadata partition2Meta = capturedCommit.get(new TopicPartition("atopic", 2));
        assertEquals(2L, partition2Meta.offset(), "the latest committed offset, plus one, for partition 2 should be in the commit");
    }

    @Test
    void committedRecordsAreCommittedOnStop() throws InterruptedException {
        // given a started task
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        sourceTask.start(config);

        // when SourceRecords are committed on the task
        SourceRecord record2 = mockSourceRecord().topic("atopic").partition(1).offset(2L).build();
        SourceRecord record3 = mockSourceRecord().topic("atopic").partition(2).offset(1L).build();
        sourceTask.commitRecord(record2);
        sourceTask.commitRecord(record3);

        // when the task is stopped
        sourceTask.stop();

        // it will try to clean up by sending outstanding commits
        verify(mockConsumer).commitSync(commitCaptor.capture());
        verify(mockConsumer).close();

        // we have consumer commits for 2 partitions
        final Map<TopicPartition, OffsetAndMetadata> capturedCommit = commitCaptor.getValue();
        assertEquals(2, capturedCommit.size(), "2 topic/partitions should be committed");

        // and the latest received offset is in the commit for each topic/partition
        final OffsetAndMetadata partition1Meta = capturedCommit.get(new TopicPartition("atopic", 1));
        assertEquals(3L, partition1Meta.offset(), "the latest committed offset, plus one, for partition 1 should be in the commit");
        final OffsetAndMetadata partition2Meta = capturedCommit.get(new TopicPartition("atopic", 2));
        assertEquals(2L, partition2Meta.offset(), "the latest committed offset, plus one, for partition 2 should be in the commit");
    }

    @Test
    void topicPartitionsArePausedAfterPoll() throws InterruptedException {
        // given a started task
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        sourceTask.start(config);

        // and 3 records are returned when polled
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic3", 1, 3)));
        sourceTask.poll();

        // the corresponding topic/partition is paused
        verify(mockConsumer).pause(topicPartitionCaptor.capture());
        final Collection<TopicPartition> pausedPartitions = topicPartitionCaptor.getValue();
        assertEquals(1, pausedPartitions.size());
        assertTrue(pausedPartitions.contains(new TopicPartition("topic3", 1)));
    }

    @Test
    void topicPartitionsAreResumedAfterCommit() throws InterruptedException {
        // given a started task
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        sourceTask.start(config);

        // and 3 records are returned when polled, pausing the partition
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic3", 1, 3)));
        sourceTask.poll();
        verify(mockConsumer).pause(any());

        // then only after 3 commits have been received
        sourceTask.commitRecord(sourceRecord("topic3", 1, 1L));
        sourceTask.commitRecord(sourceRecord("topic3", 1, 2L));
        sourceTask.commitRecord(sourceRecord("topic3", 1, 3L));

        // the consumer is resumed on the next poll
        sourceTask.poll();
        verify(mockConsumer).resume(topicPartitionCaptor.capture());
        final Collection<TopicPartition> pausedPartitions = topicPartitionCaptor.getValue();
        assertEquals(1, pausedPartitions.size());
        assertTrue(pausedPartitions.contains(new TopicPartition("topic3", 1)));
    }

    @Test
    void topicPartitionsAreNotResumedIfNotAllMessagesCommitted() throws InterruptedException {
        // given a started task
        final HashMap<String, String> config = new HashMap<>(taskConfig);
        sourceTask.start(config);

        // and 3 records are returned when polled, pausing the partition
        when(mockConsumer.poll(any())).thenReturn(new ConsumerRecords<>(mockConsumerRecords("topic3", 1, 3)));
        sourceTask.poll();
        verify(mockConsumer).pause(topicPartitionCaptor.capture());

        // then if only 2 commits have been received
        sourceTask.commitRecord(sourceRecord("topic3", 1, 1L));
        sourceTask.commitRecord(sourceRecord("topic3", 1, 2L));

        // the consumer is not resumed on the next poll
        sourceTask.poll();
        verify(mockConsumer, never()).resume(topicPartitionCaptor.capture());
    }

    /**
     * Create a mocked Kafka consume poll result for the given topic, partition and number of records.
     * @param topic the simulated topic.
     * @param partition the simulated partition.
     * @param nrOfRecords number of records to create.
     */
    private Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> mockConsumerRecords(String topic, int partition, int nrOfRecords) {
        final List<ConsumerRecord<byte[], byte[]>> consumerRecords =
                IntStream.range(0, nrOfRecords)
                .mapToObj(offset -> new ConsumerRecord<>(topic, partition, offset,
                        ("key" + offset).getBytes(UTF_8), ("value" + offset).getBytes(UTF_8)))
                .toList();
        Map<TopicPartition, List<ConsumerRecord<byte[], byte[]>>> result = new HashMap<>();
        result.put(new TopicPartition(topic, partition), consumerRecords);
        return result;
    }

    /**
     * Utility method to verify header contents in a fluid manner (method chaining).
     * @param headerName a headername to verify.
     * @param headers the headers to search the header in.
     * @return a ValueChecker instance to chain a call on.
     */
    private ValueChecker verifyHeaderPresent(String headerName, Headers headers) {
        final Iterator<Header> foundHeaders = headers.allWithName(headerName);
        if (!foundHeaders.hasNext()) {
            return expectedValue -> fail(String.format("No header with name '%s' found", headerName));
        } else {
            return expectedValue -> {
                byte[] expectedBytes = new byte[0];
                if (expectedValue instanceof String) {
                    expectedBytes = ((String) expectedValue).getBytes();
                } else if (expectedValue instanceof Long) {
                    expectedBytes = toBytes((Long) expectedValue);
                } else if (expectedValue instanceof Integer) {
                    expectedBytes = toBytes((Integer) expectedValue);
                } else {
                    fail("Unable to check expected value of type " + expectedValue.getClass().getName());
                }
                byte[] valueToCheck = (byte[]) foundHeaders.next().value();
                assertArrayEquals(valueToCheck, expectedBytes);
            };
        }
    }

    /**
     * Functional interface to verify a header value in a dynamic way.
     */
    private interface ValueChecker {
        void withValue(Object expectedValue);
    }

    /**
     * Use lombok to create a fluid method of building SourceRecords.
     * @return a {@link SourceRecord} instance.
     */
    @Builder(builderMethodName = "mockSourceRecord")
    static SourceRecord sourceRecord(String topic, int partition, long offset) {
        Map<String, Object> sourcePartition = new HashMap<>();
        sourcePartition.put(KafkaSourceTask.TOPIC, topic);
        sourcePartition.put(KafkaSourceTask.PARTITION, partition);

        Map<String, Object> sourceOffset = new HashMap<>();
        sourceOffset.put(KafkaSourceTask.OFFSET, offset);

        return new SourceRecord(sourcePartition, sourceOffset, "targetTopic", null,
                Schema.BYTES_SCHEMA, "",
                Schema.BYTES_SCHEMA, "", null, new ConnectHeaders());
    }

    private byte[] toBytes(Long value) {
        final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(value);
        return buffer.array();
    }

    private byte[] toBytes(Integer value) {
        final ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(value);
        return buffer.array();
    }


}
