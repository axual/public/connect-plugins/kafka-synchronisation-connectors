package io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import eu.rekawek.toxiproxy.Proxy;
import eu.rekawek.toxiproxy.ToxiproxyClient;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.KafkaCluster;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.testcontainers.containers.ToxiproxyContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.lifecycle.Startable;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class ToxiProxyKafka implements Startable, KafkaCluster {
    private static final String KAFKA_NETWORK_ALIAS = "broker1";
    public static final String TOXIC_LISTENER_NAME = "toxyproxy";
    public static final String DIRECT_LISTENER_NAME = "direct";
    private static final int TOXY_PROXY_PORT = 8666;
    private static final DockerImageName TOXIPROXY_IMAGE =
            DockerImageName.parse("ghcr.io/shopify/toxiproxy:2.5.0")
                    .asCompatibleSubstituteFor("shopify/toxiproxy");

    private static final AtomicInteger INSTANCE_COUNTER = new AtomicInteger(1);

    @Getter
    private final ToxiproxyContainer proxyContainer = new ToxiproxyContainer(TOXIPROXY_IMAGE)
            .withAccessToHost(true)
            .withStartupAttempts(3)
            .withStartupTimeout(Duration.ofSeconds(30))
            .withCreateContainerCmdModifier(cmd -> cmd.getHostConfig().withMemory(
                    32L * 1024 * 1024).withCpuPeriod(100_000L).withCpuQuota(10_000L));

    private final String logPrefix;

    @Getter
    private Proxy toxicProxy;

    private final KafkaContainer kafkaContainer = new KafkaContainer()
            .withExposedListener(DIRECT_LISTENER_NAME)
            .withNetworkListener(TOXIC_LISTENER_NAME, KAFKA_NETWORK_ALIAS)
            .withStartupAttempts(3)
            .withStartupTimeout(Duration.ofSeconds(30));


    private final KafkaOperations kafkaOperations;

    public ToxiProxyKafka() {
        final var clusterId = "ToxiProxyKafka-" + INSTANCE_COUNTER.getAndIncrement();
        this.logPrefix = clusterId + " - ";
        this.kafkaOperations = new KafkaOperations(kafkaContainer, clusterId);

        this.proxyContainer.setLogConsumers(
                List.of(new Slf4jLogConsumer(log, true)
                        .withPrefix(logPrefix + "Main")
                        .withMdc("cluster", clusterId)
                        .withMdc("component", "Main")
                ));
        this.kafkaContainer.setLogConsumers(
                List.of(
                        new Slf4jLogConsumer(log, true)
                                .withPrefix(logPrefix + "Kafka")
                                .withMdc("cluster", clusterId)
                                .withMdc("component", "Kafka")
                ));
    }

    public KafkaContainer kafka() {
        return kafkaContainer;
    }

    public void start() {
        log.info("{}Starting ToxiProxy Kafka", logPrefix);

        if (!proxyContainer.isRunning()) {
            proxyContainer.start();
        }

        final var advertisedExternalBrokerPort = proxyContainer.getMappedPort(TOXY_PROXY_PORT);
        final var advertisedExternalBrokerHost = proxyContainer.getHost();

        log.error("ToxiProxy has internal port '{}' listening externally on host '{}' and mapped port '{}'", TOXY_PROXY_PORT, advertisedExternalBrokerHost, advertisedExternalBrokerPort);

        kafkaContainer.withAdvertised(TOXIC_LISTENER_NAME, advertisedExternalBrokerHost, advertisedExternalBrokerPort);
        kafkaContainer.start();

        var kafkaHost = kafkaContainer.getCurrentIpAddress();
        var listenerPort = kafkaContainer.getListenerPort(TOXIC_LISTENER_NAME);

        final ToxiproxyClient toxiproxyClient = new ToxiproxyClient(proxyContainer.getHost(), proxyContainer.getControlPort());
        try {
            toxicProxy = toxiproxyClient.createProxy("kafkaProxy", "0.0.0.0:" + TOXY_PROXY_PORT, kafkaHost + ":" + listenerPort);
            toxicProxy.enable();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        log.info("{}Started ToxiProxy Kafka", logPrefix);
    }

    @Override
    public void stop() {
        log.info("{}Stopping ToxiProxy Kafka", logPrefix);
        proxyContainer.stop();
        kafkaContainer.stop();
        log.info("{}Stopped ToxiProxy Kafka", logPrefix);
    }

    @Override
    public void createTopic(final String name, final int partitions, final boolean compacted) {
        kafkaOperations.createTopic(name, partitions, compacted);
    }

    @Override
    public void deleteTopic(final String name) {
        kafkaOperations.deleteTopic(name);
    }

    @Override
    public ConsumerRecords<byte[], byte[]> consume(final String listenerName, final int expectedRecordCount, final long maxDuration, final String groupId, final Collection<String> topics) {
        return kafkaOperations.consume(listenerName, expectedRecordCount, maxDuration, groupId, topics);
    }

    @Override
    public long produce(final String listenerName, final ProducerRecord<byte[], byte[]> producerRecord) {
        return kafkaOperations.produce(listenerName, producerRecord);
    }

    @Override
    public String bootstrapServers() {
        return kafkaOperations.bootstrapServers();
    }

    @Override
    public String bootstrapServers(final String listenerName) {
        return kafkaOperations.bootstrapServers(listenerName);
    }

    @Override
    public Map<String, Object> clientConnectionProperties() {
        return kafkaOperations.clientConnectionProperties();
    }

    @Override
    public Map<String, Object> clientConnectionProperties(final String listenerName) {
        return kafkaOperations.clientConnectionProperties(listenerName);
    }

    @Override
    public Map<String, Object> producerProperties(final String listenerName, final String transactionalId) {
        return kafkaOperations.producerProperties(listenerName, transactionalId);
    }

    @Override
    public Map<String, Object> consumerProperties(final String listenerName, final String groupId) {
        return kafkaOperations.consumerProperties(listenerName, groupId);
    }
}
