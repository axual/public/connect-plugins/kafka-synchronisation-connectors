package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2023 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.KafkaException;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ProducerFactoryTest {

    private static final String REMOTE_PREFIX = "remote.";

    ProducerFactory factory;

    @Test
    void createProducer_should_throwKafkaException_when_propsAreEmpty() {
        Map<String, String> minimumConfigs = new HashMap<>();
        minimumConfigs.put(REMOTE_PREFIX + ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "bootstrap.url");
        final KafkaSinkConfig config = new KafkaSinkConfig(minimumConfigs);
        factory = new ProducerFactory();

        assertThrows(KafkaException.class, () -> factory.createProducer(config));
    }

}