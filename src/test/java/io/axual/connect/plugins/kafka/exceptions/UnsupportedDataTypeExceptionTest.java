package io.axual.connect.plugins.kafka.exceptions;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.jupiter.api.Test;

import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UnsupportedDataTypeExceptionTest {
    private static final Class<?> CLASS_TO_FAIL = ConcurrentHashMap.class;
    private static final String EXPECTED_CONTAINS_CLASS_NAME = CLASS_TO_FAIL.getName();

    @Test
    void construction() {
        UnsupportedDataTypeException toTest = new UnsupportedDataTypeException(CLASS_TO_FAIL);
        final String message = toTest.getMessage();
        assertNotNull(message);
        assertTrue(message.contains(EXPECTED_CONTAINS_CLASS_NAME), "Class name is expected in the exception message");
        assertEquals(CLASS_TO_FAIL, toTest.getUnsupportedClass());
    }
}