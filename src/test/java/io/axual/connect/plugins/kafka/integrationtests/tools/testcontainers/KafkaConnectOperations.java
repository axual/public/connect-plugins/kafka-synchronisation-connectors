package io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.integrationtests.tools.providers.ProvideClientProperties;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.ProvideConnectClient;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.ProvideConnectOperations;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.connect.runtime.AbstractStatus;
import org.apache.kafka.connect.runtime.rest.entities.ConnectorInfo;
import org.apache.kafka.connect.runtime.rest.entities.ConnectorStateInfo;

import java.time.Duration;
import java.util.Map;
import java.util.Optional;

import static org.awaitility.Awaitility.await;

@Slf4j
public class KafkaConnectOperations extends KafkaOperations implements ProvideConnectOperations {

    private final ProvideConnectClient connect;

    public KafkaConnectOperations(ProvideConnectClient connectClientProvider, ProvideClientProperties clientPropertiesProvider, String logPrefix) {
        super(clientPropertiesProvider, logPrefix);
        this.connect = connectClientProvider;
    }

    @Override
    public ConnectorInfo createConnector(String connectorName, Map<String, String> connectorConfig) {
        return connect.connectClient().createConnector(connectorName, connectorConfig);
    }

    @Override
    public Optional<Boolean> isConnectorRunning(final String connectorName, final int numTasks) {
        try {
            ConnectorStateInfo info = connect.connectClient().getConnectorStatus(connectorName);
            boolean result = info != null
                    && info.tasks().size() >= numTasks
                    && info.connector().state().equals(AbstractStatus.State.RUNNING.toString())
                    && info.tasks().stream().allMatch(s -> s.state().equals(AbstractStatus.State.RUNNING.toString()));
            return Optional.of(result);
        } catch (Exception e) {
            log.warn("{}Could not check connector status. {}", logPrefix, e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public void waitForConnectorAndTasksToStart(String connectorName, int numTasks, Duration timeout) {
        log.info("{}waitForConnectorAndTasksToStart({}, {}, {})", logPrefix, connectorName, numTasks, timeout);
        await(String.format("Connector %s with %d tasks did not start in time.", connectorName, numTasks))
                .atMost(timeout)
                .until(() -> isConnectorRunning(connectorName, numTasks).orElse(false));
    }
}
