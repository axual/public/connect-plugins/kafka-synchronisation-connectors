package io.axual.connect.plugins.kafka.integrationtests;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.integrationtests.tools.providers.StartableKafkaCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.StartableKafkaConnectCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers.TestcontainerKafkaCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers.TestcontainerKafkaConnectCluster;
import io.axual.connect.plugins.kafka.sink.KafkaSinkConfig;
import io.axual.connect.plugins.kafka.sink.KafkaSinkConnector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.connect.converters.ByteArrayConverter;
import org.apache.kafka.connect.runtime.ConnectorConfig;
import org.apache.kafka.connect.runtime.SinkConnectorConfig;
import org.apache.kafka.connect.runtime.rest.entities.ConnectorInfo;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
class KafkaSinkIT {
    public static final String K_FORMAT = "Key %d";
    public static final String V_FORMAT = "Value %d";
    public static final String H_NAME_FORMAT = "Header %d";
    public static final String H_VALUE_FORMAT = "Value %d_%d";
    public static final int RECORD_COUNT = 10;
    public static final int HEADER_COUNT = 2;
    private static final Logger LOG = LoggerFactory.getLogger(KafkaSinkIT.class);

    @Container
    StartableKafkaConnectCluster local = new TestcontainerKafkaConnectCluster("local");

    @Container
    StartableKafkaCluster remote = new TestcontainerKafkaCluster("remote");

    private static final String SINGLE_PARTITION_TOPIC = "test-topic";
    private static final String MULTI_PARTITION_TOPIC = "test-topic-multiple-partition";
    private static final int SINGLE_PARTITION = 1;
    private static final int FIVE_PARTITIONS = 5;
    private static final int PRODUCE_PARTITION = 0;
    private static final String CONNECTOR_NAME = "passthrough-test";

    private static final String STATIC_HEADER_1_NAME = "Test-Static-Name-1";
    private static final String STATIC_HEADER_1_VALUE = "Test-Static-Value-1";
    private static final byte[] EXPECTED_STATIC_HEADER_1_VALUE = STATIC_HEADER_1_VALUE.getBytes(StandardCharsets.UTF_8);
    private static final String STATIC_HEADER_2_NAME = "Test-Static-Name-2";
    private static final String STATIC_HEADER_2_VALUE = "Test-Static-Value-2";
    private static final byte[] EXPECTED_STATIC_HEADER_2_VALUE = STATIC_HEADER_2_VALUE.getBytes(StandardCharsets.UTF_8);

    @Test
    void produceOnSinglePartitionTopicRemoteCluster() {
        local.createTopic(SINGLE_PARTITION_TOPIC, SINGLE_PARTITION);
        remote.createTopic(SINGLE_PARTITION_TOPIC, SINGLE_PARTITION);
        setupConnector(SINGLE_PARTITION_TOPIC);

        List<ProducerRecord<byte[], byte[]>> producedRecords = IntStream.range(0, RECORD_COUNT)
                .mapToObj(ri -> new ProducerRecord<>(
                        SINGLE_PARTITION_TOPIC,
                        PRODUCE_PARTITION,
                        String.format(K_FORMAT, ri).getBytes(StandardCharsets.UTF_8),
                        String.format(V_FORMAT, ri).getBytes(StandardCharsets.UTF_8),
                        IntStream.range(0, HEADER_COUNT)
                                .mapToObj(hi -> new RecordHeader(
                                        String.format(H_NAME_FORMAT, hi),
                                        String.format(H_VALUE_FORMAT, ri, hi).getBytes(StandardCharsets.UTF_8)
                                )).collect(Collectors.toList())
                ))
                .toList();

        assertTrue(produceMessages(producedRecords));
        final ConsumerRecords<byte[], byte[]> consumedRemoteRecords = remote.consume(producedRecords.size(), TimeUnit.SECONDS.toMillis(20), SINGLE_PARTITION_TOPIC);
        assertEquals(RECORD_COUNT, consumedRemoteRecords.count());

        // Create a queue to verify ordering of messages
        final Queue<ConsumerRecord<byte[], byte[]>> toVerify = new ArrayBlockingQueue<>(consumedRemoteRecords.count());
        consumedRemoteRecords.forEach(toVerify::add);

        for (ProducerRecord<byte[], byte[]> produced : producedRecords) {
            boolean match = false;
            while (toVerify.peek() != null) {
                ConsumerRecord<byte[], byte[]> consumed = toVerify.poll();
                if (Arrays.equals(produced.key(), consumed.key())
                        && Arrays.equals(produced.value(), consumed.value())
                        && produced.partition() == consumed.partition()
                        && containsStaticHeaders(consumed)
                        && matchProducedHeaders(produced, consumed)
                ) {
                    match = true;
                    break;
                }
            }
            if (!match) {
                fail(String.format("Could not find matching remote record for local record %s", produced));
            }
        }
    }

    @Test
    void produceOnMultiPartitionTopicRemoteCluster() {
        local.createTopic(MULTI_PARTITION_TOPIC, FIVE_PARTITIONS);
        remote.createTopic(MULTI_PARTITION_TOPIC, FIVE_PARTITIONS);
        setupConnector(MULTI_PARTITION_TOPIC);

        List<ProducerRecord<byte[], byte[]>> producedRecords = IntStream.range(0, RECORD_COUNT)
                .mapToObj(ri -> new ProducerRecord<>(
                        MULTI_PARTITION_TOPIC,
                        ri % FIVE_PARTITIONS,
                        String.format(K_FORMAT, ri).getBytes(StandardCharsets.UTF_8),
                        String.format(V_FORMAT, ri).getBytes(StandardCharsets.UTF_8),
                        IntStream.range(0, HEADER_COUNT)
                                .mapToObj(hi -> new RecordHeader(
                                        String.format(H_NAME_FORMAT, hi),
                                        String.format(H_VALUE_FORMAT, ri, hi).getBytes(StandardCharsets.UTF_8)
                                )).collect(Collectors.toList())
                ))
                .toList();

        assertTrue(produceMessages(producedRecords), "Producing test messages should work");
        final ConsumerRecords<byte[], byte[]> consumedRemoteRecords = remote.consume(producedRecords.size(), TimeUnit.SECONDS.toMillis(20), MULTI_PARTITION_TOPIC);

        assertEquals(RECORD_COUNT, consumedRemoteRecords.count());

        Map<TopicPartition, List<ProducerRecord>> producedTopicPartitionMap = producedRecords.stream().collect(Collectors.groupingBy(r -> new TopicPartition(r.topic(), r.partition())));

        assertEquals(producedTopicPartitionMap.keySet().size(), consumedRemoteRecords.partitions().size());

        producedTopicPartitionMap.entrySet()
                .stream()
                .forEach(entry -> {
                    List<ProducerRecord> producerRecords = entry.getValue();
                    List<ConsumerRecord<byte[], byte[]>> consumerRecords = consumedRemoteRecords.records(entry.getKey());

                    assertEquals(producerRecords.size(), consumerRecords.size());
                    IntStream.range(0, producerRecords.size()).forEach(i -> {
                        ProducerRecord<byte[], byte[]> produced = producerRecords.get(i);
                        ConsumerRecord<byte[], byte[]> consumed = consumerRecords.get(i);

                        if (!(Arrays.equals(produced.key(), consumed.key())
                                && Arrays.equals(produced.value(), consumed.value())
                                && produced.partition() == consumed.partition()
                                && containsStaticHeaders(consumed)
                                && matchProducedHeaders(produced, consumed))) {
                            fail(String.format("Could not find matching remote record for local record %s", produced));
                        }
                    });
                });
    }

    void setupConnector(String topic) {
        Map<String, String> props = new HashMap<>();
        props.put(SinkConnectorConfig.NAME_CONFIG, CONNECTOR_NAME);
        props.put(SinkConnectorConfig.CONNECTOR_CLASS_CONFIG, KafkaSinkConnector.class.getName());
        props.put(SinkConnectorConfig.TOPICS_CONFIG, topic);
        for (Map.Entry<String, Object> entry : remote.producerProperties().entrySet()) {
            if (entry.getValue() != null) {
                props.put(KafkaSinkConfig.REMOTE_CONFIG_PREFIX + entry.getKey(), entry.getValue().toString());
            }
        }

        props.put(KafkaSinkConfig.REMOTE_CONFIG_PREFIX + ProducerConfig.ACKS_CONFIG, "all");
        props.put(KafkaSinkConfig.REMOTE_CONFIG_PREFIX + ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");
        props.put(ConnectorConfig.KEY_CONVERTER_CLASS_CONFIG, ByteArrayConverter.class.getName());
        props.put(ConnectorConfig.VALUE_CONVERTER_CLASS_CONFIG, ByteArrayConverter.class.getName());
        props.put(ConnectorConfig.HEADER_CONVERTER_CLASS_CONFIG, ByteArrayConverter.class.getName());

        props.put(KafkaSinkConfig.STATIC_HEADER_ALIAS_CONFIG, "h1,h2");
        props.put(String.format(KafkaSinkConfig.STATIC_HEADER_NAME_CONFIG_FORMAT, "h1"), STATIC_HEADER_1_NAME);
        props.put(String.format(KafkaSinkConfig.STATIC_HEADER_VALUE_CONFIG_FORMAT, "h1"), STATIC_HEADER_1_VALUE);
        props.put(String.format(KafkaSinkConfig.STATIC_HEADER_NAME_CONFIG_FORMAT, "h2"), STATIC_HEADER_2_NAME);
        props.put(String.format(KafkaSinkConfig.STATIC_HEADER_VALUE_CONFIG_FORMAT, "h2"), STATIC_HEADER_2_VALUE);

        ConnectorInfo info = local.connectClient().createConnector(CONNECTOR_NAME, props);
        LOG.info("Created Connector {}", info);
    }

    private boolean matchProducedHeaders(ProducerRecord<?, ?> produced, ConsumerRecord<?, ?> consumed) {
        return StreamSupport.stream(produced.headers().spliterator(), true)
                .allMatch(ph -> StreamSupport.stream(consumed.headers().spliterator(), true)
                        .anyMatch(ch -> ph.key().equals(ch.key()) && Arrays.equals(ph.value(), ch.value()))
                );
    }

    private boolean containsStaticHeaders(ConsumerRecord<?, ?> record) {
        if (record == null || record.headers() == null) {
            return false;
        }

        boolean header1Found = false;
        for (Header header : record.headers().headers(STATIC_HEADER_1_NAME)) {
            if (Arrays.equals(EXPECTED_STATIC_HEADER_1_VALUE, header.value())) {
                header1Found = true;
                break;
            }
        }
        boolean header2Found = false;
        for (Header header : record.headers().headers(STATIC_HEADER_2_NAME)) {
            if (Arrays.equals(EXPECTED_STATIC_HEADER_2_VALUE, header.value())) {
                header2Found = true;
                break;
            }
        }

        return header1Found && header2Found;
    }

    private boolean produceMessages(Collection<ProducerRecord<byte[], byte[]>> records) {
        LOG.info("Producing {} records to local cluster", records.size());
        final Map<String, Object> props = local.clientConnectionProperties();
        final String uuid = UUID.randomUUID().toString();
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, getClass().getSimpleName() + "-" + uuid);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, 1);
        props.put(ProducerConfig.RETRIES_CONFIG, 0);

        try (KafkaProducer<byte[], byte[]> producer = new KafkaProducer<>(props)) {
            for (ProducerRecord<byte[], byte[]> record : records) {
                producer.send(record).get();
            }
            LOG.info("Produced {} records to local cluster", records.size());
            return true;
        } catch (InterruptedException | ExecutionException e) {
            LOG.info("Exception while producing records", e);
            return false;
        }

    }
}
