package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.exceptions.SinkRunnerProduceFailedException;
import io.axual.connect.plugins.kafka.exceptions.UnsupportedDataTypeException;
import io.axual.connect.plugins.kafka.selectors.*;
import lombok.Builder;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.NotLeaderOrFollowerException;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.common.utils.Utils;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.header.Header;
import org.apache.kafka.connect.header.Headers;
import org.apache.kafka.connect.sink.SinkRecord;
import org.awaitility.Awaitility;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.axual.connect.plugins.kafka.sink.KafkaSinkConfig.*;
import static io.axual.connect.plugins.kafka.sink.KafkaSinkRunnerTest.PayloadType.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class KafkaSinkRunnerTest {
    private static final byte[] CONVERT_EXPECTED_BYTE_BUFFER = "ByteBufferTest".getBytes(StandardCharsets.UTF_8);
    private static final ByteBuffer CONVERT_INPUT_BYTE_BUFFER = ByteBuffer.wrap(CONVERT_EXPECTED_BYTE_BUFFER);
    private static final byte[] CONVERT_EXPECTED_BYTE_ARRAY = "ByteArrayTest".getBytes(StandardCharsets.UTF_8);
    private static final byte[] CONVERT_INPUT_BYTE_ARRAY = CONVERT_EXPECTED_BYTE_ARRAY;
    private static final Object CONVERT_EXPECTED_EXCEPTION = new Exception("Expected Exception");
    private static final byte[] CONVERT_EXPECTED_NULL = null;
    private static final String HEADER_PREFIX = "Header-Test-";

    private static final String TOPIC = "sink-test";
    private static final int PARTITION = 1;
    private static final long OFFSET = 12L;
    private static final long TIMESTAMP = 123456789L;

    private static final long MAX_WAIT_TIME = 500L;
    private static final int MAX_QUEUE_SIZE = 1;

    private static final String STATIC_HEADER_1_NAME = "Static Header One";
    private static final String STATIC_HEADER_1_VALUE = "Value One";
    private static final String STATIC_HEADER_2_NAME = "Static Header Two";
    private static final String STATIC_HEADER_2_VALUE = "Value Two";
    private static final Map<String, String> STATIC_HEADERS = createStaticHeaders();

    private static final String TOPIC_SELECTOR = TopicSelector.Type.source.name();
    private static final String PARTITION_SELECTOR = PARTITION_SELECTOR_SOURCE;
    private static final String FORWARD_TOPIC_HEADER = "Topic Forwarded";
    private static final String FORWARD_PARTITION_HEADER = "Partition Forwarded";
    private static final String FORWARD_OFFSET_HEADER = "Offset Forwarded";
    private static final String FORWARD_TIMESTAMP_HEADER = "Timestamp Forwarded";
    private static final TopicPartition EXPECTED_TOPIC_PARTITION = new TopicPartition(TOPIC, PARTITION);
    private static final long EXPECTED_OFFSET = OFFSET + 1;

    @Captor
    ArgumentCaptor<ProducerRecord<byte[], byte[]>> recordCaptor;
    @Mock
    Producer<byte[], byte[]> producer;
    @Mock(strictness = Mock.Strictness.LENIENT)
    ProducerFactory producerFactory;

    MockedConstruction<ProducerFactory> producerFactoryMockedConstruction;

    @Mock(strictness = Mock.Strictness.LENIENT)
    KafkaSinkConfig kafkaSinkConfig;

    private static Map<String, String> createStaticHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put(STATIC_HEADER_1_NAME, STATIC_HEADER_1_VALUE);
        headers.put(STATIC_HEADER_2_NAME, STATIC_HEADER_2_VALUE);
        return headers;
    }

    static Stream<Arguments> put_SuccessProduced() {
        return Stream.of(
                Arguments.of(TopicSelector.Type.source.name(), null, null, PARTITION_SELECTOR_SOURCE),
                Arguments.of(TopicSelector.Type.source.name(), null, null, PARTITION_SELECTOR_PARTITIONER),
                Arguments.of(TopicSelector.Type.fixed.name(), FixedTopicSelector.TOPIC_SELECTOR_TARGET, "targeted", PARTITION_SELECTOR_SOURCE),
                Arguments.of(TopicSelector.Type.fixed.name(), FixedTopicSelector.TOPIC_SELECTOR_TARGET, "targeted", PARTITION_SELECTOR_PARTITIONER),
                Arguments.of(TopicSelector.Type.prefix.name(), PrefixTopicSelector.TOPIC_SELECTOR_PREFIX, "prefixed-", PARTITION_SELECTOR_SOURCE),
                Arguments.of(TopicSelector.Type.prefix.name(), PrefixTopicSelector.TOPIC_SELECTOR_PREFIX, "prefixed-", PARTITION_SELECTOR_PARTITIONER)
        );
    }

    static Stream<Arguments> put_SuccessProducedClass() {
        return Stream.of(
                Arguments.of(SourceTopicSelector.class, null, null, PARTITION_SELECTOR_SOURCE),
                Arguments.of(SourceTopicSelector.class, null, null, PARTITION_SELECTOR_PARTITIONER),
                Arguments.of(FixedTopicSelector.class, FixedTopicSelector.TOPIC_SELECTOR_TARGET, "targeted", PARTITION_SELECTOR_SOURCE),
                Arguments.of(FixedTopicSelector.class, FixedTopicSelector.TOPIC_SELECTOR_TARGET, "targeted", PARTITION_SELECTOR_PARTITIONER),
                Arguments.of(PrefixTopicSelector.class, PrefixTopicSelector.TOPIC_SELECTOR_PREFIX, "prefixed-", PARTITION_SELECTOR_SOURCE),
                Arguments.of(PrefixTopicSelector.class, PrefixTopicSelector.TOPIC_SELECTOR_PREFIX, "prefixed-", PARTITION_SELECTOR_PARTITIONER)
        );
    }

    static Stream<Arguments> convertConnectRecordSource() {
        return Stream.of(
                convertRecordSourceArguments(NULL, NULL, Collections.emptyList()),
                convertRecordSourceArguments(NULL, BYTEARRAY, Collections.emptyList()),
                convertRecordSourceArguments(BYTEARRAY, NULL, Collections.emptyList()),
                convertRecordSourceArguments(BYTEARRAY, BYTEARRAY, Collections.emptyList()),
                convertRecordSourceArguments(NULL, BYTEBUFFER, Collections.emptyList()),
                convertRecordSourceArguments(BYTEBUFFER, NULL, Collections.emptyList()),
                convertRecordSourceArguments(BYTEBUFFER, BYTEBUFFER, Collections.emptyList()),
                convertRecordSourceArguments(NULL, EXCEPTION, Collections.emptyList()),
                convertRecordSourceArguments(EXCEPTION, NULL, Collections.emptyList()),
                convertRecordSourceArguments(EXCEPTION, EXCEPTION, Collections.emptyList()),
                convertRecordSourceArguments(NULL, NULL, Arrays.asList(NULL, BYTEARRAY, EXCEPTION, BYTEBUFFER, EXCEPTION)),
                convertRecordSourceArguments(NULL, NULL, Arrays.asList(NULL, EXCEPTION, NULL))
        );
    }

    static Arguments convertRecordSourceArguments(PayloadType keyType, PayloadType valueType, List<PayloadType> headerTypes) {
        final StringBuilder displayNameBuilder = new StringBuilder()
                .append("K-")
                .append(keyType.name())
                .append(" V-")
                .append(valueType.name())
                .append(" H-[");
        if (headerTypes != null && !headerTypes.isEmpty()) {
            displayNameBuilder.append(
                    headerTypes.stream()
                            .map(PayloadType::name)
                            .collect(Collectors.joining(", ")));
        }
        displayNameBuilder.append(" ]");
        final SinkRecord sinkRecord = createSinkRecord(keyType, valueType, headerTypes);
        final Object expectedRecord = expectedKafkaRecord(keyType, valueType, headerTypes);

        return Arguments.of(displayNameBuilder.toString(), sinkRecord, getValidator(expectedRecord), getExpectedException(expectedRecord));
    }

    static Class<? extends Throwable> getExpectedException(Object expected) {
        return expected == CONVERT_EXPECTED_EXCEPTION ? UnsupportedDataTypeException.class : null;
    }

    static Consumer<ProducerRecord<byte[], byte[]>> getValidator(Object expected) {
        if (expected == CONVERT_EXPECTED_EXCEPTION) {
            return (record) -> fail("Expecting a failure");
        }
        return (record) -> assertEquals(expected, record);
    }

    static Object expectedKafkaRecord(PayloadType keyType, PayloadType valueType, List<PayloadType> headerTypes) {
        return expectedKafkaRecord(TOPIC, PARTITION, keyType, valueType, headerTypes);
    }

    static Object expectedKafkaRecord(String topic, Integer partition, PayloadType keyType, PayloadType valueType, List<PayloadType> headerTypes) {
        final Object convertedKey = getConverted(keyType);
        final Object convertedValue = getConverted(valueType);
        final org.apache.kafka.common.header.Headers convertedHeaders = getKafkaHeaders(headerTypes);
        if (convertedKey == CONVERT_EXPECTED_EXCEPTION || convertedValue == CONVERT_EXPECTED_EXCEPTION) {
            return CONVERT_EXPECTED_EXCEPTION;
        }
        return new ProducerRecord<>(topic, partition, TIMESTAMP, convertedKey, convertedValue, convertedHeaders);
    }

    static org.apache.kafka.common.header.Headers getKafkaHeaders(List<PayloadType> payloadTypes) {
        RecordHeaders recordHeaders = new RecordHeaders();

        for (int i = 0; i < payloadTypes.size(); i++) {
            final Object headerValue = getConverted(payloadTypes.get(i));
            if (headerValue instanceof byte[] || headerValue == null) {
                recordHeaders.add(HEADER_PREFIX + i, (byte[]) headerValue);
            }
        }

        STATIC_HEADERS.forEach((k, v) -> recordHeaders.add(new RecordHeader(k, v.getBytes(StandardCharsets.UTF_8))));

        // Add the forwarded headers
        recordHeaders.add(FORWARD_TOPIC_HEADER, TOPIC.getBytes(StandardCharsets.UTF_8));
        recordHeaders.add(FORWARD_PARTITION_HEADER, serialize(PARTITION));
        recordHeaders.add(FORWARD_OFFSET_HEADER, serialize(OFFSET));
        recordHeaders.add(FORWARD_TIMESTAMP_HEADER, serialize(TIMESTAMP));

        return recordHeaders;
    }

    static byte[] serialize(Integer toSerialize) {
        final ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(toSerialize);
        return buffer.array();
    }

    static byte[] serialize(Long toSerialize) {
        final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(toSerialize);
        return buffer.array();
    }

    static SinkRecord createSinkRecord(PayloadType keyType, PayloadType valueType, List<PayloadType> headerTypes) {
        return createSinkRecord(TOPIC, PARTITION, keyType, valueType, headerTypes);
    }

    static SinkRecord createSinkRecord(String topic, Integer partition, PayloadType keyType, PayloadType valueType, List<PayloadType> headerTypes) {
        final Object key = getPayload(keyType);
        final Object value = getPayload(valueType);
        final Headers headers = getConnectHeaders(headerTypes);
        return new SinkRecord(topic, partition, null, key, null, value, OFFSET, TIMESTAMP, null, headers);
    }

    static Headers getConnectHeaders(List<PayloadType> payloadTypes) {
        ConnectHeaders connectHeaders = new ConnectHeaders();
        if (payloadTypes == null || payloadTypes.isEmpty()) {
            return connectHeaders;
        }

        for (int i = 0; i < payloadTypes.size(); i++) {
            connectHeaders.add(HEADER_PREFIX + i, getPayload(payloadTypes.get(i)), null);
        }

        return connectHeaders;
    }

    static Object getPayload(PayloadType payloadType) {
        return switch (payloadType) {
            case BYTEARRAY -> CONVERT_INPUT_BYTE_ARRAY;
            case BYTEBUFFER -> CONVERT_INPUT_BYTE_BUFFER;
            case NULL -> null;
            case EXCEPTION -> 0;
        };
    }

    static Object getConverted(PayloadType payloadType) {
        return switch (payloadType) {
            case BYTEARRAY -> CONVERT_EXPECTED_BYTE_ARRAY;
            case BYTEBUFFER -> CONVERT_EXPECTED_BYTE_BUFFER;
            case NULL -> CONVERT_EXPECTED_NULL;
            case EXCEPTION -> CONVERT_EXPECTED_EXCEPTION;
        };
    }

    @BeforeEach
    void prepareConfig() {
        doReturn(MAX_QUEUE_SIZE).when(kafkaSinkConfig).getQueueSize();
        doReturn(MAX_WAIT_TIME).when(kafkaSinkConfig).getQueuePutWaitMs();
        doReturn(STATIC_HEADERS).when(kafkaSinkConfig).getStaticHeaders();
        doReturn(Collections.emptyMap()).when(kafkaSinkConfig).getRemoteConfig();
        doReturn(Optional.of(FORWARD_TOPIC_HEADER)).when(kafkaSinkConfig).getTopicHeader();
        doReturn(Optional.of(FORWARD_PARTITION_HEADER)).when(kafkaSinkConfig).getPartitionHeader();
        doReturn(Optional.of(FORWARD_OFFSET_HEADER)).when(kafkaSinkConfig).getOffsetHeader();
        doReturn(Optional.of(FORWARD_TIMESTAMP_HEADER)).when(kafkaSinkConfig).getTimestampHeader();
        doReturn(TOPIC_SELECTOR).when(kafkaSinkConfig).getTopicSelector();
        doReturn(PARTITION_SELECTOR).when(kafkaSinkConfig).getPartitionSelector();
        doReturn("").when(kafkaSinkConfig).getHeaderPrefix();
        doReturn(producer).when(producerFactory).createProducer(any());

        producerFactoryMockedConstruction = mockConstruction(ProducerFactory.class,
                (mock, context) -> when(mock.createProducer(any())).thenReturn(producer)
        );
    }

    @AfterEach
    void closeMocks() {
        producerFactoryMockedConstruction.close();
    }

    @Test
    void put_QueueFull() throws InterruptedException {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        assertTrue(runner.put(Collections.emptyList()));
        assertFalse(runner.put(Collections.emptyList()));
    }

    @Test
    void put_QueueAvailable() throws InterruptedException {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        assertTrue(runner.put(Collections.emptyList()));
    }

    @ParameterizedTest
    @MethodSource("put_SuccessProduced")
    void put_SuccessProduced(String topicSelectorMethod, String topicSelectorConfigProperty, String topicSelectorConfig, String partitionSelectorMethod) throws InterruptedException {
        Map<String, Object> mockedConfigMap = new HashMap<>();
        mockedConfigMap.put("topic.selector." + topicSelectorConfigProperty, topicSelectorConfig);
        mockedConfigMap.put(topicSelectorConfigProperty, topicSelectorConfig);

        // topicSelector and partitionSelector are used for verification
        final TopicSelector topicSelector = TopicSelector.createSelector(topicSelectorMethod);
        topicSelector.configure(mockedConfigMap);
        final PartitionSelector partitionSelector = KafkaSinkRunner.createSelector(partitionSelectorMethod);

        doReturn(topicSelectorMethod).when(kafkaSinkConfig).getTopicSelector();
        doReturn(partitionSelectorMethod).when(kafkaSinkConfig).getPartitionSelector();
        doReturn(mockedConfigMap).when(kafkaSinkConfig).originalsWithPrefix(TOPIC_SELECTOR_CONFIG + ".");

        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        final Future<?> future = Executors.newSingleThreadExecutor().submit(runner);
        Awaitility.await("Waiting for runner to start").atMost(10, TimeUnit.SECONDS).pollInterval(Duration.ofMillis(100)).until(runner::isRunning);

        final KafkaFuture<RecordMetadata> futureValid = KafkaFuture.completedFuture(new RecordMetadata(EXPECTED_TOPIC_PARTITION, EXPECTED_OFFSET, 0, 0L, 0, 0));
        doReturn(futureValid, futureValid, futureValid).when(producer).send(any());

        final SinkRecord record1 = createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> expectedRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(topicSelector.select(record1), partitionSelector.select(record1), BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord record2 = createSinkRecord(TOPIC, PARTITION, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(topicSelector.select(record2), partitionSelector.select(record2), BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));

        final SinkRecord record3 = createSinkRecord(TOPIC, PARTITION, NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord3 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(topicSelector.select(record3), partitionSelector.select(record3), NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));

        final List<SinkRecord> sinkRecords = Arrays.asList(record1, record2, record3);
        final List<ProducerRecord<byte[], byte[]>> expectedProduceRecords = Arrays.asList(expectedRecord1, expectedRecord2, expectedRecord3);

        final Map<TopicPartition, OffsetAndMetadata> expectedOffsets = Collections.singletonMap(EXPECTED_TOPIC_PARTITION, new OffsetAndMetadata(EXPECTED_OFFSET));

        assertTrue(runner.isRunning());
        assertFalse(future.isDone());
        assertTrue(runner.put(sinkRecords));
        assertNotEquals(runner.getThreadID(), -1L);
        Utils.sleep(200);
        assertFalse(future.isDone());
        assertTrue(runner.isRunning());
        verify(producer, times(expectedProduceRecords.size())).send(recordCaptor.capture());

        assertEquals(expectedProduceRecords, recordCaptor.getAllValues());
        runner.stop();
        assertEquals(expectedOffsets, runner.getStoredOffsetsAndClear());
    }

    @ParameterizedTest
    @MethodSource("put_SuccessProducedClass")
    void put_SuccessProducedClass(Class<?> topicSelectorClass, String topicSelectorConfigProperty, String topicSelectorConfig, String partitionSelectorMethod) throws InterruptedException {
        Map<String, Object> mockedConfigMap = new HashMap<>();
        mockedConfigMap.put("topic.selector." + topicSelectorConfigProperty, topicSelectorConfig);
        mockedConfigMap.put(topicSelectorConfigProperty, topicSelectorConfig);

        // topicSelector and partitionSelector are used for verification
        final TopicSelector topicSelector = TopicSelector.createSelector(topicSelectorClass);
        topicSelector.configure(mockedConfigMap);
        final PartitionSelector partitionSelector = KafkaSinkRunner.createSelector(partitionSelectorMethod);

        doReturn(topicSelectorClass).when(kafkaSinkConfig).getTopicSelectorClass();
        doReturn(partitionSelectorMethod).when(kafkaSinkConfig).getPartitionSelector();
        doReturn(mockedConfigMap).when(kafkaSinkConfig).originalsWithPrefix(TOPIC_SELECTOR_CONFIG + ".");

        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        final Future<?> future = Executors.newSingleThreadExecutor().submit(runner);
        Awaitility.await("Waiting for runner to start").atMost(10, TimeUnit.SECONDS).pollInterval(Duration.ofMillis(100)).until(runner::isRunning);

        final KafkaFuture<RecordMetadata> futureValid = KafkaFuture.completedFuture(new RecordMetadata(EXPECTED_TOPIC_PARTITION, EXPECTED_OFFSET, 0, 0L, 0, 0));
        doReturn(futureValid, futureValid, futureValid).when(producer).send(any());

        final SinkRecord record1 = createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> expectedRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(topicSelector.select(record1), partitionSelector.select(record1), BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord record2 = createSinkRecord(TOPIC, PARTITION, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(topicSelector.select(record2), partitionSelector.select(record2), BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));

        final SinkRecord record3 = createSinkRecord(TOPIC, PARTITION, NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord3 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(topicSelector.select(record3), partitionSelector.select(record3), NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));

        final List<SinkRecord> sinkRecords = Arrays.asList(record1, record2, record3);
        final List<ProducerRecord<byte[], byte[]>> expectedProduceRecords = Arrays.asList(expectedRecord1, expectedRecord2, expectedRecord3);

        final Map<TopicPartition, OffsetAndMetadata> expectedOffsets = Collections.singletonMap(EXPECTED_TOPIC_PARTITION, new OffsetAndMetadata(EXPECTED_OFFSET));

        assertTrue(runner.isRunning());
        assertFalse(future.isDone());
        assertTrue(runner.put(sinkRecords));
        assertFalse(runner.isRecordQueueEmpty());
        Utils.sleep(200);
        assertFalse(future.isDone());
        assertTrue(runner.isRunning());
        verify(producer, times(expectedProduceRecords.size())).send(recordCaptor.capture());

        assertTrue(runner.isRecordQueueEmpty());
        assertEquals(expectedProduceRecords, recordCaptor.getAllValues());
        runner.stop();
        assertEquals(expectedOffsets, runner.getStoredOffsetsAndClear());
    }


    @Test
    void Records_Produced_Success_On_Multi_TopicPartition() throws InterruptedException, ExecutionException {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        final Future<?> future = Executors.newSingleThreadExecutor().submit(runner);
        Awaitility.await("Waiting for runner to start").atMost(10, TimeUnit.SECONDS).pollInterval(Duration.ofMillis(100)).until(runner::isRunning);

        int partition1 = 1;
        int partition2 = 2;
        long tp1Record1Offset = 11;
        long tp1Record2Offset = 12;
        long tp1Record3Offset = 13;
        long tp2Record1Offset = 21;
        long tp2Record2Offset = 22;

        final KafkaFuture<RecordMetadata> future1 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition1), tp1Record1Offset, 0, 0L,  0, 0));
        final KafkaFuture<RecordMetadata> future2 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition1), tp1Record2Offset, 0, 0L,  0, 0));
        final KafkaFuture<RecordMetadata> future3 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition1), tp1Record3Offset, 0, 0L,  0, 0));
        final KafkaFuture<RecordMetadata> future4 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition2), tp2Record1Offset, 0, 0L,  0, 0));
        final KafkaFuture<RecordMetadata> future5 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition2), tp2Record2Offset, 0, 0L,  0, 0));

        // Topic partition 1 records
        final SinkRecord tp1Record1 = createSinkRecord(TOPIC, partition1, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> tp1ProducerRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition1, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord tp1Record2 = createSinkRecord(TOPIC, partition1, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> tp1ProducerRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition1, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));

        final SinkRecord tp1Record3 = createSinkRecord(TOPIC, partition1, NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> tp1ProducerRecord3 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition1, NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));

        // Topic partition 2 records
        final SinkRecord tp2Record1 = createSinkRecord(TOPIC, partition2, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> tp2ProducerRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition2, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord tp2Record2 = createSinkRecord(TOPIC, partition2, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> tp2ProducerRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition2, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));


        final List<SinkRecord> sinkRecords = Arrays.asList(tp1Record1, tp1Record2, tp1Record3, tp2Record1, tp2Record2);
        final List<ProducerRecord<byte[], byte[]>> expectedProduceRecords = Arrays.asList(tp1ProducerRecord1, tp1ProducerRecord2, tp1ProducerRecord3, tp2ProducerRecord1, tp2ProducerRecord2);

        doReturn(future1, future2, future3, future4, future5).when(producer).send(any());

        final Map<TopicPartition, OffsetAndMetadata> expectedOffsets = new HashMap<>();
        expectedOffsets.put(new TopicPartition(TOPIC, partition1), new OffsetAndMetadata(EXPECTED_OFFSET));
        expectedOffsets.put(new TopicPartition(TOPIC, partition2), new OffsetAndMetadata(EXPECTED_OFFSET));

        assertTrue(runner.isRunning());
        assertFalse(future.isDone());
        assertTrue(runner.put(sinkRecords));
        Utils.sleep(500);
        assertFalse(future.isDone());
        assertTrue(runner.isRunning());

        // try again to verify that the queue process data
        assertTrue(runner.put(sinkRecords));
        assertFalse(runner.isRecordQueueEmpty());
        Utils.sleep(200);
        assertTrue(runner.isRecordQueueEmpty());
        verify(producer, times(expectedProduceRecords.size() *2 )).send(recordCaptor.capture());

        assertEquals(expectedOffsets, runner.getStoredOffsetsAndClear());
    }

    @Test
    void records_Produced_Fail_On_Multi_TopicPartition() throws InterruptedException, ExecutionException {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        final Future<?> future = Executors.newSingleThreadExecutor().submit(runner);
        Awaitility.await("Waiting for runner to start").atMost(10, TimeUnit.SECONDS).pollInterval(Duration.ofMillis(100)).until(runner::isRunning);

        int partition1 = 1;
        int partition2 = 2;
        long tp1Record1Offset = 11;
        long tp1Record3Offset = 12;
        long tp2Record1Offset = 21;
        long tp2Record2Offset = 22;

        final KafkaFuture<RecordMetadata> future1 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition1), tp1Record1Offset, 0, 0L, 0, 0));
        final KafkaFuture<RecordMetadata> futureError2 = mock(KafkaFuture.class);
        final KafkaFuture<RecordMetadata> future3 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition1), tp1Record3Offset, 0, 0L, 0, 0));
        final KafkaFuture<RecordMetadata> future4 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition2), tp2Record1Offset, 0, 0L, 0, 0));
        final KafkaFuture<RecordMetadata> future5 = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, partition2), tp2Record2Offset, 0, 0L, 0, 0));

        doThrow(new ExecutionException(new KafkaException("Producer failed to send record"))).when(futureError2).get();

        // Topic partition 1 records
        final SinkRecord tp1Record1 = createSinkRecord(TOPIC, partition1, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> tp1ProducerRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition1, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord tp1Record2 = createSinkRecord(TOPIC, partition1, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> tp1ProducerRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition1, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));

        final SinkRecord tp1Record3 = createSinkRecord(TOPIC, partition1, NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> tp1ProducerRecord3 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition1, NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));

        // Topic partition 2 records
        final SinkRecord tp2Record1 = createSinkRecord(TOPIC, partition2, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> tp2ProducerRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition2, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord tp2Record2 = createSinkRecord(TOPIC, partition2, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> tp2ProducerRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(TOPIC, partition2, BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));


        final List<SinkRecord> sinkRecords = Arrays.asList(tp1Record1, tp1Record2, tp1Record3, tp2Record1, tp2Record2);
        final List<ProducerRecord<byte[], byte[]>> expectedProduceRecords = Arrays.asList(tp1ProducerRecord1, tp1ProducerRecord2, tp1ProducerRecord3, tp2ProducerRecord1, tp2ProducerRecord2);

        doReturn(future1, futureError2, future3, future4, future5).when(producer).send(any());

        final Map<TopicPartition, OffsetAndMetadata> expectedOffsets = new HashMap<>();
        expectedOffsets.put(new TopicPartition(TOPIC, partition1), new OffsetAndMetadata(EXPECTED_OFFSET));

        assertTrue(runner.isRunning());
        assertFalse(future.isDone());
        assertTrue(runner.put(sinkRecords));
        Utils.sleep(500);
        assertTrue(future.isDone());
        assertFalse(runner.isRunning());

        // try again to verify that the queue still holds data
        assertFalse(runner.put(sinkRecords));

        assertEquals(expectedOffsets, runner.getStoredOffsetsAndClear());
        verifyStackErrorContains(runner, "Producer failed to send record");
    }

    @Test
    void put_Fail_With_Records_Produced() throws InterruptedException, ExecutionException {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        final Future<?> future = Executors.newSingleThreadExecutor().submit(runner);
        Awaitility.await("Waiting for runner to start").atMost(10, TimeUnit.SECONDS).pollInterval(Duration.ofMillis(100)).until(runner::isRunning);

        final KafkaFuture<RecordMetadata> futureValid = KafkaFuture.completedFuture(new RecordMetadata(EXPECTED_TOPIC_PARTITION, EXPECTED_OFFSET, 0, 0L, 0, 0));
        final KafkaFuture<RecordMetadata> futureInterrupted = mock(KafkaFuture.class);
        doThrow(new InterruptedException("Error details here")).when(futureInterrupted).get();

        doReturn(futureValid, futureInterrupted).when(producer).send(any());

        final SinkRecord record1 = createSinkRecord(BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> expectedRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord record2 = createSinkRecord(BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));

        final SinkRecord record3 = createSinkRecord(NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord3 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(NULL, NULL, Arrays.asList(NULL, BYTEARRAY, BYTEBUFFER));

        final List<SinkRecord> sinkRecords = Arrays.asList(record1, record2, record3);
        final List<ProducerRecord<byte[], byte[]>> expectedProduceRecords = Arrays.asList(expectedRecord1, expectedRecord2, expectedRecord3);

        final Map<TopicPartition, OffsetAndMetadata> expectedOffsets = Collections.singletonMap(EXPECTED_TOPIC_PARTITION, new OffsetAndMetadata(EXPECTED_OFFSET));

        assertTrue(runner.isRunning());
        assertFalse(future.isDone());
        assertTrue(runner.put(sinkRecords));
        Utils.sleep(200);
        assertTrue(future.isDone());
        assertFalse(runner.isRunning());

        // try again to verify that the queue still holds data
        assertFalse(runner.put(sinkRecords));

        verify(producer, times(expectedProduceRecords.size())).send(recordCaptor.capture());

        assertEquals(expectedProduceRecords, recordCaptor.getAllValues());

        assertEquals(expectedOffsets, runner.getStoredOffsetsAndClear());
        verifyStackErrorContains(runner, null);
    }

    @Test
    void put_ProduceFailure() throws ExecutionException, InterruptedException {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        final Future<?> future = Executors.newSingleThreadExecutor().submit(runner);
        Awaitility.await("Waiting for runner to start").atMost(10, TimeUnit.SECONDS).pollInterval(Duration.ofMillis(100)).until(runner::isRunning);

        final KafkaFuture<RecordMetadata> futureExecution = mock(KafkaFuture.class);
        doThrow(new ExecutionException(new RuntimeException("Error details here"))).when(futureExecution).get();

        doReturn(futureExecution).when(producer).send(any());

        final SinkRecord record1 = createSinkRecord(BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> expectedRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord record2 = createSinkRecord(BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));

        final List<SinkRecord> sinkRecords = Arrays.asList(record1, record2);
        final List<ProducerRecord<byte[], byte[]>> expectedProduceRecords = Arrays.asList(expectedRecord1, expectedRecord2);

        assertTrue(runner.isRunning());
        assertFalse(future.isDone());
        assertTrue(runner.put(sinkRecords));
        Utils.sleep(200);
        assertTrue(future.isDone());
        assertFalse(runner.isRunning());

        verifyStackErrorContains(runner, null);

        // try again to verify that the queue still holds data
        assertFalse(runner.put(sinkRecords));

        verify(producer, times(expectedProduceRecords.size())).send(recordCaptor.capture());

        assertEquals(expectedProduceRecords, recordCaptor.getAllValues());

        assertTrue(runner.getStoredOffsetsAndClear().isEmpty());
    }

    @Test
    void put_ProduceInterrupted() throws ExecutionException, InterruptedException {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        final Future<?> future = Executors.newSingleThreadExecutor().submit(runner);
        Awaitility.await("Waiting for runner to start").atMost(10, TimeUnit.SECONDS).pollInterval(Duration.ofMillis(100)).until(runner::isRunning);

        final KafkaFuture<RecordMetadata> futureInterrupted = mock(KafkaFuture.class);
        doThrow(new InterruptedException("Error details here")).when(futureInterrupted).get();

        doReturn(futureInterrupted).when(producer).send(any());

        final SinkRecord record1 = createSinkRecord(BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));
        final ProducerRecord<byte[], byte[]> expectedRecord1 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY));

        final SinkRecord record2 = createSinkRecord(BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));
        final ProducerRecord<byte[], byte[]> expectedRecord2 = (ProducerRecord<byte[], byte[]>) expectedKafkaRecord(BYTEBUFFER, BYTEBUFFER, Arrays.asList(NULL, BYTEBUFFER));

        final List<SinkRecord> sinkRecords = Arrays.asList(record1, record2);
        final List<ProducerRecord<byte[], byte[]>> expectedProduceRecords = Arrays.asList(expectedRecord1, expectedRecord2);

        assertTrue(runner.isRunning());
        assertFalse(future.isDone());
        assertTrue(runner.put(sinkRecords));
        Utils.sleep(200);
        assertTrue(future.isDone());
        assertFalse(runner.isRunning());

        // try again to verify that the queue still holds data
        assertFalse(runner.put(sinkRecords));

        verify(producer, times(expectedProduceRecords.size())).send(recordCaptor.capture());

        assertEquals(expectedProduceRecords, recordCaptor.getAllValues());
        assertTrue(runner.getStoredOffsetsAndClear().isEmpty());
        assertFalse(runner.isRunning());
        verifyStackErrorContains(runner, null);
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("convertConnectRecordSource")
    void convertConnectRecord(final String name, final SinkRecord toConvert, final Consumer<ProducerRecord<byte[], byte[]>> validator, Class<? extends Throwable> expectedException) {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        if (expectedException == null) {
            validator.accept(runner.convertConnectRecord(toConvert));
        } else {
            assertThrows(expectedException, () -> runner.convertConnectRecord(toConvert));
        }
    }

    @Test
    void headerPrefixIsConfigurable() {
        // given a configuration that has a header prefix configured and no static headers
        doReturn("prefixed-").when(kafkaSinkConfig).getHeaderPrefix();
        doReturn(new HashMap<>()).when(kafkaSinkConfig).getStaticHeaders();
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});

        // and a SinkRecord that contains a header
        Header header1 = mock(Header.class);
        when(header1.key()).thenReturn("header1");
        when(header1.value()).thenReturn("value1".getBytes(StandardCharsets.UTF_8));
        SinkRecord sinkRecord = sinkRecord().topic("topic").timestamp(0L).header(header1).build();

        // when the record is converted
        final ProducerRecord<byte[], byte[]> producerRecord = runner.convertConnectRecord(sinkRecord);

        // the output contains the input header, prefixed with the prefix.
        final org.apache.kafka.common.header.Header foundHeader = producerRecord.headers().lastHeader("prefixed-header1");
        assertNotNull(foundHeader);
        assertEquals("value1", new String(foundHeader.value()));
    }

    @Test
    void secondThreadForSingleRunnerStops() {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        Thread t1 = new Thread(runner);
        t1.start();
        Utils.sleep(100);
        assertTrue(runner.isRunning());
        Thread t2 = new Thread(runner);
        t2.start();
        Utils.sleep(100);
        assertFalse(t2.isAlive());
        runner.stop();
    }

    @Test
    void processRecords_should_notThrow_when_nonFailedProduceQueueProducesFailWithRetriableException() throws Exception {
        boolean isFailedProduceQueue = false;
        when(kafkaSinkConfig.getMaxProduceRetries()).thenReturn(1);
        KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});

        // Setup the produce retriable exception
        final KafkaFuture<RecordMetadata> futureFailure = mock(KafkaFuture.class);
        doThrow(new ExecutionException(new NotLeaderOrFollowerException("Producer failed to send record, can retry"))).when(futureFailure).get();
        doReturn(futureFailure).when(producer).send(any());

        List<SinkRecord> records = new ArrayList<>();
        records.add(createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY)));

        Map<TopicPartition, List<SinkRecord>> toProduce = new HashMap<>();
        toProduce.put(new TopicPartition(TOPIC, PARTITION), records);

        assertDoesNotThrow(() -> runner.processRecords(toProduce, isFailedProduceQueue));

        assertThat(runner.getStoredOffsetsAndClear())
                .isEmpty();
    }

    @Test
    void processRecords_should_throwException_when_nonFailedProduceQueueProducesFailWithRetriableException_and_noRetries() throws Exception {
        boolean isFailedProduceQueue = false;
        when(kafkaSinkConfig.getMaxProduceRetries()).thenReturn(0);
        KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});

        // Setup the produce retriable exception
        final KafkaFuture<RecordMetadata> futureFailure = mock(KafkaFuture.class);
        doThrow(new ExecutionException(new NotLeaderOrFollowerException("Producer failed to send record, can retry"))).when(futureFailure).get();
        doReturn(futureFailure).when(producer).send(any());

        List<SinkRecord> records = new ArrayList<>();
        records.add(createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY)));

        Map<TopicPartition, List<SinkRecord>> toProduce = new HashMap<>();
        toProduce.put(new TopicPartition(TOPIC, PARTITION), records);

        assertThrows(SinkRunnerProduceFailedException.class, () -> runner.processRecords(toProduce, isFailedProduceQueue));

        assertThat(runner.getStoredOffsetsAndClear())
                .isEmpty();
    }

    @Test
    void processRecords_should_stopTheTask_when_nonRetriableExceptionThrown() throws Exception {
        boolean isFailedProduceQueue = true;
        when(kafkaSinkConfig.getMaxProduceRetries()).thenReturn(3);
        KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});

        // Setup the produce failure
        final KafkaFuture<RecordMetadata> futureFailure = mock(KafkaFuture.class);
        doThrow(new ExecutionException(new KafkaException("Producer failed to send record"))).when(futureFailure).get();
        doReturn(futureFailure).when(producer).send(any());

        List<SinkRecord> records = new ArrayList<>();
        records.add(createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY)));

        Map<TopicPartition, List<SinkRecord>> toProduce = new HashMap<>();
        toProduce.put(new TopicPartition(TOPIC, 0), records);

        assertThrows(SinkRunnerProduceFailedException.class, () -> runner.processRecords(toProduce, isFailedProduceQueue));
    }

    @Test
    void processRecords_should_stopTheTask_when_failedProduceQueueRetriedProducesExceedRetries() throws Exception {
        boolean isFailedProduceQueue = true;
        when(kafkaSinkConfig.getMaxProduceRetries()).thenReturn(2);
        KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});

        // Setup the produce failure
        final KafkaFuture<RecordMetadata> futureFailure = mock(KafkaFuture.class);
        doThrow(new ExecutionException(new NotLeaderOrFollowerException("Producer failed to send record"))).when(futureFailure).get();
        doReturn(futureFailure, futureFailure, futureFailure, futureFailure).when(producer).send(any());

        List<SinkRecord> records = new ArrayList<>();
        records.add(createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY)));

        Map<TopicPartition, List<SinkRecord>> toProduce = new HashMap<>();
        toProduce.put(new TopicPartition(TOPIC, 0), records);

        assertDoesNotThrow(() -> runner.processRecords(toProduce, isFailedProduceQueue));
        assertDoesNotThrow(() -> runner.processRecords(toProduce, isFailedProduceQueue));
        assertThrows(SinkRunnerProduceFailedException.class, () -> runner.processRecords(toProduce, isFailedProduceQueue));
    }

    @Test
    void processRecords_should_notStopTheTask_when_previouslyFailedProduceQueueProduceRetrySucceeds() throws Exception {
        boolean isFailedProduceQueue = false;
        when(kafkaSinkConfig.getMaxProduceRetries()).thenReturn(1);
        KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});

        // 1/ Setup the produce retriable exception
        final KafkaFuture<RecordMetadata> futureFailure = mock(KafkaFuture.class);
        doThrow(new ExecutionException(new NotLeaderOrFollowerException("Producer failed to send record, can retry"))).when(futureFailure).get();
        doReturn(futureFailure).when(producer).send(any());

        List<SinkRecord> records = new ArrayList<>();
        records.add(createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY)));
        Map<TopicPartition, List<SinkRecord>> toProduce = new HashMap<>();
        toProduce.put(new TopicPartition(TOPIC, PARTITION), records);

        runner.processRecords(toProduce, isFailedProduceQueue);

        // 2/ Setup a produce success which should recover and continue
        isFailedProduceQueue = true;

        final KafkaFuture<RecordMetadata> futureSuccess = KafkaFuture.completedFuture(new RecordMetadata(new TopicPartition(TOPIC, PARTITION), OFFSET, 0, 0L, 0, 0));

        doReturn(futureSuccess).when(producer).send(any());

        records.clear();
        records.add(createSinkRecord(TOPIC, PARTITION, BYTEARRAY, BYTEARRAY, Arrays.asList(NULL, BYTEARRAY)));
        toProduce.clear();
        toProduce.put(new TopicPartition(TOPIC, PARTITION), records);

        runner.processRecords(toProduce, isFailedProduceQueue);

        final Map<TopicPartition, OffsetAndMetadata> expectedOffsets = new HashMap<>();
        expectedOffsets.put(new TopicPartition(TOPIC, PARTITION), new OffsetAndMetadata(EXPECTED_OFFSET));
        assertEquals(expectedOffsets, runner.getStoredOffsetsAndClear());
    }

    @Test
    void stop_should_closeProducer() {
        final KafkaSinkRunner runner = new KafkaSinkRunner(kafkaSinkConfig, noop -> {});
        doNothing().when(this.producer).close();

        runner.stop();

        verify(this.producer).flush();
        verify(this.producer).close();
    }

    @Test
    @DisplayName("Verify creation of SourcePartitionSelector")
    void createSelector_Source() {
        PartitionSelector selector = KafkaSinkRunner.createSelector(PARTITION_SELECTOR_SOURCE);
        assertEquals(SourcePartitionSelector.class, selector.getClass());
    }

    @Test
    @DisplayName("Verify creation of NullPartitionSelector")
    void createSelector_Partitioner() {
        PartitionSelector selector = KafkaSinkRunner.createSelector(PARTITION_SELECTOR_PARTITIONER);
        assertEquals(NullPartitionSelector.class, selector.getClass());
    }

    @Test
    @DisplayName("Verify rejection of null as selector name")
    void createSelector_Null() {
        assertThrows(IllegalArgumentException.class, () -> KafkaSinkRunner.createSelector(null));
    }

    @Test
    @DisplayName("Verify rejection of unknown selector name")
    void createSelector_Unknown() {
        assertThrows(IllegalArgumentException.class, () -> KafkaSinkRunner.createSelector("UNKNOWN"));
    }


    // the following will use Lombok to create a fluid way of building SinkRecord instances.
    @Builder(builderMethodName = "sinkRecord")
    SinkRecord sinkRecordBuilder(String topic, int partition, Schema keySchema, Object key, Schema valueSchema, Object value, long kafkaOffset,
                                 Long timestamp, TimestampType timestampType, Header header) {
        return new SinkRecord(topic, partition, keySchema, key, valueSchema, value, kafkaOffset, timestamp, timestampType, Collections.singletonList(header));
    }

    enum PayloadType {
        BYTEARRAY, BYTEBUFFER, NULL, EXCEPTION
    }

    private void verifyStackErrorContains(KafkaSinkRunner runner, String errorMessage) {
        if (errorMessage == null) {
            errorMessage = "Error details here";
        }
        MatcherAssert.assertThat(runner.getFailureDescription(), CoreMatchers.containsString(errorMessage));
    }
}
