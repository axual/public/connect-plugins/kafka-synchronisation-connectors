package io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.ContainerNetwork;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.ProvideClientProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.kafka.clients.CommonClientConfigs;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.images.builder.Transferable;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Testcontainers implementation for Apache Kafka.
 * <p>
 * Supported image: {@code apache/kafka}, {@code apache/kafka-native}
 * <p>
 * Exposed ports: 9092
 */
@Slf4j
public class KafkaContainer extends GenericContainer<KafkaContainer> implements ProvideClientProperties {

    private static final DockerImageName APACHE_KAFKA_NATIVE_IMAGE_NAME = DockerImageName.parse("apache/kafka-native:3.8.1");

    private static final String DEFAULT_EXPOSED_LISTENER_NAME = "DEFAULT_LISTENER";

    private static final String DEFAULT_INTERNAL_TOPIC_RF = "1";

    private static final String STARTER_SCRIPT = "/tmp/testcontainers_start.sh";
    private static final String LOG_LOCATION = "/etc/kafka/docker/log4j.properties";

    private static final String DEFAULT_CLUSTER_ID = "4L6g3nShT-eMCtK--X86sw";

    final AtomicInteger portProvider = new AtomicInteger(9092);
    private final Map<String, ListenerConfig> customListeners = new LinkedHashMap<>();

    public KafkaContainer() {
        this(APACHE_KAFKA_NATIVE_IMAGE_NAME);
    }

    public KafkaContainer(String imageName) {
        this(DockerImageName.parse(imageName));
    }

    public KafkaContainer(DockerImageName dockerImageName) {
        super(dockerImageName);
        dockerImageName.assertCompatibleWith(APACHE_KAFKA_NATIVE_IMAGE_NAME);
        this
                .withEnv("CLUSTER_ID", DEFAULT_CLUSTER_ID)
                .withEnv("KAFKA_PROCESS_ROLES", "broker,controller")
                .withEnv("KAFKA_NODE_ID", "1")
                .withEnv("KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR", DEFAULT_INTERNAL_TOPIC_RF)
                .withEnv("KAFKA_OFFSETS_TOPIC_NUM_PARTITIONS", DEFAULT_INTERNAL_TOPIC_RF)
                .withEnv("KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR", DEFAULT_INTERNAL_TOPIC_RF)
                .withEnv("KAFKA_TRANSACTION_STATE_LOG_MIN_ISR", DEFAULT_INTERNAL_TOPIC_RF)
                .withEnv("KAFKA_LOG_FLUSH_INTERVAL_MESSAGES", Long.MAX_VALUE + "")
                .withEnv("KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS", "0")
                .withCommand("sh", "-c", "while [ ! -f " + STARTER_SCRIPT + " ]; do sleep 0.1; done; " + STARTER_SCRIPT)
                .waitingFor(Wait.forLogMessage(".*Transitioning from RECOVERY to RUNNING.*", 1))
                .withStartupTimeout(Duration.ofSeconds(15))
                .withStartupAttempts(3)
                .withCreateContainerCmdModifier(cmd -> cmd.getHostConfig().withMemory(512L * 1024 * 1024).withCpuPeriod(100_000L).withCpuQuota(25_000L));
    }

    @Override
    protected void configure() {
        // Setup environment for listeners
        if (customListeners.isEmpty()) {
            this.withExposedListener(DEFAULT_EXPOSED_LISTENER_NAME);
        }
        var listeners = new ArrayList<String>();
        var protocolMap = new ArrayList<String>();

        listeners.add("CONTROLLER://0.0.0.0:9090");
        protocolMap.add("CONTROLLER:PLAINTEXT");

        listeners.add("BROKER://0.0.0.0:9091");
        protocolMap.add("BROKER:PLAINTEXT");
        for (var listener : customListeners.values()) {
            listener.onConfigure();
            var port = listener.getPort();
            String listenerName = listener.listenerConfigName();
            listeners.add("%s://0.0.0.0:%d".formatted(listenerName, port));
            protocolMap.add("%s:PLAINTEXT".formatted(listenerName));
        }
        var controllerQuorumVoters = String.format("%s@localhost:9090", getEnvMap().get("KAFKA_NODE_ID"));

        this
                .withEnv("KAFKA_LISTENERS", String.join(",", listeners))
                .withEnv("KAFKA_LISTENER_SECURITY_PROTOCOL_MAP", String.join(",", protocolMap))
                .withEnv("KAFKA_INTER_BROKER_LISTENER_NAME", "BROKER")
                .withEnv("KAFKA_CONTROLLER_LISTENER_NAMES", "CONTROLLER")
                .withEnv("KAFKA_CONTROLLER_QUORUM_VOTERS", controllerQuorumVoters)
                .withAutoCreateTopics(false);
    }

    @Override
    protected void containerIsStarting(InspectContainerResponse containerInfo) {
        var brokerAdvertisedListener = String.format("BROKER://%s:%s", containerInfo.getConfig().getHostName(), "9091");
        var advertisedListeners = new ArrayList<String>();
        advertisedListeners.add(brokerAdvertisedListener);

        for (var listener : customListeners.values()) {
            var advertisedListener = "%s://%s".formatted(listener.listenerConfigName(), listener.bootstrapServers());
            advertisedListeners.add(advertisedListener);
            log.info("Listener {} internal port {} advertised as {}", listener.name, listener.port, advertisedListener);
        }

        var kafkaAdvertisedListeners = String.join(",", advertisedListeners);
        var command = "#!/bin/bash\n";
        // exporting KAFKA_ADVERTISED_LISTENERS with the container hostname
        command += String.format("export KAFKA_ADVERTISED_LISTENERS=%s%n", kafkaAdvertisedListeners);
        command += "env | sort | grep KAFKA_\n";

        command += "/etc/kafka/docker/run \n";
        copyFileToContainer(Transferable.of(command, 0777), STARTER_SCRIPT);
        try {
            var logResource = IOUtils.resourceToString("testcontainers/kafka/log4j.properties", StandardCharsets.UTF_8, this.getClass().getClassLoader());
            copyFileToContainer(Transferable.of(logResource, 0666), LOG_LOCATION);
            log.info("Copied log4jProperties");
        } catch (IOException e) {
            log.info("Could not read or copy log4j properties to container");
            throw new RuntimeException(e);
        }
    }

    @Override
    public String bootstrapServers() {
        return bootstrapServers(null);
    }

    @Override
    public String bootstrapServers(final String listenerName) {
        if (customListeners.isEmpty()) {
            return null;
        }
        final var realListenerName = Optional.ofNullable(listenerName).orElseGet(() -> customListeners.keySet().iterator().next());
        var config = Optional.ofNullable(customListeners.get(realListenerName)).orElseThrow(() -> new IllegalArgumentException("Unknown listener: " + realListenerName));
        return config.bootstrapServers();
    }

    @Override
    public Map<String, Object> clientConnectionProperties() {
        var connectionProperties = new HashMap<String, Object>();
        connectionProperties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers());
        connectionProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "PLAINTEXT");
        return connectionProperties;
    }

    @Override
    public Map<String, Object> clientConnectionProperties(final String listenerName) {
        var connectionProperties = new HashMap<String, Object>();
        connectionProperties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers(listenerName));
        connectionProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "PLAINTEXT");
        return connectionProperties;
    }

    public KafkaContainer withAutoCreateTopics(boolean autoCreateTopics) {
        return this.withEnv("KAFKA_AUTO_CREATE_TOPICS_ENABLE", String.valueOf(autoCreateTopics));
    }

    public KafkaContainer withAdvertisedHost(String listenerName, String advertisedHost) {
        final var config = getExposedListener(listenerName);
        config.setAdvertisedHost(advertisedHost);
        return this;
    }

    public KafkaContainer withAdvertisedPort(String listenerName, Integer advertisedPort) {
        final var config = getExposedListener(listenerName);
        config.setAdvertisedPort(advertisedPort);
        return this;
    }

    public KafkaContainer withAdvertised(String listenerName, String advertisedHost, Integer advertisedPort) {
        final var config = getExposedListener(listenerName);
        config.setAdvertisedHost(advertisedHost);
        config.setAdvertisedPort(advertisedPort);
        log.info("Setting advertised '{}' with advertisedHost '{}' and advertisedPort '{}' Result = {}", listenerName, advertisedHost, advertisedPort, config);
        return this;
    }

    private ListenerConfig getExposedListener(String listenerName) {
        return Optional.ofNullable(customListeners.get(listenerName)).orElseThrow(() -> new IllegalArgumentException("Unknown exposed listener: " + listenerName));
    }

    public KafkaContainer withExposedListener(String listenerName) {
        customListeners.computeIfAbsent(listenerName, n -> new ListenerConfig(n, portProvider.getAndIncrement()));
        return this;
    }

    public KafkaContainer withNetworkListener(String listenerName, String networkAlias) {
        customListeners.computeIfAbsent(listenerName, n -> new NetworkListenerConfig(n, networkAlias, portProvider.getAndIncrement()));
        return this;
    }


    public Integer getMappedListenerPort(String listenerName) {
        return getMappedPort(getListenerPort(listenerName));
    }

    public Integer getListenerPort(String listenerName) {
        var config = Optional.ofNullable(customListeners.get(listenerName)).orElseThrow(() -> new IllegalArgumentException("Unknown listener: " + listenerName));
        return config.getPort();
    }

    public String getCurrentIpAddress() {
        return getCurrentContainerInfo().getNetworkSettings().getNetworks().values().stream().findFirst().map(ContainerNetwork::getIpAddress).orElse(null);
    }


    @Getter
    @Setter
    @EqualsAndHashCode
    private class ListenerConfig {
        protected static final String BOOTSTRAP_SERVER_FORMAT = "%s:%d";
        final String name;
        final int port;
        Integer advertisedPort;
        String advertisedHost;

        public ListenerConfig(final String name, final int port) {
            this.name = name;
            this.port = port;
        }

        public String bootstrapServers() {
            var bootstrapHost = Optional.ofNullable(advertisedHost).orElseGet(KafkaContainer.this::getHost);
            var bootstrapPort = Optional.ofNullable(advertisedPort).orElseGet(() -> getMappedPort(port));
            return String.format(BOOTSTRAP_SERVER_FORMAT, bootstrapHost, bootstrapPort);
        }

        public String listenerConfigName() {
            return "EXPOSED" + port;
        }

        public void onConfigure() {
            addExposedPort(port);
        }
    }

    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = true)
    private class NetworkListenerConfig extends ListenerConfig {
        private final String networkAlias;

        public NetworkListenerConfig(final String name, final String networkAlias, final int port) {
            super(name, port);
            this.networkAlias = networkAlias;
        }

        @Override
        public String bootstrapServers() {
            var bootstrapHost = Optional.ofNullable(advertisedHost).orElseGet(this::getCurrentIpAddress);
            var bootstrapPort = Optional.ofNullable(advertisedPort).orElse(port);
            return String.format(BOOTSTRAP_SERVER_FORMAT, bootstrapHost, bootstrapPort);
        }

        @Override
        public String listenerConfigName() {
            return "NETWORKED" + port;
        }

        private String getCurrentIpAddress() {
            return Optional.ofNullable(KafkaContainer.this.getCurrentIpAddress()).orElse(networkAlias);
        }


        @Override
        public void onConfigure() {
            // do nothing
        }
    }
}
