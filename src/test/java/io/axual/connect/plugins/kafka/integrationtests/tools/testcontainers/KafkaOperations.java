package io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.integrationtests.tools.providers.KafkaCluster;
import io.axual.connect.plugins.kafka.integrationtests.tools.providers.ProvideClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.errors.TopicExistsException;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.utils.Utils;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG;

@Slf4j
public class KafkaOperations implements KafkaCluster {
    private static final short REPLICATION_FACTOR = 1;
    protected final String logPrefix;
    protected final ProvideClientProperties kafka;

    public KafkaOperations(ProvideClientProperties kafka, final String logPrefix) {

        this.kafka = kafka;
        this.logPrefix = logPrefix;
    }

    @Override
    public void createTopic(final String name, final int partitions, final boolean compacted) {
        log.info("{}Creating topic {} with {} partition(s)", logPrefix, name, partitions);
        if (name == null) {
            throw new IllegalArgumentException("Topic name cannot be null");
        }

        try (var adminClient = AdminClient.create(adminProperties())) {
            var newTopic = new NewTopic(name, partitions, REPLICATION_FACTOR);
            newTopic.configs(Collections.singletonMap(TopicConfig.CLEANUP_POLICY_CONFIG, compacted ? TopicConfig.CLEANUP_POLICY_COMPACT : TopicConfig.CLEANUP_POLICY_DELETE));

            var createResult = adminClient.createTopics(Collections.singleton(newTopic));
            try {
                createResult.all().get();
                log.info("{}Created topic {} with {} partitions", logPrefix, name, partitions);
            } catch (InterruptedException e) {
                throw new RuntimeException("Create topic for topic " + name + " was interrupted", e);
            } catch (ExecutionException e) {
                var cause = e.getCause();
                if (cause instanceof TopicExistsException) {
                    log.info("{}Topic {} already exists", logPrefix, name);
                    return;
                }
                if (cause instanceof RuntimeException rte) {
                    throw rte;
                }
                throw new RuntimeException("Create topic for topic " + name + " has failed", cause == null ? e : cause);
            }
        }
    }

    @Override
    public void deleteTopic(final String name) {
        log.info("{}Deleting topic {}", logPrefix, name);
        if (name == null) {
            throw new IllegalArgumentException("Topic name cannot be null");
        }

        try (var adminClient = AdminClient.create(adminProperties())) {
            var deleteResult = adminClient.deleteTopics(Collections.singleton(name));
            deleteResult.topicNameValues().get(name).get();
            log.info("{}Deleted topic {}", logPrefix, name);
        } catch (InterruptedException e) {
            throw new RuntimeException("Delete topic " + name + " was interrupted", e);
        } catch (ExecutionException e) {
            throw new RuntimeException("Delete topic " + name + " has failed", e.getCause() == null ? e : e.getCause());
        }
    }

    @Override
    public ConsumerRecords<byte[], byte[]> consume(String listenerName, final int expectedRecordCount, final long maxDuration, final String groupId, final Collection<String> topics) {
        var recordsPerPartition = new HashMap<TopicPartition, List<ConsumerRecord<byte[], byte[]>>>();
        int consumedRecords = 0;
        var consumerProperties = consumerProperties(listenerName, groupId);
        consumerProperties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "500");
        consumerProperties.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "50000");
        var allPartitionsHaveRecords = false;

        try (KafkaConsumer<byte[], byte[]> consumer = new KafkaConsumer<>(consumerProperties)) {
            final var assignedTopicPartitions = setupConsumer(groupId, topics, consumer);
            consumer.seekToBeginning(consumer.assignment());
            final var startMillis = System.currentTimeMillis();
            final var endTimeMillis = startMillis + maxDuration;
            long timeLeft;
            do {
                timeLeft = Long.max(endTimeMillis - System.currentTimeMillis(), 0L);
                if (timeLeft <= 0) {
                    log.info("{}Timed out while waiting to consume {} records within the time bound of {}ms", logPrefix, expectedRecordCount, maxDuration);
                    break;
                }

                log.info("{}Consuming from {} for {} millis, total records consumed: {}, time left: {}",
                        logPrefix, topics, maxDuration, consumedRecords, timeLeft);
                var polledRecords = consumer.poll(Duration.ofSeconds(1));
                if (polledRecords.isEmpty()) {
                    Utils.sleep(200);
                } else {
                    for (var partition : polledRecords.partitions()) {
                        var r = polledRecords.records(partition);
                        recordsPerPartition.computeIfAbsent(partition, t -> new ArrayList<>()).addAll(r);
                        consumedRecords += r.size();
                    }

                    allPartitionsHaveRecords = allPartitionsHaveRecords || recordsPerPartition.size() == assignedTopicPartitions.size();
                }
            } while (expectedRecordCount > consumedRecords || !allPartitionsHaveRecords);
        }
        log.info("{}Consumed a total of {} recordsPerPartition from {} partitions", logPrefix, consumedRecords, recordsPerPartition.size());
        return new ConsumerRecords<>(recordsPerPartition);
    }


    @Override
    public long produce(String listenerName, final ProducerRecord<byte[], byte[]> producerRecord) {
        try (var producer = new KafkaProducer<>(producerProperties(listenerName), new ByteArraySerializer(), new ByteArraySerializer())) {
            return producer.send(producerRecord).get(10_000, TimeUnit.MILLISECONDS).offset();
        } catch (Exception e) {
            throw new KafkaException("Could not produce message: " + producerRecord, e);
        }
    }


    @Override
    public String bootstrapServers() {
        return kafka.bootstrapServers();
    }

    @Override
    public String bootstrapServers(final String listenerName) {
        return kafka.bootstrapServers(listenerName);
    }

    @Override
    public Map<String, Object> clientConnectionProperties() {
        return kafka.clientConnectionProperties();
    }

    @Override
    public Map<String, Object> clientConnectionProperties(final String listenerName) {
        return kafka.clientConnectionProperties(listenerName);
    }

    @Override
    public Map<String, Object> producerProperties(final String listenerName, final String transactionalId) {
        var producerProperties = clientConnectionProperties(listenerName);
        producerProperties.put(ProducerConfig.ACKS_CONFIG, "all");
        producerProperties.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, 1);
        producerProperties.put(ProducerConfig.RETRIES_CONFIG, 3);
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());

        if (transactionalId != null) {
            producerProperties.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, transactionalId);
        }

        return producerProperties;
    }

    @Override
    public Map<String, Object> consumerProperties(final String listenerName, final String groupId) {
        var consumerProperties = clientConnectionProperties(listenerName);
        consumerProperties.put(ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerProperties.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerProperties.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, false);
        consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
        consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());

        if (groupId != null) {
            consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        }

        return consumerProperties;
    }


    private Set<TopicPartition> setupConsumer(String groupId, Collection<String> topics, KafkaConsumer<byte[], byte[]> consumer) {
        if (groupId == null) {
            consumer.assign(topics.stream()
                    .map(consumer::partitionsFor)
                    .flatMap(Collection::stream)
                    .map(ti -> new TopicPartition(ti.topic(), ti.partition()))
                    .toList()
            );
        } else {
            consumer.subscribe(topics);
        }
        var assignment = consumer.assignment();
        log.info("{}Verifier consumer assigned to {} partitions: {}", logPrefix, assignment.size(), assignment);
        return assignment;
    }
}
