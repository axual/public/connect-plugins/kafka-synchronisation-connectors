package io.axual.connect.plugins.kafka.integrationtests.tools.testcontainers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.github.dockerjava.api.model.ContainerNetwork;
import lombok.extern.slf4j.Slf4j;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;

@Slf4j
public class ApicurioRegistryContainer extends GenericContainer<ApicurioRegistryContainer> {
    private static final DockerImageName APICURIO_IMAGE_NAME = DockerImageName.parse("apicurio/apicurio-registry-mem:2.6.2.Final");

    public ApicurioRegistryContainer() {
        this(APICURIO_IMAGE_NAME);
    }

    public ApicurioRegistryContainer(String imageName) {
        this(DockerImageName.parse(imageName));
    }

    public ApicurioRegistryContainer(DockerImageName dockerImageName) {
        super(dockerImageName);
        dockerImageName.assertCompatibleWith(APICURIO_IMAGE_NAME);
        this
                .withEnv("REGISTRY_APIS_V2_DATE_FORMAT", "yyyy-MM-dd'T'HH:mm:ss'Z'")
                .withStartupTimeout(Duration.ofSeconds(15))
                .withStartupAttempts(3)
                .waitingFor(Wait.forLogMessage(".*apicurio-registry-app.* started in.* Listening on: http://0.0.0.0:8080.*", 1))
                .withExposedPorts(getServicePort())
                .withCreateContainerCmdModifier(cmd -> cmd.getHostConfig().withMemory(512L * 1024 * 1024).withCpuPeriod(100_000L).withCpuQuota(25_000L))
        ;
    }

    public Integer getMappedServicePort() {
        return getMappedPort(getServicePort());
    }

    public Integer getServicePort() {
        return 8080;
    }


    public String getApicurioUrl() {
        return "%s/apis/registry/v2".formatted(getHttpUrlBase());
    }

    public String getMappedApicurioUrl() {
        return "%s/apis/registry/v2".formatted(getMappedHttpUrlBase());
    }

    public String getMappedCCompatUrl() {
        return "%s/apis/ccompat/v6".formatted(getMappedHttpUrlBase());
    }

    public String getCCompatUrl() {
        return "%s/apis/ccompat/v6".formatted(getHttpUrlBase());
    }

    private String getHttpUrlBase() {
        return getUrlBase(getCurrentIpAddress(), getServicePort());
    }

    private String getMappedHttpUrlBase() {
        return getUrlBase(getHost(), getMappedServicePort());
    }

    private String getUrlBase(String address, int port) {
        return "http://%s:%d".formatted(address, port);
    }

    public String getCurrentIpAddress() {
        return getCurrentContainerInfo().getNetworkSettings().getNetworks().values().stream().findFirst().map(ContainerNetwork::getIpAddress).orElse(null);
    }
}
