package io.axual.connect.plugins.kafka.integrationtests.tools.providers;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

public interface ProvideConsuming {
    /**
     * Consume at least n records in a given duration. The duration starts after consumer subscription is completed
     *
     * @param maxRecords  the number of expected records in this topic.
     * @param maxDuration the max duration to wait for these records (in milliseconds).
     * @param topic       the topic to subscribe and consume records from.
     * @return a {@link ConsumerRecords} collection containing at least maxRecords records.
     */
    default ConsumerRecords<byte[], byte[]> consume(int maxRecords, long maxDuration, String topic) {
        return consume(null, maxRecords, maxDuration, null, Collections.singleton(topic));
    }

    /**
     * Consume at least n records in a given duration. The duration starts after consumer subscription is completed
     *
     * @param listenerName The Kafka listener to connect to for consuming. Null means using the first external listener
     * @param maxRecords  the number of expected records in this topic.
     * @param maxDuration the max duration to wait for these records (in milliseconds).
     * @param topic       the topic to subscribe and consume records from.
     * @return a {@link ConsumerRecords} collection containing at least maxRecords records.
     */
    default ConsumerRecords<byte[], byte[]> consume(String listenerName, int maxRecords, long maxDuration, String topic) {
        return consume(listenerName, maxRecords, maxDuration, null, Collections.singleton(topic));
    }

    /**
     * Consume at least n records in a given duration. The duration starts after consumer subscription is completed
     *
     * @param maxRecords  the number of expected records in this topic.
     * @param maxDuration the max duration to wait for these records (in milliseconds).
     * @param groupId     the group id to use in this consumer. Null means don't set a group id
     * @param topic       the topic to subscribe and consume records from.
     * @return a {@link ConsumerRecords} collection containing at least maxRecords records.
     */
    default ConsumerRecords<byte[], byte[]> consume(int maxRecords, long maxDuration, String groupId, String topic) {
        return consume(null, maxRecords, maxDuration, groupId, Collections.singleton(topic));
    }

    /**
     * Consume at least n records in a given duration. The duration starts after consumer subscription is completed
     *
     * @param listenerName The Kafka listener to connect to for consuming. Null means using the first external listener
     * @param maxRecords  the number of expected records in this topic.
     * @param maxDuration the max duration to wait for these records (in milliseconds).
     * @param groupId     the group id to use in this consumer. Null means don't set a group id
     * @param topic       the topic to subscribe and consume records from.
     * @return a {@link ConsumerRecords} collection containing at least maxRecords records.
     */
    default ConsumerRecords<byte[], byte[]> consume(String listenerName, int maxRecords, long maxDuration, String groupId, String topic) {
        return consume(listenerName, maxRecords, maxDuration, groupId, Collections.singleton(topic));
    }

    /**
     * Consume at least n records in a given duration. The duration starts after consumer subscription is completed
     *
     * @param maxRecords  the number of expected records in this topic.
     * @param maxDuration the max duration to wait for these records (in milliseconds).
     * @param topics      the topics to subscribe and consume records from.
     * @return a {@link ConsumerRecords} collection containing at least maxRecords records.
     */
    default ConsumerRecords<byte[], byte[]> consume(int maxRecords, long maxDuration, Collection<String> topics) {
        return consume(null, maxRecords, maxDuration, null, topics);
    }

    /**
     * Consume at least n records in a given duration. The duration starts after consumer subscription is completed
     *
     * @param listenerName The Kafka listener to connect to for consuming. Null means using the first external listener
     * @param maxRecords  the number of expected records in this topic.
     * @param maxDuration the max duration to wait for these records (in milliseconds).
     * @param topics      the topics to subscribe and consume records from.
     * @return a {@link ConsumerRecords} collection containing at least maxRecords records.
     */
    default ConsumerRecords<byte[], byte[]> consume(String listenerName, int maxRecords, long maxDuration, Collection<String> topics) {
        return consume(listenerName, maxRecords, maxDuration, null, topics);
    }

    /**
     * Consume at least n records in a given duration. The duration starts after consumer subscription is completed
     *
     * @param listenerName The Kafka listener to connect to for consuming. Null means using the first external listener
     * @param maxRecords  the number of expected records in this topic.
     * @param maxDuration the max duration to wait for these records (in milliseconds).
     * @param groupId     The optional groupId to use. Null means don't set a group id
     * @param topics      the topics to subscribe and consume records from.
     * @return a {@link ConsumerRecords} collection containing at least maxRecords records.
     */
    ConsumerRecords<byte[], byte[]> consume(String listenerName, int maxRecords, long maxDuration, String groupId, Collection<String> topics);

    /**
     * Generate a unique group id to use in consumers.
     *
     * @return the new unique group id
     */
    default String generateGroupId() {
        return UUID.randomUUID().toString();
    }
}
