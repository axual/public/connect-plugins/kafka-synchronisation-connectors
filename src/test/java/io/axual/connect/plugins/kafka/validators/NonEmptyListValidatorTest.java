package io.axual.connect.plugins.kafka.validators;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class NonEmptyListValidatorTest {

    private NonEmptyListValidator nonEmptyListValidator;

    @BeforeEach
    void setup() {
        nonEmptyListValidator = new NonEmptyListValidator();
    }

    @Test
    void shouldOnlyAcceptList() {
        try {
            nonEmptyListValidator.ensureValid("testkey", "xyzzy");
            fail("Validator should not accept String");
        } catch (ConfigException e) {
            assertTrue(e.getMessage().endsWith("A config value of type List is expected"));
        }
    }

    @Test
    void shouldNotAcceptEmptyList() {
        try {
            nonEmptyListValidator.ensureValid("testkey", new ArrayList<String>());
            fail("Validator should not accept empty list");
        } catch (ConfigException e) {
            assertTrue(e.getMessage().endsWith("A list with at least one entry is required"));
        }
    }

    @Test
    void acceptsNonEmptyList() {
        assertDoesNotThrow(() -> nonEmptyListValidator.ensureValid("testkey", Arrays.asList("foo")));
    }

}
