package org.apache.kafka.clients.admin;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.internals.KafkaFutureImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Hack: work around unreachable constructor, set up completion with either Throwable or given result.
 */
public class MockDescribeTopicsResult extends DescribeTopicsResult {

    private final Throwable exceptionToThrow;

    private final Map<String, TopicDescription> descriptionsToReturn;

    MockDescribeTopicsResult(Map<String, TopicDescription> descriptionsToReturn, Throwable exceptionToThrow) {
        super(new HashMap<>());
        this.descriptionsToReturn = descriptionsToReturn;
        this.exceptionToThrow = exceptionToThrow;
    }

    public static MockDescribeTopicsResult of(Map<String, TopicDescription> descriptionsToReturn) {
        return new MockDescribeTopicsResult(descriptionsToReturn, null);
    }

    public static MockDescribeTopicsResult failingWith(Throwable exceptionToThrow) {
        return new MockDescribeTopicsResult(null, exceptionToThrow);
    }

    @Override
    public KafkaFuture<Map<String, TopicDescription>> all() {
        return new KafkaFutureImpl<>() {
            @Override
            public KafkaFuture<Map<String, TopicDescription>> whenComplete(BiConsumer biConsumer) {
                biConsumer.accept(descriptionsToReturn, exceptionToThrow);
                return new KafkaFutureImpl<>();
            }
        };
    }
}
