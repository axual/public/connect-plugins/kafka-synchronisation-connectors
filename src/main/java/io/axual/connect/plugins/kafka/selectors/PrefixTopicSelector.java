package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;

import java.util.Map;

/**
 * The prefix topic selector will always return the original topic name prefixed with the configuration string as the selected topic.
 * It does not accept a null config String or a String with only whitespace characters.
 */
public class PrefixTopicSelector implements TopicSelector {
    private String prefix = null;
    public static final String TOPIC_SELECTOR_PREFIX = "prefix";
    public static final String TOPIC_SELECTOR_PREFIX_DOC = "The prefix to use for output topic";

    @Override
    public void configure(Map<String, Object> props) {
        AbstractConfig config = new AbstractConfig(configDef(), props);
        this.configure(config.getString(TOPIC_SELECTOR_PREFIX));
    }

    @Override
    public String select(String inputTopic) {
        if (prefix == null) {
            throw new IllegalStateException("Prefix not configured");
        }
        if (inputTopic == null) {
            throw new IllegalArgumentException("No topic set in record");
        }
        return prefix + inputTopic;
    }

    @Override
    public ConfigDef configDef() {
        return new ConfigDef()
                .define(TOPIC_SELECTOR_PREFIX,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.MEDIUM,
                        TOPIC_SELECTOR_PREFIX_DOC);
    }

    private void configure(String config) {
        if (config == null || config.trim().isEmpty()) {
            throw new IllegalArgumentException("Null or empty config fields not allowed");
        }
        this.prefix = config.trim();
    }
}
