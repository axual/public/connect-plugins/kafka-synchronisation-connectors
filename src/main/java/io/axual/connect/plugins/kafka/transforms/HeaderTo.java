package io.axual.connect.plugins.kafka.transforms;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.header.Header;
import org.apache.kafka.connect.header.Headers;
import org.apache.kafka.connect.transforms.Transformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Copies the schema and value of a Connect header to either the key of value of the message.
 */
public abstract class HeaderTo<R extends ConnectRecord<R>> implements Transformation<R> {
    public static final String HEADER_NAME_CONFIG = "name";
    public static final String HEADER_NAME_DOC = "The name of the header which needs to be copied to either key or value";
    public static final String HEADER_REMOVE_CONFIG = "remove";
    public static final String HEADER_REMOVE_DOC = "When set to true the header will be removed from the header collection after copying to key or value";
    private static final Logger LOG = LoggerFactory.getLogger(HeaderTo.class);
    private static final ConfigDef CONFIG_DEF = new ConfigDef()
            .define(HEADER_NAME_CONFIG, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new ConfigDef.NonEmptyString(), ConfigDef.Importance.HIGH, HEADER_NAME_DOC)
            .define(HEADER_REMOVE_CONFIG, ConfigDef.Type.BOOLEAN, false, ConfigDef.Importance.MEDIUM, HEADER_REMOVE_DOC);

    private String headerName = null;
    private boolean removeHeader = false;


    @Override
    public R apply(R record) {
        if (headerName == null) {
            // should never happen
            throw new IllegalStateException("Transformation not configured");
        }

        final Header toCopy = record.headers().lastWithName(headerName);
        if (toCopy == null) {
            // Nothing to copy, use original record
            if (LOG.isDebugEnabled()) {
                LOG.debug("Record from topic {} partition {} does not have header {}, returning original record", record.topic(), record.kafkaPartition(), headerName);
            }
            return record;
        }

        final Headers headers = record.headers().duplicate();

        if (removeHeader) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Removing header {} from topic {} partition {}", headerName, record.topic(), record.kafkaPartition());
            }
            headers.remove(headerName);
        }

        return createConnectRecord(record, headers, toCopy.schema(), toCopy.value());
    }

    @Override
    public ConfigDef config() {
        return new ConfigDef(CONFIG_DEF);
    }

    @Override
    public void close() {
        // Nothing to do
    }

    @Override
    public void configure(Map<String, ?> configs) {
        AbstractConfig config = new AbstractConfig(CONFIG_DEF, configs);
        headerName = config.getString(HEADER_NAME_CONFIG);
        removeHeader = config.getBoolean(HEADER_REMOVE_CONFIG);
    }

    abstract R createConnectRecord(R record, Headers newHeaders, Schema schema, Object data);

    public static final class Key<R extends ConnectRecord<R>> extends HeaderTo<R> {

        @Override
        R createConnectRecord(R record, Headers newHeaders, Schema schema, Object data) {
            LOG.debug("Creating new record and replacing the key with header data");
            return record.newRecord(record.topic(), record.kafkaPartition(), schema, data, record.valueSchema(), record.value(), record.timestamp(), newHeaders);
        }
    }

    public static final class Value<R extends ConnectRecord<R>> extends HeaderTo<R> {

        @Override
        R createConnectRecord(R record, Headers newHeaders, Schema schema, Object data) {
            LOG.debug("Creating new record and replacing the value with header data");
            return record.newRecord(record.topic(), record.kafkaPartition(), record.keySchema(), record.key(), schema, data, record.timestamp(), newHeaders);
        }
    }
}
