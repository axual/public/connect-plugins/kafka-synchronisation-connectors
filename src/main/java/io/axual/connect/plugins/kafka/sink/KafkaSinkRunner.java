package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.selectors.NullPartitionSelector;
import io.axual.connect.plugins.kafka.selectors.SourcePartitionSelector;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.FencedInstanceIdException;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.errors.RetriableException;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.utils.Utils;
import org.apache.kafka.connect.sink.SinkRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import io.axual.connect.plugins.kafka.exceptions.ProduceFailedException;
import io.axual.connect.plugins.kafka.exceptions.SinkRunnerProduceFailedException;
import io.axual.connect.plugins.kafka.exceptions.UnsupportedDataTypeException;
import io.axual.connect.plugins.kafka.selectors.PartitionSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import static io.axual.connect.plugins.kafka.sink.KafkaSinkConfig.PARTITION_SELECTOR_PARTITIONER;
import static io.axual.connect.plugins.kafka.sink.KafkaSinkConfig.PARTITION_SELECTOR_SOURCE;

/**
 * This is the worker for the Sink logic. It allows outside threads to place a record collection on the queue which need to be processed.
 * The runner will convert, enrich and produce the record while maintaining a record of the offsets of successfully produced records.
 */
public class KafkaSinkRunner implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaSinkRunner.class);

    private final AtomicBoolean stopWorker = new AtomicBoolean(false);
    private final AtomicBoolean isRunning = new AtomicBoolean(false);
    private final Map<TopicPartition, OffsetAndMetadata> storedOffsets = new HashMap<>();
    private final BlockingQueue<Collection<SinkRecord>> recordQueue;
    private final long putWaitMs;
    private final Collection<Header> staticHeaders;
    private final Optional<String> forwardTopicHeader;
    private final Optional<String> forwardPartitionHeader;
    private final Optional<String> forwardOffsetHeader;
    private final Optional<String> forwardTimestampHeader;

    private final ProducerFactory producerFactory = new ProducerFactory();
    private final AtomicReference<Producer<byte[], byte[]>> producer = new AtomicReference<>();

    private final PartitionSelector partitionSelector;
    private final TopicSelector topicSelector;

    private final String forwardedHeaderPrefix;
    private final Integer maxProduceRetries;
    private final Map<String, String> logContext;
    private final Consumer<RuntimeException> topicPartitionFailedHandler;
    private String sinkRunnerFailureDescription;

    private final Map<TopicPartition, List<SinkRecord>> failedProduceQueue = new ConcurrentHashMap<>();
    private final Map<MessageCoordinates, AtomicInteger> produceRetries = new ConcurrentHashMap<>();

    private final KafkaSinkConfig config;

    private Long threadID = -1L;

    public KafkaSinkRunner(KafkaSinkConfig config, Consumer<RuntimeException> topicPartitionFailedHandler) {
        this.config = config;
        this.producer.set(producerFactory.createProducer(config));
        this.recordQueue = new LinkedBlockingQueue<>(config.getQueueSize());
        this.putWaitMs = config.getQueuePutWaitMs();
        this.staticHeaders = config.getStaticHeaders().entrySet().stream()
                .map(e -> new RecordHeader(e.getKey(), e.getValue().getBytes(StandardCharsets.UTF_8)))
                .collect(Collectors.toList());
        this.forwardTopicHeader = config.getTopicHeader();
        this.forwardPartitionHeader = config.getPartitionHeader();
        this.forwardOffsetHeader = config.getOffsetHeader();
        this.forwardTimestampHeader = config.getTimestampHeader();
        this.topicSelector = createTopicSelector(config);
        this.topicSelector.configure(config.originalsWithPrefix(KafkaSinkConfig.TOPIC_SELECTOR_CONFIG + "."));
        this.partitionSelector = createSelector(config.getPartitionSelector());
        this.forwardedHeaderPrefix = config.getHeaderPrefix();
        this.maxProduceRetries = config.getMaxProduceRetries();
        this.logContext = MDC.getCopyOfContextMap();
        this.topicPartitionFailedHandler = topicPartitionFailedHandler;
    }

    /**
     * Create a PartitionSelector for the specified method.
     *
     * @param method The required selector method
     * @return An instantiated PartitionSelector
     */
    public static PartitionSelector createSelector(String method) {
        if (method == null) {
            throw new IllegalArgumentException("Null method is not supported");
        }

        return switch (method) {
            case PARTITION_SELECTOR_SOURCE -> new SourcePartitionSelector();
            case PARTITION_SELECTOR_PARTITIONER -> new NullPartitionSelector();
            default -> throw new IllegalArgumentException("Unknown method " + method + " is not supported");
        };
    }

    /**
     * Instantiates TopicSelector from the given config.
     * <p>
     * TOPIC_SELECTOR_CLASS_CONFIG takes precedence over deprecated TOPIC_SELECTOR_CONFIG
     * @param config Sink Connector configuration
     * @return the {@link TopicSelector} implementation to use.
     */
    public static TopicSelector createTopicSelector(KafkaSinkConfig config) {
        return config.getTopicSelectorClass() != null ?
                TopicSelector.createSelector(config.getTopicSelectorClass()) : TopicSelector.createSelector(config.getTopicSelector());
    }

    /**
     * Put a collection of SinkRecords on the queue,
     *
     * @param records the collection of records to be produced
     * @return true if the collection was placed on the queue
     * @throws InterruptedException when the offer is interrupted
     */
    public boolean put(Collection<SinkRecord> records) throws InterruptedException {
        return recordQueue.offer(records, putWaitMs, TimeUnit.MILLISECONDS);
    }

    /**
     * Get the offsets of the records already produced
     *
     * @return the map containing the offsets
     */
    public Map<TopicPartition, OffsetAndMetadata> getStoredOffsetsAndClear() {
        try {
            return new HashMap<>(storedOffsets);
        } finally {
            storedOffsets.clear();
        }
    }

    /**
     * Stop the runner
     */
    public void stop() {
        stopWorker.set(true);
        closeProducer(producer.get());
    }

    /**
     * Is the runner still active
     *
     * @return true if the runner is still active
     */
    public boolean isRunning() {
        return isRunning.get();
    }

    public String getFailureDescription() {
        return this.sinkRunnerFailureDescription;
    }

    private SinkRunnerProduceFailedException stopWithFailure(Exception rte) {
        return stopWithFailure(rte.getMessage(), rte);
    }

    private SinkRunnerProduceFailedException stopWithFailure(String message, Exception rte) {
        this.isRunning.set(false);
        sinkRunnerFailureDescription = rte.getCause() == null ?
                String.format("%s, caused by: %s", message, rte.getMessage()) : String.format("%s, caused by: %s", message, rte.getCause().getMessage());
        return new SinkRunnerProduceFailedException(message, rte);
    }

    public Long getThreadID() {
        return threadID;
    }

    @Override
    public void run() {
        if (isRunning.getAndSet(true)) {
            LOG.error("Runner was already active, stopping");
            return;
        }

        MDC.setContextMap(logContext);
        threadID = Thread.currentThread().getId();
        LOG.info("New thread ID {}", threadID);

        while (!stopWorker.get()) {
            // Poll with timeout to allow worker thread to be stopped
            if (!failedProduceQueue.isEmpty()) {
                LOG.info("FailedProduceQueue has size of partitions: {} in records: {}", failedProduceQueue.size(), failedProduceQueue.values().stream().mapToInt(List::size).sum());
                processRecords(failedProduceQueue, true);
            } else {
                Collection<SinkRecord> records = recordQueue.peek();
                if (Objects.isNull(records)) {
                    // Prevents CPU spikes in case of no data
                    Utils.sleep(50);
                    continue;
                }
                LOG.debug("Runner has received {} records.", records.size());
                // Create a map of topicPartition and list of records belonging to that topicPartition
                Map<TopicPartition, List<SinkRecord>> topicPartitionRecordsMap = records.stream()
                        .collect(Collectors.groupingBy(r -> new TopicPartition(r.topic(), r.kafkaPartition())));
                processRecords(topicPartitionRecordsMap);
                LOG.debug("Records successfully sent to producer thread {}", threadID);
                if (!isRunning.get()) {
                    throw new ProduceFailedException("An exception occurred while producing a record");
                }
                recordQueue.poll();
                topicPartitionRecordsMap.clear();
                LOG.trace("Process loop completed {}", threadID);
            }
        }
        isRunning.set(false);
    }

    private void processRecords(Map<TopicPartition, List<SinkRecord>> toProduce) {
        processRecords(toProduce, false);
    }

    /**
     * This will process records per topic partition
     */
    protected void processRecords(Map<TopicPartition, List<SinkRecord>> toProduce, boolean isFailedProduceQueue) {
        Map<TopicPartition, List<SinkRecord>> topicPartitionRecordsMap = new HashMap<>(toProduce);
        LOG.trace("Processing {} records, from FailedProduceQueue? {}", toProduce.size(), isFailedProduceQueue);

        topicPartitionRecordsMap
                .values()
                .stream()
                .map(this::produceRecords)
                .forEach(futures -> {
                    // using For loop, so that we can break it in case any topicPartition record fail to produce
                    for (FutureWrapper future : futures) {
                        try {
                            MDC.setContextMap(logContext);
                            // wait for produce callback to complete
                            RecordMetadata metadata = future.getKafkaFuture().get();
                            // increase offset by 1 so that next poll should not include committed record
                            long offset = future.getSourceOffset() + 1;
                            // store topicPartition offset
                            storedOffsets.put(new TopicPartition(future.getSourceTopicName(), future.getSourcePartition()), new OffsetAndMetadata(offset));
                            if (isFailedProduceQueue) {
                                removeFromFailedProduceQueue(toProduce, future, metadata);
                            }
                        } catch (InterruptedException e) {
                            LOG.error("Produce operation was interrupted", e.getCause());
                            Thread.currentThread().interrupt();
                            throw stopWithFailure(new ProduceFailedException("Get result was interrupted", e));
                        } catch (ExecutionException e) {
                            handleExecutionException(isFailedProduceQueue, future, e);
                        }
                    }
                });
    }

    private void handleExecutionException(boolean isFailedProduceQueue, FutureWrapper future, ExecutionException e) {
        LOG.error("An ExecutionException occurred while producing a record, cause:", e.getCause());
        if (maxProduceRetries == 0) {
            LOG.warn("Will not reattempt a failed produce when max.produce.retries set to 0");
            throw stopWithFailure(e);
        }

        if (e.getCause() instanceof RetriableException) {
            handleRetriableException(isFailedProduceQueue, future, e);
        } else if (e.getCause() instanceof FencedInstanceIdException) {
            LOG.info("Recreating the producer to resolve FencedInstanceIdException");
            Producer<byte[], byte[]> oldProducer = this.producer.getAndSet(producerFactory.createProducer(config));
            closeProducer(oldProducer);
        } else {
            // set isRunning to false so no new records added to queue from task#put()
            throw stopWithFailure("Unexpected Execution Exception", e);
        }
    }

    private void handleRetriableException(boolean isFailedProduceQueue, FutureWrapper future, ExecutionException e) {
        if (!isFailedProduceQueue) {
            failedProduceQueuePut(future);
        } else {
            countRetryForTopicPartition(future);

            if (exceededMaxRetriesForSinkRecord(future)) {
                SinkRunnerProduceFailedException exception = stopWithFailure(String.format("Max produce retries (%d) exceeded for TP: %s-%d",
                        maxProduceRetries, future.getSourceTopicName(), future.getSourcePartition()), e);
                topicPartitionFailedHandler.accept(exception);
                throw exception;
            }
        }
    }

    private void closeProducer(Producer<byte[], byte[]> producer) {
        try {
            producer.flush();
            producer.close();
        } catch (InterruptException ex) {
            LOG.error("Producer close operation was interrupted", ex.getCause());
            Thread.currentThread().interrupt();
        }
    }

    private void countRetryForTopicPartition(FutureWrapper futureWrapper) {
        produceRetries.computeIfAbsent(
                MessageCoordinates.of(futureWrapper),
                k -> new AtomicInteger()
        ).incrementAndGet();
    }

    private boolean exceededMaxRetriesForSinkRecord(FutureWrapper futureWrapper) {
        return produceRetries.get(MessageCoordinates.of(futureWrapper)).get() > maxProduceRetries;
    }

    private synchronized void removeFromFailedProduceQueue(Map<TopicPartition, List<SinkRecord>> toProduce, FutureWrapper future, RecordMetadata metadata) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Successfully produced from FailedProduceQueue to TP {}-{} FailedProduceQueue of size in partitions: {} in records: {}",
                    metadata.topic(), metadata.partition(), toProduce.size(), failedProduceQueue.values().stream().mapToInt(List::size).sum());
        }
        TopicPartition topicPartition = new TopicPartition(future.getSourceTopicName(), future.getOriginalPayload().kafkaPartition());
        toProduce.get(topicPartition).remove(future.getOriginalPayload());
        if (toProduce.get(topicPartition).isEmpty()) {
            toProduce.remove(topicPartition);
        }
        produceRetries.remove(MessageCoordinates.of(future));
        LOG.debug("FailedProduceQueue size: {}", toProduce.size());
    }

    private synchronized void failedProduceQueuePut(FutureWrapper futureWrapper) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Putting SinkRecord to FailedProduceQueue TP {}-{}", futureWrapper.originalPayload.topic(), futureWrapper.sourcePartition);
        }
        TopicPartition topicPartition = new TopicPartition(futureWrapper.originalPayload.topic(), futureWrapper.sourcePartition);
        failedProduceQueue.computeIfAbsent(
                topicPartition,
                k -> new ArrayList<>()
        ).add(futureWrapper.getOriginalPayload());
        LOG.debug("FailedProduceQueue size in partitions: {} in records: {}", failedProduceQueue.size(), failedProduceQueue.values().stream().mapToInt(List::size).sum());
    }

    /**
     * Convert and produce records
     */
    private List<FutureWrapper> produceRecords(Collection<SinkRecord> records) {
        return records.stream()
                .map(sinkRecord -> {
                    LOG.debug("Processing record: topic: {}, partition: {}, offset: {}", sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
                    ProducerRecord<byte[], byte[]> producerRecord = convertConnectRecord(sinkRecord);
                    LOG.trace("ProducerRecord {} for topic: {} partition: {} offset: {}", producerRecord, sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
                    Future<RecordMetadata> future = producer.get().send(producerRecord);
                    LOG.trace("Record send for topic: {} partition: {} offset: {}", sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
                    // wrapping into FutureWrapper so we can have source topic, partition & offsets
                    return FutureWrapper.builder()
                            .kafkaFuture(future)
                            .sourceTopicName(sinkRecord.topic())
                            .sourcePartition(sinkRecord.kafkaPartition())
                            .sourceOffset(sinkRecord.kafkaOffset())
                            .originalPayload(sinkRecord)
                            .build();
                })
                .collect(Collectors.toList());
    }

    /**
     * Record conversion helper, creates a Kafka ProducerRecord from a Connect SinkRecord
     *
     * @param sinkRecord the Connect SinkRecord to convert
     * @return the Kafka ProducerRecord equivalent of the SinkRecord provided
     */
    public ProducerRecord<byte[], byte[]> convertConnectRecord(SinkRecord sinkRecord) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("Converting sink record topic: {} partition: {} offset: {}", sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
        }
        RecordHeaders headers = convertConnectHeaders(sinkRecord.headers());

        LOG.trace("Record headers: {}", headers);

        LOG.trace("Adding record headers to static headers");
        // Add static headers
        staticHeaders.forEach(headers::add);

        // Add forwarding headers
        forwardTopicHeader.ifPresent(hdr -> headers.add(hdr, sinkRecord.topic().getBytes(StandardCharsets.UTF_8)));
        forwardPartitionHeader.ifPresent(hdr -> headers.add(hdr, serialize(sinkRecord.kafkaPartition())));
        forwardOffsetHeader.ifPresent(hdr -> headers.add(hdr, serialize(sinkRecord.kafkaOffset())));
        forwardTimestampHeader.ifPresent(hdr -> headers.add(hdr, serialize(sinkRecord.timestamp())));

        String topic = topicSelector.select(sinkRecord);
        Integer partition = partitionSelector.select(sinkRecord);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Creating producerRecord for topic: {} partition: {} offset: {}", sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
            LOG.trace("TopicSelector class: {}", topicSelector.getClass().getName());
            LOG.trace("PartitionSelector class: {}", partitionSelector.getClass().getName());
            LOG.trace("Record target topic: {} for sinkRecord topic: {} partition: {} offset: {}", topic, sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
            LOG.trace("Record target partition: {} for sinkRecord topic: {} partition: {} offset: {}", partition, sinkRecord.topic(), sinkRecord.kafkaPartition(), sinkRecord.kafkaOffset());
        }

        return new ProducerRecord<>(
                topic,
                partition,
                sinkRecord.timestamp(),
                convertData(sinkRecord.key()),
                convertData(sinkRecord.value()),
                headers
        );
    }

    /**
     * Helper method for use in the creation of a producer record. Records that can't be converted will be filtered out.
     *
     * @param headers The Connect Headers to be used in the producer
     * @return The Kafka Record Headers to be attached to the produced record
     */
    private RecordHeaders convertConnectHeaders(org.apache.kafka.connect.header.Headers headers) {
        return new RecordHeaders(StreamSupport.stream(headers.spliterator(), false)
                .filter(Objects::nonNull)
                .map(this::convertConnectHeader)
                .filter(Objects::nonNull)
                .toList());
    }

    /**
     * Converts a Kafka Connect Header to Kafka Record Header.
     *
     * @param header the Connect header to convert
     * @return the converted header, or null if an unsupported value was used.
     */
    private Header convertConnectHeader(org.apache.kafka.connect.header.Header header) {
        final String key = header.key();
        try {
            LOG.debug("Converting header: {}", key);
            return new RecordHeader(forwardedHeaderPrefix + key, convertData(header.value()));
        } catch (UnsupportedDataTypeException e) {
            LOG.debug("Dropping header '{}' because of unsupported header type {}", key, e.getUnsupportedClass().getName());
            return null;
        }
    }

    /**
     * Used to convert key or value data to a byte array to be send to the remote cluster
     *
     * @param data the object to convert
     * @return the converted data
     * @throws UnsupportedDataTypeException when an unsupported data type is provided
     */
    private byte[] convertData(Object data) {
        if (data == null) {
            return null;
        }

        LOG.debug("Converting record data to byte[]");

        if (data instanceof byte[] bytes) {
            return bytes;
        }

        if (data instanceof ByteBuffer bytebuffer) {
            return bytebuffer.array();
        }

        throw new UnsupportedDataTypeException(data.getClass());
    }

    /**
     * Serialize the integer value to a byte[]
     *
     * @param toSerialize the integer to serialize
     * @return the serialized data
     */
    private byte[] serialize(Integer toSerialize) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(toSerialize);
        return buffer.array();
    }

    /**
     * Serialize the long value to a byte[]
     *
     * @param toSerialize the long to serialize
     * @return the serialized data
     */
    private byte[] serialize(Long toSerialize) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(toSerialize);
        return buffer.array();
    }

    /**
     * return true, if record queue is empty false otherwise
     */
    public boolean isRecordQueueEmpty() {
        return this.recordQueue.isEmpty();
    }

    @Builder
    @Getter
    private static class FutureWrapper {
        private String sourceTopicName;
        private int sourcePartition;
        private long sourceOffset;
        private Future<RecordMetadata> kafkaFuture;
        /**
         * Used to retry message send
         */
        private SinkRecord originalPayload;
    }

    @Builder
    @Getter
    @EqualsAndHashCode
    static class MessageCoordinates {
        private String topic;
        private int partition;
        private long offset;

        public static MessageCoordinates of(FutureWrapper futureWrapper) {
            return new MessageCoordinates(futureWrapper.sourceTopicName, futureWrapper.sourcePartition, futureWrapper.sourceOffset);
        }
    }
}
