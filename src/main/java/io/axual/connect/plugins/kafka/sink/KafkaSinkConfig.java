package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.serialization.ByteArraySerializer;

import java.util.*;

import static io.axual.connect.plugins.kafka.validators.TopicSelectorClassValidator.topicSelectorClassValidator;
import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_DOC;

/**
 * Configuration class for the Kafka Sink Connector implementation
 */
public class KafkaSinkConfig extends AbstractConfig {
    public static final String REMOTE_CONFIG_PREFIX = "remote.";

    public static final String SINK_QUEUE_SIZE_CONFIG = "queue.size";
    public static final Integer SINK_QUEUE_SIZE_DEFAULT = 10;
    public static final String SINK_QUEUE_SIZE_DOC = "The size of the queue used to feed the producer thread. The value is about the number of collections of records to be put, not the total number of records enqueued";
    public static final String SINK_QUEUE_PUT_WAIT_MS_CONFIG = "queue.put.wait.ms";
    public static final Long SINK_QUEUE_PUT_WAIT_MS_DEFAULT = 500L;
    public static final String SINK_QUEUE_PUT_WAIT_MS_DOC = "The time to wait before the put method fails with a Connect RetriableException.";
    public static final String STATIC_HEADER_ALIAS_CONFIG = "header.static.aliases";
    public static final List<String> STATIC_HEADER_ALIAS_DEFAULT = Collections.emptyList();
    public static final String STATIC_HEADER_ALIAS_DOC = "A list of aliases used as keys for describing static headers";
    public static final String STATIC_HEADER_NAME_CONFIG_FORMAT = "header.static.%s.name";
    public static final String STATIC_HEADER_VALUE_CONFIG_FORMAT = "header.static.%s.value";

    public static final String FORWARD_TOPIC_HEADER_NAME_CONFIG = "forward.metadata.topic.name";
    public static final String FORWARD_TOPIC_HEADER_NAME_DEFAULT = "X-Kafka-Original-Topic";
    public static final String FORWARD_TOPIC_HEADER_NAME_DOC = "The name of the header which will contain the source topic name";

    public static final String FORWARD_PARTITION_HEADER_NAME_CONFIG = "forward.metadata.partition.name";
    public static final String FORWARD_PARTITION_HEADER_NAME_DEFAULT = "X-Kafka-Original-Partition";
    public static final String FORWARD_PARTITION_HEADER_NAME_DOC = "The name of the header which will contain the source partition number";

    public static final String FORWARD_OFFSET_HEADER_NAME_CONFIG = "forward.metadata.offset.name";
    public static final String FORWARD_OFFSET_HEADER_NAME_DEFAULT = "X-Kafka-Original-Offset";
    public static final String FORWARD_OFFSET_HEADER_NAME_DOC = "The name of the header which will contain the source offset";

    public static final String FORWARD_TIMESTAMP_HEADER_NAME_CONFIG = "forward.metadata.timestamp.name";
    public static final String FORWARD_TIMESTAMP_HEADER_NAME_DEFAULT = "X-Kafka-Original-Timestamp";
    public static final String FORWARD_TIMESTAMP_HEADER_NAME_DOC = "The name of the header which will contain the source record timestamp";

    public static final String PARTITION_SELECTOR_SOURCE = "source";
    public static final String PARTITION_SELECTOR_PARTITIONER = "partitioner";
    public static final String PARTITION_SELECTOR_CONFIG = "partition.selector";
    public static final String PARTITION_SELECTOR_DEFAULT = PARTITION_SELECTOR_SOURCE;
    public static final String PARTITION_SELECTOR_DOC = "The way that the target partition is determined. Valid values are <ul><li>source: use the same partition as the original record</li><li>partitioner: use the producer partitioner</li></ul>";

    public static final String TOPIC_SELECTOR_CLASS_CONFIG = TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG;
    public static final Class<?> TOPIC_SELECTOR_CLASS_DEFAULT = null;
    public static final String TOPIC_SELECTOR_CLASS_DOC = "The way that the target topic name is determined. Valid values include classes that implement the TopicSelector interface";
    public static final String TOPIC_SELECTOR_CONFIG = TopicSelector.TOPIC_SELECTOR_CONFIG;
    public static final String TOPIC_SELECTOR_DEFAULT = TopicSelector.Type.source.name();
    public static final String TOPIC_SELECTOR_DOC = "Deprecated, please use " + TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG + " instead. The way that the target topic name is determined. Valid values are: " + String.join(",", Arrays.asList(TopicSelector.Type.names()));

    public static final String MAX_PRODUCE_RETRIES_CONFIG = "max.produce.retries";
    public static final Integer MAX_PRODUCE_RETRIES_DEFAULT = 2;
    public static final String MAX_PRODUCE_RETRIES_DOC = "The remote producer will perform it's retries as defined by the KafkaProducer configuration. A failure after those or of another sort will be caught by the connector and reattempted. If this fails too the task will fail and manual action will be required";

    private static final String REMOTE_GROUP_PREFIX = "remote";
    public static final String REMOTE_HEADER_PREFIX_CONFIG = "header.remote.prefix";
    public static final String REMOTE_HEADER_PREFIX_CONFIG_DEFAULT = "";
    public static final String REMOTE_HEADER_PREFIX_CONFIG_DOC = "Prefix for forwarded remote headers";

    private static final ConfigDef CONFIG;

    static {
        CONFIG = new ConfigDef()
                .define(TOPIC_SELECTOR_CONFIG,
                        ConfigDef.Type.STRING,
                        TOPIC_SELECTOR_DEFAULT,
                        ConfigDef.ValidString.in(TopicSelector.Type.names()),
                        ConfigDef.Importance.LOW,
                        TOPIC_SELECTOR_DOC)
                .define(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG,
                        ConfigDef.Type.CLASS,
                        TOPIC_SELECTOR_CLASS_DEFAULT,
                        topicSelectorClassValidator(),
                        ConfigDef.Importance.HIGH,
                        TOPIC_SELECTOR_CLASS_DOC)

                .define(PARTITION_SELECTOR_CONFIG,
                        ConfigDef.Type.STRING,
                        PARTITION_SELECTOR_DEFAULT,
                        ConfigDef.ValidString.in(PARTITION_SELECTOR_SOURCE, PARTITION_SELECTOR_PARTITIONER),
                        ConfigDef.Importance.HIGH,
                        PARTITION_SELECTOR_DOC)

                .define(SINK_QUEUE_SIZE_CONFIG,
                        ConfigDef.Type.INT,
                        SINK_QUEUE_SIZE_DEFAULT,
                        ConfigDef.Range.atLeast(1),
                        ConfigDef.Importance.MEDIUM,
                        SINK_QUEUE_SIZE_DOC)
                .define(SINK_QUEUE_PUT_WAIT_MS_CONFIG,
                        ConfigDef.Type.LONG,
                        SINK_QUEUE_PUT_WAIT_MS_DEFAULT,
                        ConfigDef.Range.atLeast(0),
                        ConfigDef.Importance.MEDIUM,
                        SINK_QUEUE_PUT_WAIT_MS_DOC)

                .define(STATIC_HEADER_ALIAS_CONFIG,
                        ConfigDef.Type.LIST,
                        STATIC_HEADER_ALIAS_DEFAULT,
                        ConfigDef.Importance.MEDIUM,
                        STATIC_HEADER_ALIAS_DOC)

                .define(FORWARD_TOPIC_HEADER_NAME_CONFIG,
                        ConfigDef.Type.STRING,
                        FORWARD_TOPIC_HEADER_NAME_DEFAULT,
                        ConfigDef.Importance.MEDIUM,
                        FORWARD_TOPIC_HEADER_NAME_DOC)

                .define(FORWARD_PARTITION_HEADER_NAME_CONFIG,
                        ConfigDef.Type.STRING,
                        FORWARD_PARTITION_HEADER_NAME_DEFAULT,
                        ConfigDef.Importance.MEDIUM,
                        FORWARD_PARTITION_HEADER_NAME_DOC)

                .define(FORWARD_OFFSET_HEADER_NAME_CONFIG,
                        ConfigDef.Type.STRING,
                        FORWARD_OFFSET_HEADER_NAME_DEFAULT,
                        ConfigDef.Importance.MEDIUM,
                        FORWARD_OFFSET_HEADER_NAME_DOC)

                .define(FORWARD_TIMESTAMP_HEADER_NAME_CONFIG,
                        ConfigDef.Type.STRING,
                        FORWARD_TIMESTAMP_HEADER_NAME_DEFAULT,
                        ConfigDef.Importance.MEDIUM,
                        FORWARD_TIMESTAMP_HEADER_NAME_DOC)

                .define(REMOTE_HEADER_PREFIX_CONFIG,
                        ConfigDef.Type.STRING,
                        REMOTE_HEADER_PREFIX_CONFIG_DEFAULT,
                        ConfigDef.Importance.MEDIUM,
                        REMOTE_HEADER_PREFIX_CONFIG_DOC)

                .define(MAX_PRODUCE_RETRIES_CONFIG,
                        ConfigDef.Type.INT,
                        MAX_PRODUCE_RETRIES_DEFAULT,
                        ConfigDef.Importance.LOW,
                        MAX_PRODUCE_RETRIES_DOC)
        ;

        // Create a new ConfigDef to contain the cleaned producer config def 
        ConfigDef producerConfigDef = new ConfigDef();
        for (ConfigDef.ConfigKey configKey : ProducerConfig.configDef().configKeys().values()) {
            // remove configs for key and value serializers, as we hard code these internally.
            if (ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG.equals(configKey.name) || ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG.equals(configKey.name)) {
                continue;
            }

            final ConfigDef.Type newType = configKey.type == ConfigDef.Type.CLASS ? ConfigDef.Type.STRING : configKey.type;
            final Object newDefault;
            if (configKey.defaultValue instanceof Class<?>) {
                newDefault = ((Class<?>) configKey.defaultValue).getName();
            } else if (configKey.defaultValue instanceof Collection<?>) {
                newDefault = ((Collection<?>) configKey.defaultValue).stream()
                        .map(value -> value instanceof Class<?> ? ((Class) value).getName() : value)
                        .toList();
            } else {
                newDefault = configKey.defaultValue;
            }

            if (ProducerConfig.BOOTSTRAP_SERVERS_CONFIG.equals(configKey.name)) {
                producerConfigDef.define(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ConfigDef.Type.LIST, ConfigDef.Importance.HIGH, BOOTSTRAP_SERVERS_DOC);
                continue;
            }

            // Add the cleaned configDef keys, with the original settings, allowing embed to properly embed dependents and recommender as well
            producerConfigDef.define(configKey.name, newType, newDefault, configKey.validator, configKey.importance, configKey.documentation, configKey.group, configKey.orderInGroup, configKey.width, configKey.displayName, configKey.dependents, configKey.recommender);
        }

        // Embed the cleaned config with the right names
        CONFIG.embed(REMOTE_CONFIG_PREFIX, REMOTE_GROUP_PREFIX, 0, producerConfigDef);
    }

    private final Map<String, String> staticHeaders;

    public KafkaSinkConfig(Map<String, String> originals) {
        super(configDef(), originals);

        List<String> headerErrors = new ArrayList<>();
        Map<String, String> headers = new HashMap<>();
        final List<String> aliases = getList(STATIC_HEADER_ALIAS_CONFIG);
        for (String alias : aliases) {
            String configNameKey = String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, alias);
            String configNameValue = String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, alias);

            boolean errorsFound = false;
            if (!originals.containsKey(configNameKey)) {
                errorsFound = true;
                headerErrors.add(String.format("No name defined for static header alias %s. Expected configuration: %s", alias, configNameKey));
            }
            if (!originals.containsKey(configNameValue)) {
                errorsFound = true;
                headerErrors.add(String.format("No value defined for static header alias %s. Expected configuration: %s", alias, configNameValue));
            }

            if (errorsFound)
                continue;

            headers.put(
                    (String) ConfigDef.parseType(configNameKey, originals.get(configNameKey), ConfigDef.Type.STRING),
                    (String) ConfigDef.parseType(configNameKey, originals.get(configNameValue), ConfigDef.Type.STRING));
        }

        if (!headerErrors.isEmpty()) {
            throw new ConfigException(STATIC_HEADER_ALIAS_CONFIG, aliases, String.join("\n", headerErrors));
        }
        staticHeaders = Collections.unmodifiableMap(headers);
    }

    /**
     * Get a new instance of the configuration definition
     *
     * @return a new instance of the configuration definition
     */
    public static ConfigDef configDef() {
        return new ConfigDef(CONFIG);
    }

    /**
     * Return the original configuration options for the producer with the remote prefixes stripped
     *
     * @return configuration properties for the remote producer
     */
    public Map<String, Object> getRemoteConfig() {
        final Map<String, Object> producerConfig = originalsWithPrefix(REMOTE_CONFIG_PREFIX, true);
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
        return producerConfig;
    }

    /**
     * Get the size of the runner queue containing the record collections to process.
     *
     * @return the maximum size of the queue
     */
    public int getQueueSize() {
        return getInt(SINK_QUEUE_SIZE_CONFIG);
    }

    /**
     * Get the number of milliseconds to wait until timing out the offer to put a collection of records on the runner queue
     *
     * @return the number of milliseconds to wait
     */
    public long getQueuePutWaitMs() {
        return getLong(SINK_QUEUE_PUT_WAIT_MS_CONFIG);
    }

    /**
     * Get the static Kafka headers to send with every record.
     *
     * @return a map containing the header names and values
     * @see #STATIC_HEADER_ALIAS_CONFIG
     * @see #STATIC_HEADER_NAME_CONFIG_FORMAT
     */
    public Map<String, String> getStaticHeaders() {
        return staticHeaders;
    }

    /**
     * The header name to use to forward the record topic to the remote cluster as a header.
     *
     * @return an Optional containing the header name to use, or <code>Optional.empty()</code> if it shouldn't be forwarded.
     */
    public Optional<String> getTopicHeader() {
        return Optional.ofNullable(getString(FORWARD_TOPIC_HEADER_NAME_CONFIG));
    }

    /**
     * The header name to use to forward the record partition to the remote cluster as a header.
     *
     * @return an Optional containing the header name to use, or <code>Optional.empty()</code> if it shouldn't be forwarded.
     */
    public Optional<String> getPartitionHeader() {
        return Optional.ofNullable(getString(FORWARD_PARTITION_HEADER_NAME_CONFIG));
    }

    /**
     * The header name to use to forward the record offset to the remote cluster as a header.
     *
     * @return an Optional containing the header name to use, or <code>Optional.empty()</code> if it shouldn't be forwarded.
     */
    public Optional<String> getOffsetHeader() {
        return Optional.ofNullable(getString(FORWARD_OFFSET_HEADER_NAME_CONFIG));
    }

    /**
     * The header name to use to forward the record timestamp to the remote cluster as a header.
     *
     * @return an Optional containing the header name to use, or <code>Optional.empty()</code> if it shouldn't be forwarded.
     */
    public Optional<String> getTimestampHeader() {
        return Optional.ofNullable(getString(FORWARD_TIMESTAMP_HEADER_NAME_CONFIG));
    }

    /**
     * Get the target topic selection method.
     *
     * @return the selection method
     */
    public Class<?> getTopicSelectorClass() {
        return getClass(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG);
    }

    /**
     * Get the target topic selection method.
     *
     * @return the selection method
     * @deprecated should switch to configuration by class name instead.
     */
    @Deprecated(forRemoval = true, since = "2.0.0")
    public String getTopicSelector() {
        return getString(TOPIC_SELECTOR_CONFIG);
    }

    /**
     * Get the partition selection method.
     *
     * @return the selection method
     */
    public String getPartitionSelector() {
        return getString(PARTITION_SELECTOR_CONFIG);
    }

    /**
     * Get the prefix for forwarded remote headers.
     * @return the configured prefix (never <code>null</code>).
     */
    public String getHeaderPrefix() {
        return getString(REMOTE_HEADER_PREFIX_CONFIG);
    }

    /**
     * Get the max produce retries attempted before the task fails.
     *
     * @return the number of produce retries to wait
     */
    public Integer getMaxProduceRetries() {
        return getInt(MAX_PRODUCE_RETRIES_CONFIG);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KafkaSinkConfig that)) return false;
        if (!super.equals(o)) return false;
        return staticHeaders.equals(that.staticHeaders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), staticHeaders);
    }

}
