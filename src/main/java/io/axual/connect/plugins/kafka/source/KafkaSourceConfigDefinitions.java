package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.selectors.FixedTopicSelector;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;
import io.axual.connect.plugins.kafka.validators.NonEmptyListValidator;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.ConfigDef;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static io.axual.connect.plugins.kafka.validators.TopicSelectorClassValidator.topicSelectorClassValidator;
import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_DOC;
import static org.apache.kafka.common.config.ConfigDef.NO_DEFAULT_VALUE;

/**
 * Utility class to create configuration definitions for {@link KafkaSourceConfig}.
 */
public class KafkaSourceConfigDefinitions {

    public static final String REMOTE_CONFIG_PREFIX = "remote.";
    public static final String REMOTE_GROUP_PREFIX = "remote";
    public static final String SOURCE_TOPICS_CONFIG = "source.topics";
    public static final Object SOURCE_TOPICS_CONFIG_DEFAULT = NO_DEFAULT_VALUE;
    public static final String SOURCE_TOPICS_CONFIG_DOC = "List of topics to subscribe to on the source system";
    public static final String SOURCE_TOPICS_INTERVAL_CONFIG = "source.topics.poll.ms";
    public static final Long SOURCE_TOPICS_INTERVAL_CONFIG_DEFAULT = 100L;
    public static final String SOURCE_TOPICS_INTERVAL_CONFIG_DOC = "Poll interval for the source cluster";
    public static final String TOPIC_HEADER_NAME_CONFIG = "forward.metadata.topic.name";
    public static final String TOPIC_HEADER_NAME_CONFIG_DEFAULT = "X-Kafka-Original-Topic";
    public static final String TOPIC_HEADER_NAME_CONFIG_DOC = "Name of the header in which the originating topic name is sent";
    public static final String PARTITION_HEADER_NAME_CONFIG = "forward.metadata.partition.name";
    public static final String PARTITION_HEADER_NAME_CONFIG_DEFAULT = "X-Kafka-Original-Partition";
    public static final String PARTITION_HEADER_NAME_CONFIG_DOC = "Name of the header in which the originating partition is sent";
    public static final String OFFSET_HEADER_NAME_CONFIG = "forward.metadata.offset.name";
    public static final String OFFSET_HEADER_NAME_CONFIG_DEFAULT = "X-Kafka-Original-Offset";
    public static final String OFFSET_HEADER_NAME_CONFIG_DOC = "Name of the header in which the original offset is sent";
    public static final String TIMESTAMP_HEADER_NAME_CONFIG = "forward.metadata.timestamp.name";
    public static final String TIMESTAMP_HEADER_NAME_CONFIG_DEFAULT = "X-Kafka-Original-Timestamp";
    public static final String TIMESTAMP_HEADER_NAME_CONFIG_DOC = "Name of the header in which the original timestamp is sent";
    public static final String STATIC_HEADER_NAMES_CONFIG = "header.static.aliases";
    public static final String STATIC_HEADER_NAMES_DOC = "An alias list of static headers we want to be present";
    public static final List<String> STATIC_HEADER_NAMES_DEFAULT = Collections.emptyList();
    public static final String STATIC_HEADER_NAME_FORMAT = "header.static.%s.name";
    public static final String STATIC_VALUE_NAME_FORMAT = "header.static.%s.value";
    public static final String REMOTE_HEADER_PREFIX_CONFIG = "header.remote.prefix";
    public static final String REMOTE_HEADER_PREFIX_CONFIG_DEFAULT = "";
    public static final String REMOTE_HEADER_PREFIX_CONFIG_DOC = "Prefix for forwarded remote headers";

    public static final String TOPIC_SELECTOR_CLASS_CONFIG = "topic.selector.class";
    public static final Class<?> TOPIC_SELECTOR_CLASS_DEFAULT = FixedTopicSelector.class;
    public static final String TOPIC_SELECTOR_CLASS_DOC = "The way that the target topic name is determined. Valid values are: " + Arrays.toString(TopicSelector.Type.names());
    public static final String TOPIC_SELECTOR_DEFAULT = TopicSelector.Type.fixed.name();
    public static final String TOPIC_SELECTOR_DOC = "The way that the target topic name is determined. Valid values are: " + Arrays.toString(TopicSelector.Type.names());
    private static final ConfigDef INSTANCE = new ConfigDef();
    private static final String GROUP_ID_DOC = "A unique string that identifies the consumer group this connector uses. This property is required.";



    static {
        INSTANCE
                .define(TOPIC_HEADER_NAME_CONFIG, ConfigDef.Type.STRING, TOPIC_HEADER_NAME_CONFIG_DEFAULT, ConfigDef.Importance.MEDIUM, TOPIC_HEADER_NAME_CONFIG_DOC)
                .define(SOURCE_TOPICS_CONFIG, ConfigDef.Type.LIST, SOURCE_TOPICS_CONFIG_DEFAULT, new NonEmptyListValidator(), ConfigDef.Importance.MEDIUM, SOURCE_TOPICS_CONFIG_DOC)
                .define(SOURCE_TOPICS_INTERVAL_CONFIG, ConfigDef.Type.LONG, SOURCE_TOPICS_INTERVAL_CONFIG_DEFAULT, ConfigDef.Importance.MEDIUM, SOURCE_TOPICS_INTERVAL_CONFIG_DOC)
                .define(PARTITION_HEADER_NAME_CONFIG, ConfigDef.Type.STRING, PARTITION_HEADER_NAME_CONFIG_DEFAULT, ConfigDef.Importance.MEDIUM, PARTITION_HEADER_NAME_CONFIG_DOC)
                .define(OFFSET_HEADER_NAME_CONFIG, ConfigDef.Type.STRING, OFFSET_HEADER_NAME_CONFIG_DEFAULT, ConfigDef.Importance.MEDIUM, OFFSET_HEADER_NAME_CONFIG_DOC)
                .define(TIMESTAMP_HEADER_NAME_CONFIG, ConfigDef.Type.STRING, TIMESTAMP_HEADER_NAME_CONFIG_DEFAULT, ConfigDef.Importance.MEDIUM, TIMESTAMP_HEADER_NAME_CONFIG_DOC)
                .define(STATIC_HEADER_NAMES_CONFIG, ConfigDef.Type.LIST, STATIC_HEADER_NAMES_DEFAULT, ConfigDef.Importance.MEDIUM, STATIC_HEADER_NAMES_DOC)
                .define(REMOTE_HEADER_PREFIX_CONFIG, ConfigDef.Type.STRING, REMOTE_HEADER_PREFIX_CONFIG_DEFAULT, ConfigDef.Importance.MEDIUM, REMOTE_HEADER_PREFIX_CONFIG_DOC)
                .define(TopicSelector.TOPIC_SELECTOR_CONFIG,
                        ConfigDef.Type.STRING,
                        TOPIC_SELECTOR_DEFAULT,
                        ConfigDef.ValidString.in(TopicSelector.Type.names()),
                        ConfigDef.Importance.LOW,
                        TOPIC_SELECTOR_DOC)
                .define(TOPIC_SELECTOR_CLASS_CONFIG,
                        ConfigDef.Type.CLASS,
                        null,
                        topicSelectorClassValidator(),
                        ConfigDef.Importance.HIGH,
                        TOPIC_SELECTOR_CLASS_DOC)
        ;

        // embed all Kafka consumer configurations, except key and value deserializers as we hardcode these.
        ConfigDef consumerConfigDef = new ConfigDef();
        for (ConfigDef.ConfigKey configKey : ConsumerConfig.configDef().configKeys().values()) {
            if (ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG.equals(configKey.name) || ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG.equals(configKey.name)) {
                continue;
            }

            if (ConsumerConfig.GROUP_ID_CONFIG.equals(configKey.name)) {
                consumerConfigDef.define(ConsumerConfig.GROUP_ID_CONFIG, ConfigDef.Type.STRING, ConfigDef.Importance.HIGH, GROUP_ID_DOC);
                continue;
            }

            if (ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG.equals(configKey.name)) {
                consumerConfigDef.define(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, ConfigDef.Type.LIST, ConfigDef.Importance.HIGH, BOOTSTRAP_SERVERS_DOC);
                continue;
            }

            final ConfigDef.Type newType = configKey.type == ConfigDef.Type.CLASS ? ConfigDef.Type.STRING : configKey.type;
            final Object newDefault;
            if (configKey.defaultValue instanceof Class<?>) {
                newDefault = ((Class<?>) configKey.defaultValue).getName();
            } else if (configKey.defaultValue instanceof Collection<?>) {
                newDefault = ((Collection<?>) configKey.defaultValue).stream()
                        .map(value -> value instanceof Class<?> ? ((Class) value).getName() : value)
                        .toList();
            } else {
                newDefault = configKey.defaultValue;
            }

            // Add the cleaned configDef keys, with the original settings, allowing embed to properly embed dependents and recommender as well
            consumerConfigDef.define(configKey.name, newType, newDefault, configKey.validator, configKey.importance, configKey.documentation, configKey.group, configKey.orderInGroup, configKey.width, configKey.displayName, configKey.dependents, configKey.recommender);
        }

        // Embed the cleaned config with the right names
        INSTANCE.embed(REMOTE_CONFIG_PREFIX, REMOTE_GROUP_PREFIX, 0, consumerConfigDef);
    }

    private KafkaSourceConfigDefinitions() {
    }

    public static ConfigDef configDef() {
        return new ConfigDef(INSTANCE);
    }
}
