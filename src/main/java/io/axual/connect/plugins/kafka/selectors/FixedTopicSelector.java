package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;

import java.util.Map;

/**
 * The fixed topic selector will always return the configuration string as the selected topic.
 * It does not accept a null config String or a String with only whitespace characters.
 */
public class FixedTopicSelector implements TopicSelector {
    public static final String TOPIC_SELECTOR_TARGET = "target";
    public static final String TOPIC_SELECTOR_TARGET_DOC = "The fixed target output topic";
    private String topic = null;

    @Override
    public void configure(Map<String, Object> props) {
        AbstractConfig config = new AbstractConfig(configDef(), props);
        this.configure(config.getString(TOPIC_SELECTOR_TARGET));
    }

    @Override
    public String select(String inputTopic) {
        if (topic == null) {
            throw new IllegalStateException("Target topic not configured");
        }
        return topic;
    }

    @Override
    public ConfigDef configDef() {
        return new ConfigDef()
                .define(TOPIC_SELECTOR_TARGET,
                ConfigDef.Type.STRING,
                ConfigDef.Importance.MEDIUM,
                        TOPIC_SELECTOR_TARGET_DOC);
    }

    private void configure(String config) {
        if (config == null || config.trim().isEmpty()) {
            throw new IllegalArgumentException("Null or empty config fields not allowed");
        }
        topic = config.trim();
    }
}
