package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.util.ConverterVerifier;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.config.Config;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigValue;
import org.apache.kafka.connect.connector.Task;
import org.apache.kafka.connect.source.SourceConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.axual.connect.plugins.kafka.KafkaSynchronisationConnectorInfo;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;

public class KafkaSourceConnector extends SourceConnector {

    private static final Logger log = LoggerFactory.getLogger(KafkaSourceConnector.class);

    private static final String REMOTE_CLIENT_ID_CONFIG = KafkaSourceConfigDefinitions.REMOTE_CONFIG_PREFIX + ConsumerConfig.CLIENT_ID_CONFIG;

    private final Map<String, String> connectorConfig = new HashMap<>();

    @Override
    public void start(Map<String, String> props) {
        log.debug("start({})", props);
        connectorConfig.clear();
        connectorConfig.putAll(props);
    }

    @Override
    public Class<? extends Task> taskClass() {
        log.debug("taskClass()");
        return KafkaSourceTask.class;
    }

    @Override
    public Config validate(Map<String, String> connectorConfigs) {
        // Add the dynamic topic selector config and embed it in the base config
        final ConfigDef enrichedConfigDef = config();
        final String topicSelectorClassName = connectorConfigs.get(KafkaSourceConfigDefinitions.TOPIC_SELECTOR_CLASS_CONFIG);

        try {
            TopicSelector.embedSelectorConfigDefs(enrichedConfigDef, connectorConfigs);
        } catch (IllegalArgumentException e) {
            // Only debug level as this exception is also part of the returned data
            log.debug("Could not determine the Topic Selector for validation {}", topicSelectorClassName);
        } catch (ClassNotFoundException e) {
            // Only debug level as this exception is also part of the returned data
            log.debug("Could not find TopicSelector class {}", topicSelectorClassName, e);
        }

        // Validate the enriched ConfigDef
        List<ConfigValue> configValueList = enrichedConfigDef.validate(connectorConfigs);

        // if no errors so far, then verify if nodes are reachable; if not, add error messages
        if (configValueList.stream().allMatch(cv -> cv.errorMessages().isEmpty())) {
            final List<Node> failingNodes = new KafkaSourceConfigVerifier().verify(new KafkaSourceConfig(connectorConfigs));
            if (!failingNodes.isEmpty()) {
                final String nodes = failingNodes.stream()
                        .map(node -> node.host() + ":" + node.port())
                        .collect(Collectors.joining(","));
                configValueList.stream()
                        .filter(cv -> KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG.equals(cv.name()))
                        .findAny()
                        .ifPresent(cv -> cv.addErrorMessage("Could not reach all leaders for this topic config. Verify connectivity between Connect and remote hosts; failing:" + nodes));
            }
        }

        // validate that key, value and header converters are ByteArrayConverter
        ConverterVerifier.verifyConverters(connectorConfigs, configValueList);

        return new Config(configValueList);
    }

    @Override
    public List<Map<String, String>> taskConfigs(int maxTasks) {
        log.debug("taskConfigs({})", maxTasks);
        return IntStream.range(0, maxTasks)
                .mapToObj(this::createTaskConfig)
                .toList();
    }

    @Override
    public void stop() {
        log.debug("stop()");
    }

    @Override
    public ConfigDef config() {
        log.debug("config()");
        return KafkaSourceConfigDefinitions.configDef();
    }

    @Override
    public String version() {
        log.debug("version()");
        return KafkaSynchronisationConnectorInfo.getVersion();
    }

    private Map<String, String> createTaskConfig(int taskNumber) {
        HashMap<String, String> taskConfig = new HashMap<>(connectorConfig);
        taskConfig.put(KafkaSourceTask.TASK_ID_CONFIG, Integer.toString(taskNumber));
        if (Objects.equals(taskConfig.get(REMOTE_CLIENT_ID_CONFIG), "")) {
            taskConfig.put(REMOTE_CLIENT_ID_CONFIG, connectorName() + "-" + taskNumber);
        }
        return taskConfig;
    }

    private String connectorName() {
        return connectorConfig.get("name");
    }
}
