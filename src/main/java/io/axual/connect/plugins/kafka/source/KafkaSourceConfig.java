package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import lombok.Getter;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.EqualsAndHashCode;
import lombok.val;

import static io.axual.connect.plugins.kafka.source.KafkaSourceConfigDefinitions.REMOTE_CONFIG_PREFIX;
import static io.axual.connect.plugins.kafka.source.KafkaSourceConfigDefinitions.SOURCE_TOPICS_CONFIG;
import static io.axual.connect.plugins.kafka.source.KafkaSourceConfigDefinitions.SOURCE_TOPICS_INTERVAL_CONFIG;
import static io.axual.connect.plugins.kafka.source.KafkaSourceConfigDefinitions.TOPIC_SELECTOR_CLASS_CONFIG;
import static io.axual.connect.plugins.kafka.selectors.TopicSelector.TOPIC_SELECTOR_CONFIG;

@EqualsAndHashCode(callSuper = true)
public class KafkaSourceConfig extends AbstractConfig {

    @Getter
    private final Map<String, String> staticHeaders;

    private final Map<String, String> topicMappings = new HashMap<>();

    private final String remoteTopicHeadername;
    private final String remotePartitionHeadername;
    private final String remoteOffsetHeadername;
    private final String remoteTimestampHeadername;
    private final String remoteHeaderPrefix;

    public KafkaSourceConfig(Map<String, ?> originals) {
        super(KafkaSourceConfigDefinitions.configDef(), originals);
        val staticHeaderNames = getList(KafkaSourceConfigDefinitions.STATIC_HEADER_NAMES_CONFIG);

        val headerErrors = getHeaderErrors(originals, staticHeaderNames);
        if (!headerErrors.isEmpty()) {
            throw new ConfigException(String.join("\n", headerErrors));
        }

        this.staticHeaders = getStaticHeaders(originals, staticHeaderNames);
        remoteTopicHeadername = getString(KafkaSourceConfigDefinitions.TOPIC_HEADER_NAME_CONFIG);
        remotePartitionHeadername = getString(KafkaSourceConfigDefinitions.PARTITION_HEADER_NAME_CONFIG);
        remoteOffsetHeadername = getString(KafkaSourceConfigDefinitions.OFFSET_HEADER_NAME_CONFIG);
        remoteTimestampHeadername = getString(KafkaSourceConfigDefinitions.TIMESTAMP_HEADER_NAME_CONFIG);
        remoteHeaderPrefix = getString(KafkaSourceConfigDefinitions.REMOTE_HEADER_PREFIX_CONFIG);
    }

    /**
     * Get the config for the remote Consumer.
     * @return the consumer configuration map
     */
    public Map<String, Object> getRemoteConfig() {
        final Map<String, Object> remoteConfig = originalsWithPrefix(REMOTE_CONFIG_PREFIX, true);
        remoteConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
        remoteConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
        return remoteConfig;
    }

    public List<String> getTopics() {
        return getList(SOURCE_TOPICS_CONFIG);
    }

    public Long getPollInterval() {
        return getLong(SOURCE_TOPICS_INTERVAL_CONFIG);
    }

    public Optional<String> getRemoteTopicHeadername() {
        return Optional.ofNullable(remoteTopicHeadername);
    }

    public Optional<String> getRemotePartitionHeadername() {
        return Optional.ofNullable(remotePartitionHeadername);
    }

    public Optional<String> getRemoteOffsetHeadername() {
        return Optional.ofNullable(remoteOffsetHeadername);
    }

    public Optional<String> getRemoteTimestampHeadername() {
        return Optional.ofNullable(remoteTimestampHeadername);
    }

    public Optional<String> getRemoteHeaderPrefix() {
        return Optional.ofNullable(remoteHeaderPrefix);
    }

    public Class<?> getTopicSelectorClass() {
        return getClass(TOPIC_SELECTOR_CLASS_CONFIG);
    }

    /**
     * @deprecated should move to configuration by topic selector class name instead of using the names in {@link io.axual.connect.plugins.kafka.selectors.TopicSelector.Type}.
     * @return the name of the topic selection method.
     */
    @Deprecated(forRemoval = true, since = "2.0.0")
    public String getTopicSelector() {
        return getString(TOPIC_SELECTOR_CONFIG);
    }

    /**
     * Check if for all configured static headers, a static header name and value are present.
     * @param originals the original config map.
     * @param staticHeaders a list of static headers.
     * @return a List of headers in error (empty List if no errors found).
     */
    private List<String> getHeaderErrors(Map<?, ?> originals, List<String> staticHeaders) {
        List<String> headerErrors = new ArrayList<>();
        staticHeaders.forEach(header -> {
            val headerNameKey = String.format(KafkaSourceConfigDefinitions.STATIC_HEADER_NAME_FORMAT, header);
            val headerValueKey = String.format(KafkaSourceConfigDefinitions.STATIC_VALUE_NAME_FORMAT, header);
            if (!originals.containsKey(headerNameKey)) {
                headerErrors.add(String.format("No header name defined for header '%s'; was expecting config '%s'", header, headerNameKey));
            }
            if (!originals.containsKey(headerValueKey)) {
                headerErrors.add(String.format("No header value defined for header '%s'; was expecting config '%s'", header, headerValueKey));
            }
        });
        return headerErrors;
    }

    /**
     * Get the map with static header names and values.
     * @param originals the original config map.
     * @param staticHeaders a list of static headers.
     * @return a Map of static header name to static header value.
     */
    private Map<String, String> getStaticHeaders(Map<?,?> originals, List<String> staticHeaders) {
        Map<String, String> result = new HashMap<>();
        staticHeaders.forEach(header -> {
            val headerName = String.format(KafkaSourceConfigDefinitions.STATIC_HEADER_NAME_FORMAT, header);
            val headerValue = String.format(KafkaSourceConfigDefinitions.STATIC_VALUE_NAME_FORMAT, header);
            result.put(
                    (String) ConfigDef.parseType(headerName, originals.get(headerName), ConfigDef.Type.STRING),
                    (String) ConfigDef.parseType(headerValue, originals.get(headerValue), ConfigDef.Type.STRING)
            );
        });
        return result;
    }
}
