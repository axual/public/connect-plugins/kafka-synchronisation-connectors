package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.ConnectRecord;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

/**
 * The topic selector is used to determine to which topic a record should be written.
 * This interface also contains the create method for the supported Topic Selectors
 */
public interface TopicSelector {

    /** configuration property for topic selector (by {@link TopicSelector.Type} name) */
    String TOPIC_SELECTOR_CONFIG = "topic.selector";
    /** configuration property for topic selector (by selector class name) */
    String TOPIC_SELECTOR_CLASS_CONFIG = "topic.selector.class";

    /**
     * Create a configured TopicSelector for the specified class.
     *
     * @param clazz The required selector class
     * @return An instantiated TopicSelector
     */
    static TopicSelector createSelector(Class<?> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("Null TopicSelector is not supported");
        }

        try {
            return clazz.asSubclass(TopicSelector.class).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException("Failed to instantiate TopicSelector", e);
        }
    }

    /**
     * Add the dynamic topic selector config and embed it in the base config
     *
     * @param toEnrich ConfigDef to append to
     * @param connectorConfigs connector configuration
     * @throws ClassNotFoundException if class provided is not found in classpath
     */
    static void embedSelectorConfigDefs(final ConfigDef toEnrich, final Map<String, String> connectorConfigs) throws ClassNotFoundException {
        final String topicSelectorClassName = connectorConfigs.get(TOPIC_SELECTOR_CLASS_CONFIG);

        if (Objects.isNull(topicSelectorClassName)) {
            String topicSelectorMethod = connectorConfigs.get(TOPIC_SELECTOR_CONFIG);
            final ConfigDef selectorConfigDef = TopicSelector.createSelector(topicSelectorMethod).configDef();
            toEnrich.embed(TOPIC_SELECTOR_CONFIG + ".", TOPIC_SELECTOR_CONFIG, 0, selectorConfigDef);
        } else {
            Class<?> topicSelectorClass = Class.forName(topicSelectorClassName);
            final ConfigDef selectorConfigDef = TopicSelector.createSelector(topicSelectorClass).configDef();
            toEnrich.embed(TOPIC_SELECTOR_CONFIG + ".", TOPIC_SELECTOR_CONFIG, 0, selectorConfigDef);
        }
    }

    /**
     * Create a configured TopicSelector for the specified method.
     *
     * @param method The required selector method
     * @return An instantiated TopicSelector
     * @deprecated since = "1.1.0"
     */
    @Deprecated(since = "1.1.0", forRemoval = true)
    static TopicSelector createSelector(String method) {
        if (method == null) {
            throw new IllegalArgumentException("Null TopicSelector method is not supported");
        }

        return TopicSelector.Type.valueOf(method).createInstance();
    }

    default void configure(Map<String, Object> props) {
//         Do nothing
    }

    /**
     * Return the config definitions for this TopicSelector.
     * @return the definition object
     */
    default ConfigDef configDef() {
        return new ConfigDef();
    }

    /**
     * Select the topic for the provided record
     *
     * @param input the record on which to base the topic name selection
     * @return the topic name to use
     */
    default String select(ConnectRecord<? extends ConnectRecord<?>> input) {
        if (input == null) {
            throw new IllegalArgumentException("Input record is null");
        }

        String targetTopic = select(input.topic());
        if (targetTopic == null || targetTopic.trim().isEmpty()) {
            throw new IllegalStateException("Target topic is either null or empty");
        }

        return targetTopic;
    }

    /**
     * Select the result topic for the provided topic.
     *
     * @param topicName the input topic name.
     * @return the output topic name to use.
     */
    String select(String topicName);

    /**
     * Enum holding all known TopicSelector names, and their implementing classes.
     * @deprecated instead of a fixed enum, we allow passing in an arbitrary class name so that custom selectors can be added.
     */
    @Deprecated(since = "1.1.0", forRemoval = true)
    enum Type {
        source(SourceTopicSelector.class),
        prefix(PrefixTopicSelector.class),
        fixed(FixedTopicSelector.class),
        mapping(MappingTopicSelector.class);

        private final Class<? extends TopicSelector> selectorClass;

        Type(Class<? extends TopicSelector> clazz) {
            this.selectorClass = clazz;
        }

        /**
         * Create an instance of the implementation for this enum instance.
         * The class is assumed to have a no-arg constructor; if creating an instance fails, a runtime exception is thrown.
         * @return an instance of a class implementing TopicSelector.
         */
        public TopicSelector createInstance() {
            try {
                return selectorClass.asSubclass(TopicSelector.class).newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException("Failed to instantiate TopicSelector", e);
            }
        }

        /**
         * Return all possible enum names (for config validation purposes)
         * @return all possible enum names
         */
        public static String[] names() {
            return Arrays.stream(values()).map(Enum::name).toArray(String[]::new);
        }
    }
}
