package io.axual.connect.plugins.kafka.selectors;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigException;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

/**
 * Select the target topic for the input by looking it up from a map. The map is read from the provided configuration.
 * Entries should be in the format:
 * <pre>
 *     [prefix].mapping.topicIn=topicOut
 * </pre>
 * Where <code>[prefix]</code> is the prefix for the topicmapper configuration.
 * If no mappings are provided, {@link #configure(Map)} will throw an exception.
 * TO DO: currently there seems to be no "proper" way of verifying (the presence of) this map from within our ConfigDef. This is
 * under investigation.
 */
@Slf4j
public class MappingTopicSelector implements TopicSelector {

    private final Map<String, String> topicMappings = new HashMap<>();

    @Override
    public void configure(Map<String, Object> props) {
        log.debug("configure()");
        AbstractConfig config = new AbstractConfig(configDef(), props);
        config.originalsWithPrefix("mapping.").forEach((key, value) -> topicMappings.put(key, (String) value));
        log.info("Configured {} topic mappings.", topicMappings.size());
        if (topicMappings.isEmpty()) {
            throw new ConfigException("No mappings from source to target topic provided");
        }
    }

    @Override
    public String select(String inputTopic) {
        log.debug("select({})", inputTopic);
        if (inputTopic == null) {
            throw new IllegalArgumentException("No topic set in record");
        }
        String targetTopic = topicMappings.get(inputTopic);
        if (targetTopic == null) {
            log.warn("No mapping for input topic '{}' found", inputTopic);
        }
        return targetTopic;
    }

}
