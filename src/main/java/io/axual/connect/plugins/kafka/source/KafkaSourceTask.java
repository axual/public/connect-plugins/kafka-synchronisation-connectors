package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.errors.ConnectException;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.source.SourceTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import io.axual.connect.plugins.kafka.KafkaSynchronisationConnectorInfo;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;

import static java.nio.charset.StandardCharsets.UTF_8;

public class KafkaSourceTask extends SourceTask {

    public static final String TOPIC = "topic";
    public static final String PARTITION = "partition";
    public static final String OFFSET = "offset";
    static final String TASK_ID_CONFIG = "task.id";
    private static final Logger log = LoggerFactory.getLogger(KafkaSourceTask.class);
    private KafkaSourceConfig config;
    private Long pollInterval;
    private Consumer<byte[], byte[]> consumer;

    private TopicSelector topicSelector;

    private AtomicReference<Map<TopicPartition, OffsetAndMetadata>> offsetsToCommit;

    private ConcurrentHashMap<TopicPartition, AtomicInteger> recordsInFlight;

    /**
     * Instantiates TopicSelector from the given config.
     * <p>
     * TOPIC_SELECTOR_CLASS_CONFIG takes precedence over deprecated TOPIC_SELECTOR_CONFIG
     * @param config Source Connector Configuration
     * @return the {@link TopicSelector} implementation to use.
     */
    public static TopicSelector createTopicSelector(KafkaSourceConfig config) {
        return config.getTopicSelectorClass() != null ?
                TopicSelector.createSelector(config.getTopicSelectorClass()) : TopicSelector.createSelector(config.getTopicSelector());
    }

    @Override
    public String version() {
        log.debug("version()");
        return KafkaSynchronisationConnectorInfo.getVersion();
    }

    @Override
    public void start(Map<String, String> props) {
        log.debug("start({})", props);
        config = new KafkaSourceConfig(props);
        final List<Node> failingNodes = new KafkaSourceConfigVerifier().verify(config);
        if (!failingNodes.isEmpty()) {
            final String nodenames = failingNodes.stream()
                    .map(node -> node.host() + ":" + node.port())
                    .collect(Collectors.joining(","));
            throw new KafkaException("Unable to connect to topic leader nodes: " + nodenames);
        }
        pollInterval = config.getPollInterval();
        offsetsToCommit = new AtomicReference<>(new ConcurrentHashMap<>());
        recordsInFlight = new ConcurrentHashMap<>();
        topicSelector = createTopicSelector(config);
        topicSelector.configure(config.originalsWithPrefix(TopicSelector.TOPIC_SELECTOR_CONFIG + "."));
        consumer = createConsumer(config.getRemoteConfig());
        consumer.subscribe(config.getTopics(), new RemoteConsumerRebalanceListener());
    }

    @Override
    public List<SourceRecord> poll() throws InterruptedException {
        log.debug("poll()");

        // first commit the received SourceRecord commits
        final Map<TopicPartition, OffsetAndMetadata> receivedCommits = offsetsToCommit.getAndSet(new ConcurrentHashMap<>());
        log.debug("Committing {} topic/partitions", receivedCommits.size());
        consumer.commitSync(receivedCommits);

        // check record in flight counts for zero and unpause any topic/partitions
        final List<TopicPartition> partitionsToResume = recordsInFlight.entrySet().stream()
                .filter(entry -> entry.getValue().get() == 0)
                .map(Map.Entry::getKey)
                .toList();
        if (!partitionsToResume.isEmpty()) {
            consumer.resume(partitionsToResume);
        }

        // poll and create SourceRecords to return; keep track of in flight records
        final ConsumerRecords<byte[], byte[]> consumerRecords = consumer.poll(Duration.ofMillis(pollInterval));
        log.debug("poll(): got {} records", consumerRecords.count());
        List<SourceRecord> recordsOut = new ArrayList<>();

        // since we won't get records from paused partitions, there is no need to increment existing counters.
        // Either a TopicPartition is not present as a key, or it has value 0 and the key can be replaced.
        final HashMap<TopicPartition, AtomicInteger> newRecordsInFlight = new HashMap<>();

        consumerRecords.forEach(consumerRecord -> {
            recordsOut.add(convertToSourceRecord(consumerRecord));

            TopicPartition topicPartition = new TopicPartition(consumerRecord.topic(), consumerRecord.partition());
            if (!newRecordsInFlight.containsKey(topicPartition)) {
                newRecordsInFlight.put(topicPartition, new AtomicInteger(1));
            } else {
                newRecordsInFlight.get(topicPartition).incrementAndGet();
            }
        });

        recordsInFlight.putAll(newRecordsInFlight);
        consumer.pause(newRecordsInFlight.keySet());

        return recordsOut;
    }

    @Override
    public void stop() {
        log.debug("stop()");
        if (consumer != null) {
            // clean up: try to commit any outstanding record commits
            commitOffsets();

            consumer.close();
            consumer = null;
        }
    }

    @Override
    public void commitRecord(SourceRecord sourceRecord) throws InterruptedException {
        final Map<String, ?> sourcePartition = sourceRecord.sourcePartition();
        String topic = (String) sourcePartition.get(TOPIC);
        Integer partition = (Integer) sourcePartition.get(PARTITION);
        Long offset = (Long) sourceRecord.sourceOffset().get(OFFSET);
        log.debug("commitRecord({},{},{})", topic, partition, offset);

        TopicPartition topicPartition = new TopicPartition(topic, partition);
        if (!recordsInFlight.containsKey(topicPartition)) {
            log.warn("Unexpected commit received for {},{}", topic, partition);
        } else {
            recordsInFlight.get(topicPartition).decrementAndGet();
        }

        // use updateAndGet to do an atomic update, see AtomicReference#updateAndGet
        // note: offset to commit should be the next offset we want to read, so received offset + 1!
        offsetsToCommit.updateAndGet(oldmap -> {
            oldmap.put(new TopicPartition(topic, partition), new OffsetAndMetadata(offset + 1));
            return oldmap;
        });
    }

    private SourceRecord convertToSourceRecord(ConsumerRecord<?, ?> consumerRecord) {
        log.debug("convertToSourceRecord()");
        Map<String, Object> sourcePartition = new HashMap<>();
        sourcePartition.put(TOPIC, consumerRecord.topic());
        sourcePartition.put(PARTITION, consumerRecord.partition());

        Map<String, Object> sourceOffset = new HashMap<>();
        sourceOffset.put(OFFSET, consumerRecord.offset());

        ConnectHeaders connectHeaders = new ConnectHeaders();
        config.getStaticHeaders().forEach((name, value) -> connectHeaders.addBytes(name, value.getBytes(UTF_8)));

        config.getRemoteTopicHeadername().ifPresent(name -> connectHeaders.addBytes(name, consumerRecord.topic().getBytes(UTF_8)));
        config.getRemotePartitionHeadername().ifPresent(name -> connectHeaders.addBytes(name, toBytes(consumerRecord.partition())));
        config.getRemoteOffsetHeadername().ifPresent(name -> connectHeaders.addBytes(name, toBytes(consumerRecord.offset())));
        config.getRemoteTimestampHeadername().ifPresent(name -> {
            connectHeaders.addBytes(name, toBytes(consumerRecord.timestamp()));
            connectHeaders.addBytes(name + "-Type", consumerRecord.timestampType().name().getBytes(UTF_8));
        });

        config.getRemoteHeaderPrefix().ifPresent(prefix ->
                consumerRecord.headers().forEach(
                        header -> connectHeaders.addBytes(prefix + header.key(), header.value()))
        );

        String targetTopic = topicSelector.select(consumerRecord.topic());
        if (targetTopic == null) {
            throw new ConnectException("No output topic mapping found for input topic: " + consumerRecord.topic());
        }
        return new SourceRecord(sourcePartition, sourceOffset, targetTopic, null,
                Schema.BYTES_SCHEMA, consumerRecord.key(),
                Schema.BYTES_SCHEMA, consumerRecord.value(), null, connectHeaders);
    }

    private void commitOffsets() {
        final Map<TopicPartition, OffsetAndMetadata> receivedCommits = offsetsToCommit.getAndSet(new ConcurrentHashMap<>());
        log.debug("Committing {} topic/partitions", receivedCommits.size());
        consumer.commitSync(receivedCommits);
    }

    private byte[] toBytes(Long value) {
        final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(value);
        return buffer.array();
    }

    private byte[] toBytes(Integer value) {
        final ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(value);
        return buffer.array();
    }

    /**
     * Factory method for Kafka consumer, package protected to be able to inject a mocked instance.
     *
     * @param config consumer configuration.
     * @return a Kafka consumer.
     */
    KafkaConsumer<byte[], byte[]> createConsumer(Map<String, Object> config) {
        return new KafkaConsumer<>(config);
    }

    /**
     * This is an implementation of ConsumerRebalanceListener in order to clear the partition assignments
     * of this task as there might be partitions which will be taken away from the consumer associated
     * with it.
     */
    class RemoteConsumerRebalanceListener implements ConsumerRebalanceListener {
        @Override
        public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            //Check if these topic partitions are already part of the paused list and if yes, remove them
            commitOffsets();
            partitions.forEach(
                    partition -> recordsInFlight.remove(partition)
            );
        }

        @Override
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
            //No action necessary for new partition assignments
        }
    }
}
