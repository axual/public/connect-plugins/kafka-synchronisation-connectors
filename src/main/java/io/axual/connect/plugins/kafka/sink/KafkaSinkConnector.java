package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.kafka.util.ConverterVerifier;
import org.apache.kafka.common.config.Config;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigValue;
import org.apache.kafka.connect.connector.Task;
import org.apache.kafka.connect.sink.SinkConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.axual.connect.plugins.kafka.KafkaSynchronisationConnectorInfo;
import io.axual.connect.plugins.kafka.selectors.TopicSelector;

/**
 * The Kafka Sink Connector contains the validation logic and task assignments, currently it will generate the maximum number of task assignments.
 * <p>
 * No control flow or additional connectivity validation is implemented yet
 */
public class KafkaSinkConnector extends SinkConnector {
    private static final Logger log = LoggerFactory.getLogger(KafkaSinkConnector.class);

    private final Map<String, String> connectorConfig = new HashMap<>();

    @Override
    public void start(Map<String, String> configMap) {
        log.debug("Starting connector with properties {}", configMap);
        connectorConfig.clear();
        connectorConfig.putAll(configMap);
    }

    @Override
    public Class<? extends Task> taskClass() {
        return KafkaSinkTask.class;
    }

    @Override
    public List<Map<String, String>> taskConfigs(int maxTasks) {
        log.debug("Creating {} task configs", maxTasks);
        List<Map<String, String>> taskConfigList = new ArrayList<>();
        for (int taskNr = 0; taskNr < maxTasks; taskNr++) {
            HashMap<String, String> taskConfig = new HashMap<>(connectorConfig);
            taskConfig.put(KafkaSinkTask.TASK_ID_CONFIG, Integer.toString(taskNr));
            taskConfigList.add(taskConfig);
        }
        return taskConfigList;
    }

    @Override
    public void stop() {
        log.debug("Stopping connector");
    }

    @Override
    public ConfigDef config() {
        return KafkaSinkConfig.configDef();
    }

    @Override
    public String version() {
        return KafkaSynchronisationConnectorInfo.getVersion();
    }

    @Override
    public Config validate(Map<String, String> connectorConfigs) {
        // Add the dynamic topic selector config and embed it in the base config
        final ConfigDef enrichedConfigDef = config();
        String topicSelectorClassName = connectorConfigs.get(TopicSelector.TOPIC_SELECTOR_CLASS_CONFIG);

        try {
            TopicSelector.embedSelectorConfigDefs(enrichedConfigDef, connectorConfigs);
        } catch (IllegalArgumentException e) {
            // Only debug level as this exception is also part of the returned data
            log.debug("Could not determine the Topic Selector for validation {}", topicSelectorClassName);
        } catch (ClassNotFoundException e) {
            // Only debug level as this exception is also part of the returned data
            log.debug("Could not find TopicSelector class {}", topicSelectorClassName, e);
        }

        List<ConfigValue> enrichedConfigValues = enrichedConfigDef.validate(connectorConfigs);
        ConverterVerifier.verifyConverters(connectorConfigs, enrichedConfigValues);
        return new Config(enrichedConfigValues);
    }
}
