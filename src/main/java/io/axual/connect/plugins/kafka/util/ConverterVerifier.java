package io.axual.connect.plugins.kafka.util;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2025 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigValue;
import org.apache.kafka.connect.converters.ByteArrayConverter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Utility class to verify that key, value and header converters are type {@link ByteArrayConverter}.
 */
public class ConverterVerifier {

    private static final List<String> CONVERTER_KEYS = Arrays.asList("key.converter","value.converter","header.converter");

    private static final String BYTE_ARRAY_CONVERTER = ByteArrayConverter.class.getName();

    private ConverterVerifier() {
        // we don't need an instance.
    }

    /**
     * Checks the provided configs for configured converters, and checks their type.
     * If any misconfigured converter is found, an entry with error message is added
     * to the provided {@link ConfigValue} list.
     * @param configs A connector configuration.
     * @param configValues a list of ConfigValues.
     */
    public static void verifyConverters(Map<String, String> configs, List<ConfigValue> configValues) {
        CONVERTER_KEYS.forEach(key -> {
            String value = configs.get(key);
            if (!configs.containsKey(key) || value != null && !value.equals(BYTE_ARRAY_CONVERTER)) {
                ConfigValue configValue = new ConfigValue(key);
                configValue.addErrorMessage(key + ": converter class should be " + BYTE_ARRAY_CONVERTER);
                configValues.add(configValue);
            }
        });
    }
}
