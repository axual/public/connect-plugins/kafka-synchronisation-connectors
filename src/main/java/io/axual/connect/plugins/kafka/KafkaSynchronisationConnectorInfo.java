package io.axual.connect.plugins.kafka;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Helper class to determine static connector properties like version
 */
public class KafkaSynchronisationConnectorInfo {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaSynchronisationConnectorInfo.class);
    private static final String INFO_PATH = "/kafka-connectors.properties";
    private static final String VERSION;

    static {
        Properties properties = new Properties();
        try (InputStream stream = KafkaSynchronisationConnectorInfo.class.getResourceAsStream(INFO_PATH)) {
            properties.load(stream);
        } catch (IOException e) {
            LOG.warn("Could not read info resource file {}", INFO_PATH, e);
        }
        VERSION = properties.getProperty("version", "unknown");
    }

    private KafkaSynchronisationConnectorInfo() {
        // Private constructor to prevent instantiation
    }

    /**
     * Get the current connector version
     *
     * @return the connector version
     */
    public static String getVersion() {
        return VERSION;
    }
}
