package io.axual.connect.plugins.kafka.transforms;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.SchemaAndValue;
import org.apache.kafka.connect.transforms.Transformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Copies the schema and value of either the record key or record value to a header.
 */
public abstract class HeaderFrom<R extends ConnectRecord<R>> implements Transformation<R> {
    public static final String HEADER_NAME_CONFIG = "name";
    public static final String HEADER_NAME_DOC = "The name of the header where the key or value data is loaded";
    public static final String HEADER_APPEND_CONFIG = "append";
    public static final String HEADER_APPEND_DOC = "When set to true the new value will be added to any existing values. When set to false all existing values will be removed";
    private static final Logger LOG = LoggerFactory.getLogger(HeaderFrom.class);
    private static final ConfigDef CONFIG_DEF = new ConfigDef()
            .define(HEADER_NAME_CONFIG, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new ConfigDef.NonEmptyString(), ConfigDef.Importance.HIGH, HEADER_NAME_DOC)
            .define(HEADER_APPEND_CONFIG, ConfigDef.Type.BOOLEAN, false, ConfigDef.Importance.MEDIUM, HEADER_APPEND_DOC);

    private String headerName = null;
    private boolean appendHeader = false;


    @Override
    public R apply(R connectRecord) {
        if (headerName == null) {
            // should never happen
            throw new IllegalStateException("Transformation not configured");
        }

        final SchemaAndValue schemaAndValue = getHeaderData(connectRecord);

        // This should not happen in Connect, having data without a schema
        if (schemaAndValue.value() != null && schemaAndValue.schema() == null) {
            LOG.warn("Skipping record because no Schema is provided for a non null value");
            return connectRecord;
        }

        if (!appendHeader) {
            connectRecord.headers().remove(headerName);
        }
        connectRecord.headers().add(headerName, schemaAndValue.value(), schemaAndValue.schema());

        return connectRecord;
    }

    @Override
    public ConfigDef config() {
        return new ConfigDef(CONFIG_DEF);
    }

    @Override
    public void close() {
        // Nothing to do
    }

    abstract SchemaAndValue getHeaderData(R connectRecord);

    @Override
    public void configure(Map<String, ?> configs) {
        AbstractConfig config = new AbstractConfig(CONFIG_DEF, configs);
        headerName = config.getString(HEADER_NAME_CONFIG);
        appendHeader = config.getBoolean(HEADER_APPEND_CONFIG);
    }

    public static final class Key<R extends ConnectRecord<R>> extends HeaderFrom<R> {

        @Override
        SchemaAndValue getHeaderData(R connectRecord) {
            LOG.debug("Returning key for header");
            return new SchemaAndValue(connectRecord.keySchema(), connectRecord.key());
        }
    }

    public static final class Value<R extends ConnectRecord<R>> extends HeaderFrom<R> {

        @Override
        SchemaAndValue getHeaderData(R connectRecord) {
            LOG.debug("Returning value for header");
            return new SchemaAndValue(connectRecord.valueSchema(), connectRecord.value());
        }
    }
}
