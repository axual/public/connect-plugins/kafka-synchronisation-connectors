package io.axual.connect.plugins.kafka.source;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 - 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.TopicPartitionInfo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

/**
 * Utility class to verify connectivity to the input cluster and topics.
 */
@Slf4j
public class KafkaSourceConfigVerifier {

    public List<Node> verify(KafkaSourceConfig config) {
        log.debug("verify({})", config);
        final AdminClient adminclient = createAdminclient(config);
        final DescribeTopicsResult describeTopicsResult = adminclient.describeTopics(config.getTopics());
        final List<Node> failingNodes = new ArrayList<>();
        describeTopicsResult.all().whenComplete((topics, e) -> {
            if (e != null) {
                log.warn("describeTopics failed", e);
                throw new KafkaException("describeTopics failed", e);
            }
            final List<Node> collect = topics.values().stream()
                    .map(TopicDescription::partitions)
                    .flatMap(Collection::stream)
                    .map(TopicPartitionInfo::leader)
                    .distinct()
                    .map(this::connectToNode)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toList());
            failingNodes.addAll(collect);
        });
        return failingNodes;
    }

    /**
     * Verify connectivity to a Node.
     * This method is package protected for easier unit testing.
     * @param node
     */
    Optional<Node> connectToNode(Node node) {
        log.debug("connectToNode({})", node);
        SocketAddress socketAddress = new InetSocketAddress(node.host(), node.port());
        try (Socket socket = new Socket() ){
            socket.connect(socketAddress);
        } catch (IOException ioe) {
            log.warn("Connection error to {}:{}", node.host(), node.port(), ioe);
            return Optional.of(node);
        }
        return Optional.empty();
    }

    /**
     * Factory method for AdminClient, package protected so we can inject a mock.
     * @param config
     * @return
     */
    AdminClient createAdminclient(KafkaSourceConfig config) {
        return AdminClient.create(config.getRemoteConfig());
    }
}
