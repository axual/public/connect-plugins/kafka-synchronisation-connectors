package io.axual.connect.plugins.kafka.sink;

/*-
 * ========================LICENSE_START=================================
 * Kafka Synchronisation Connectors for Kafka Connect
 * %%
 * Copyright (C) 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.connect.errors.ConnectException;
import org.apache.kafka.connect.errors.RetriableException;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.sink.SinkTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.axual.connect.plugins.kafka.KafkaSynchronisationConnectorInfo;

/**
 * The Kafka Sink Task contains the logic to process the records provided by Connect and send them to the runner to process the records.
 */
public class KafkaSinkTask extends SinkTask {
    protected static final String CONNECTOR_NAME_CONFIG = "name";
    protected static final String TASK_ID_CONFIG = "task.id";
    private static final Logger LOG = LoggerFactory.getLogger(KafkaSinkTask.class);
    protected String connectorName = null;
    protected String taskId = null;
    protected KafkaSinkConfig config;
    private ExecutorService executorService;
    protected KafkaSinkRunner sinkRunner;

    @Override
    public void start(Map<String, String> map) {
        config = new KafkaSinkConfig(map);
        connectorName = map.getOrDefault(CONNECTOR_NAME_CONFIG, "unknown");
        taskId = map.getOrDefault(TASK_ID_CONFIG, "unknown");
        LOG.debug("Started task '{}' for connector '{}' ", taskId, connectorName);
        sinkRunner = createRunner(config);
        executorService = Executors.newSingleThreadExecutor();
        executorService.submit(sinkRunner);
    }

    @Override
    public void stop() {
        LOG.info("Stopping task '{}' for connector '{}'", taskId, connectorName);
        sinkRunner.stop();
        sinkRunner = null;

        config = null;
        LOG.info("Stopping task '{}' for connector '{}'", taskId, connectorName);

        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
        executorService = null;
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> preCommit(Map<TopicPartition, OffsetAndMetadata> currentOffsets) {
        LOG.debug("Pre commit called for task '{}' for connector '{}'", taskId, connectorName);
        return sinkRunner == null ? Collections.emptyMap() : sinkRunner.getStoredOffsetsAndClear();
    }

    @Override
    public void put(Collection<SinkRecord> records) {
        LOG.info("Putting {} records to thread {}", records.size(), sinkRunner.getThreadID());
        try {
            if (!sinkRunner.isRunning()) {
                throw new ConnectException(String.format("Worker thread has stopped unexpectedly: %s", sinkRunner.getFailureDescription()));
            }

            if (!sinkRunner.put(records)) {
                throw new RetriableException("Could not enqueue records");
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new ConnectException("Put was interrupted");
        }
    }

    private void handleSinkRunnerFailure(RuntimeException exception) {
        throw exception;
    }

    /**
     * Helper to create the runner object, can also be used to inject mocked runners for testing purposes
     *
     * @param config   The KafkaSinkConfig created for this task
     * @return The KafkaSinkRunner created for this task
     */
    public KafkaSinkRunner createRunner(KafkaSinkConfig config) {
        return new KafkaSinkRunner(config, this::handleSinkRunnerFailure);
    }

    @Override
    public String version() {
        return KafkaSynchronisationConnectorInfo.getVersion();
    }
}
